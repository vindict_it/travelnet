<?php

if (!defined('insurance_header')) define('insurance_header', 'Страхование туристов');
if (!defined('insurance_subheader')) define('insurance_subheader', 'Для рассчета стоимости страховки или оформления договора, пожалуйста,
																	заполните поля ниже. Оператор обработает Ваш запрос и вышлет результаты
																	на почтовый адрес, закрепленный за Вашим аккаунтом. Вы сможете связаться
																	с оператором для уточнения любых моментов и вопросов. Результаты рассчетов
																	Вы получите в течении 1-3 рабочих дней.');
if (!defined('insurance_session')) define('insurance_session', 'Вы должны авторизоваться');
if (!defined('insurance_session_text')) define('insurance_session_text', 'Чтобы рассчитать стоимость страховки или оформить договор,
																		  Вам необходимо войти в свой Личный кабинет, куда мы будем отправлять
																		  нужную информацию и где Вы сможете скачать запрашиваемые документы.');

if (!defined('insurance_types')) define('insurance_types', 'Выберите вид страхования');	
if (!defined('insurance_agreement')) define('insurance_agreement', 'Договор страхования');	

if (!defined('insurance_data')) define('insurance_data', 'Данные для расчета');	
if (!defined('insurance_directions')) define('insurance_directions', 'Направление (куда направляетесь)');	
if (!defined('insurance_terms')) define('insurance_terms', 'Срок страхования');	
if (!defined('insurance_date_start')) define('insurance_date_start', 'Дата начала путешествия');	
if (!defined('insurance_date_end')) define('insurance_date_end', 'Дата окончания путешествия');	

if (!defined('insurance_people')) define('insurance_people', 'Застрахованные лица');	
if (!defined('insurance_people_add')) define('insurance_people_add', 'Добавить человека');	
if (!defined('insurance_people_insurance')) define('insurance_people_insurance', 'Застрахованный');	
if (!defined('insurance_people_birth')) define('insurance_people_birth', 'Дата рождения');	

if (!defined('insurance_calculate')) define('insurance_calculate', 'Рассчитать стоимость');																	  
if (!defined('insurance_dogovor')) define('insurance_dogovor', 'Оформить договор');	
																  
if (!defined('event_error')) define('event_error', 'Похоже, что Вы отправили форму и не нажали на кнопку. Пожалуйста, повторите отправку формы, чтобы мы смогли понять, какое действие Вы хотите использовать.');																	  
if (!defined('type_error')) define('type_error', 'Похоже, Вы не выбрали вид страховки, которая Вас интересует.');																	  
if (!defined('direction_error')) define('direction_error', 'Похоже, Вы не ввели направление Вашего путешествия. Оно должно быть от 0 до 180 символов.');																	  
if (!defined('dates_error')) define('dates_error', 'Похоже, Вы не указали даты Вашего путешествия.');																	  
if (!defined('db_error')) define('db_error', 'Извините за неудобства, но у нас на сервере произошла ошибка. Скорее всего, ведуться профилактические работы. Пожалуйста, попробуйте перезагрузить страницу.');																	  
if (!defined('no_auth')) define('no_auth', 'Для отправки формы Страховки Вам необходимо авторизоваться на сайте.');																	  
var captcha_required = false;
var firstnameBull = false;
var mailBull = false;
$(document).ready(function () {
    "use strict";
	
	var site_name = window.location.protocol + '//' + window.location.hostname;
    var firstname = $('input[name=u_name]');
    firstname.blur(function(){
        if(firstname.val().length != 0){
            if(firstname.val().length < 2){
                $('input[name=u_name]').parent().next('.color--error').text('Ім\'я не коротші 2-х символів');
                $('input[name=u_name]').parent().next('.color--error').fadeIn('fast');
                $('input[name=u_name]').parent('.clickable').addClass('has-error');

            }else if(firstname.val().length > 64){
                $('input[name=u_name]').parent().next('.color--error').text('Ім\'я довше 64-х символів');
                $('input[name=u_name]').parent().next('.color--error').fadeIn('fast');
                $('input[name=u_name]').parent('.clickable').addClass('has-error');
            } else {
                $('input[name=u_name]').parent().next('.color--error').text('');
                $('input[name=u_name]').parent().next('.color--error').fadeOut('fast');
                $('input[name=u_name]').parent('.clickable').removeClass('has-error');
            }
        }else{
            $('input[name=u_name]').parent().next('.color--error').text('Введіть ваше ім\'я');
            $('input[name=u_name]').parent().next('.color--error').fadeIn('fast');
            $('input[name=u_name]').parent('.clickable').addClass('has-error');
        }
        if(firstname.parents('.clickable').hasClass('has-error')) {
            firstnameBull = false;
        } else {
            firstnameBull = true;
        }
        if(firstnameBull && mailBull && captcha_required) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });

    var mailPattern = /^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i;
    var mail = $('input[name=u_email]');

    mail.blur(function(){
        if(mail.val().length != 0){
            $('input[name=u_email]').parent().next('.color--error').text('');
            $('input[name=u_email]').parent().next('.color--error').fadeOut('fast');
            if(mail.val().search(mailPattern) == 0){
                $('input[name=u_email]').parent().next('.color--error').text('');
                $('input[name=u_email]').parent().next('.color--error').fadeOut('fast');
                $('input[name=u_email]').parent('.clickable').removeClass('has-error');
            }else{
                $('input[name=u_email]').parent().next('.color--error').text('Введіть коректний Email');
                $('input[name=u_email]').parent().next('.color--error').fadeIn('fast');
                $('input[name=u_email]').parent('.clickable').addClass('has-error');
            }
        }else{
            $('input[name=u_email]').parent().next('.color--error').text('Введіть Email-адресу');
            $('input[name=u_email]').parent().next('.color--error').fadeIn('fast');
            $('input[name=u_email]').parent('.clickable').addClass('has-error');
        }
        if(mail.parents('.clickable').hasClass('has-error')) {
            mailBull = false;
        } else {
            mailBull = true;
        }
        if(firstnameBull && mailBull && captcha_required) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });


    var buttonSubmit = $('button[type=submit]');
    var ContactForm = $('#contacts-form-ajax');

    ContactForm.on('submit', function(e){;
        e.preventDefault();
            $.ajax({
                type: 'post',
                url: site_name + '/model/ajax-handler.php', 
                data: {form_data: $('#contacts-form-ajax').serialize(), locale: 'uk', action: 'contacts-form'}
            }).done( function () {
                swal({
                    title: "Дякую за ваше повідомлення",
                    text: "Ми отримали Ваше повідомлення і відповімо на нього по Email. Наша команда підтримки обробить Ваш запит в порядку черги.",
                    type: "success",
                    confirmButtonText: "Продовжити",
                });
                $('#contacts-form-ajax').closest('form').find("input, textarea").val("");
            }).fail(function() {
                swal({
					title: "Ой, щось пішло не так...",
					text: "Під час відправки запиту сервер відповів з помилкою. Будь ласка, првоерьте введенння Вами дані і спробуйте ще раз. Якщо це не спрацювало - зв\'яжіться з нами по іншому каналу зв\'язку.",
					type: "warning",
					confirmButtonText: "Повторити",
                });
            });
    });
});

function onloadCaptcha() {
	grecaptcha.render('captcha-container', {
		'theme': 'light',
		'sitekey': '6Lf-xngUAAAAAGNSp8SqD_pHd6fA5BG4v2vxCres',
		'expired-callback': captcha_expired,
		'callback': captcha_callback
	});
}

function captcha_callback() {
	captcha_required = true;
	if(firstnameBull && mailBull && captcha_required) {
		$('button[type=submit]').attr('disabled', false);
	} else {
		$('button[type=submit]').attr('disabled', true);
	}
}

function captcha_expired() {
	captcha_required = false;
	if(firstnameBull && mailBull && captcha_required) {
		$('button[type=submit]').attr('disabled', false);
	} else {
		$('button[type=submit]').attr('disabled', true);
	}
}
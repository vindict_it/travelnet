<?php

if (!defined('order_flight_step1')) define('order_flight_step1', 'Бронирование');
if (!defined('order_flight_step2')) define('order_flight_step2', 'Подтверждение');
if (!defined('order_flight_step3')) define('order_flight_step3', 'Оплата');
if (!defined('order_flight_step4')) define('order_flight_step4', 'Выдача документов');
if (!defined('order_download_all')) define('order_download_all', 'Скачать все документы');
if (!defined('order_download')) define('order_download', 'Скачать документ');
if (!defined('order_print')) define('order_print', 'Распечатать документ');
if (!defined('order_ready')) define('order_ready', 'Ваши документы готовы');
if (!defined('order_ready_btn')) define('order_ready_btn', 'Для их скачивания нажмите на кнопку ниже');
if (!defined('order_bank')) define('order_bank', 'Выписка из банка');
if (!defined('order_avia')) define('order_avia', 'Авиабилеты');
if (!defined('order_insurance')) define('order_insurance', 'Страховка');
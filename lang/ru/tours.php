<?php

if (!defined('selector_country_choose')) define('selector_country_choose', 'Выберите страну...');
if (!defined('selector_country_choose_graph')) define('selector_country_choose_graph', 'Для отображения графика - сначала выберите страну');
if (!defined('selector_kurort_header')) define('selector_kurort_header', 'Выберите курорт/регион');
if (!defined('selector_country_header')) define('selector_country_header', 'Выберите страну назначения');
if (!defined('selector_date_choose')) define('selector_date_choose', 'Дата поездки...');
if (!defined('selector_lowcost_header')) define('selector_lowcost_header', 'График низких цен');
if (!defined('selector_lowcost_subheader')) define('selector_lowcost_subheader', 'Цены на 2 человек при заказе тура на 7 дней');
if (!defined('selector_lowcost_from')) define('selector_lowcost_from', 'Тур на');
if (!defined('selector_lowcost_to')) define('selector_lowcost_to', 'дней');
if (!defined('selector_kurort_country')) define('selector_kurort_country', 'Выберите страну');
if (!defined('selector_kurort_arrival')) define('selector_kurort_arrival', 'Выберите место назначения');
if (!defined('selector_kurort_search')) define('selector_kurort_search', 'Быстрый поиск');
if (!defined('selector_kurort_city_choose')) define('selector_kurort_city_choose', 'Выберите город');
if (!defined('selector_kurort_city_avia')) define('selector_kurort_city_avia', 'вылета');
if (!defined('selector_kurort_city_available')) define('selector_kurort_city_available', 'Доступные города');
if (!defined('selector_kurort_dates_header')) define('selector_kurort_dates_header', 'Выберите даты');
if (!defined('selector_kurort_dates_sdvig')) define('selector_kurort_dates_sdvig', 'со сдвигом');
if (!defined('selector_kurort_dates_first')) define('selector_kurort_dates_first', 'дня от выбранной даты');
if (!defined('selector_group_pick')) define('selector_group_pick', 'Укажите состав группы');
if (!defined('selector_group_adults')) define('selector_group_adults', 'Взрослые');
if (!defined('selector_group_children')) define('selector_group_children', 'Дети');
if (!defined('selector_operator_pick')) define('selector_operator_pick', 'Выберите операторов');
if (!defined('kurort_placehodler_search')) define('kurort_placehodler_search', 'Париж');
if (!defined('main_not_found')) define('main_not_found', 'Данные по туру не были найдены');
if (!defined('main_country_choose_graph')) define('main_country_choose_graph', 'Для отображения графика - сначала выберите страну');
if (!defined('selector_header')) define('selector_header', 'Селектор туров');
if (!defined('selector_subheader')) define('selector_subheader', 'Магазин для быстрого поиска подходящего тура');
if (!defined('selector_filter')) define('selector_filter', 'Фильтр туров');
if (!defined('selector_operators')) define('selector_operators', 'Туроператоры');
if (!defined('selector_operators_pick')) define('selector_operators_pick', 'Выбрать туроператора');
if (!defined('selector_regions_pick')) define('selector_regions_pick', 'Добавить регион');
if (!defined('selector_hotels')) define('selector_hotels', 'Отель');
if (!defined('selector_codes')) define('selector_codes', 'Код тура');
if (!defined('selector_hotels_name')) define('selector_hotels_name', 'Название отеля');
if (!defined('selector_hotels_star')) define('selector_hotels_star', 'Звездность отеля');
if (!defined('selector_codes_number')) define('selector_codes_number', 'Числовой код');
if (!defined('selector_food')) define('selector_food', 'Питание');
if (!defined('selector_hotel_rating')) define('selector_hotel_rating', 'Рейтинг отеля');
if (!defined('selector_hotel_rating_from')) define('selector_hotel_rating_from', 'от');
if (!defined('selector_hotel_sea_line')) define('selector_hotel_sea_line', 'Расстояние до моря');
if (!defined('selector_hotel_line')) define('selector_hotel_line', 'линия');
if (!defined('selector_cruise_line')) define('selector_cruise_line', 'Круизная линия');
if (!defined('selector_cruise_liner')) define('selector_cruise_liner', 'Лайнер');
if (!defined('selector_countires')) define('selector_countires', 'стран');
if (!defined('selector_countires_in_tour')) define('selector_countires_in_tour', 'в туре');
if (!defined('selector_period')) define('selector_period', 'Длительность до');
if (!defined('selector_cost_two')) define('selector_cost_two', 'Цена на двоих');
if (!defined('selector_picked_filters')) define('selector_picked_filters', 'Выбранные фильтры');
if (!defined('selector_country_to')) define('selector_country_to', 'Страна назначения');
if (!defined('selector_country_to_pick')) define('selector_country_to_pick', 'Выберите страну');
if (!defined('selector_dates')) define('selector_dates', 'Даты поездки');
if (!defined('selector_loading')) define('selector_loading', 'Загрузка');
if (!defined('selector_tourists')) define('selector_tourists', 'Туристы');
if (!defined('selector_results')) define('selector_results', 'Результаты поиска');
if (!defined('selector_load_more')) define('selector_load_more', 'Загрузить еще');
if (!defined('selector_open_filter')) define('selector_open_filter', 'Открыть фильтр');





$(document).ready(function () {
    "use strict";

	var passwordPattern = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/i;
    var password = $('input[name=new-Password]');
    var passwordBull = false;

    password.blur(function(){
        if(password.val().length != 0){
            $('input[name=new-Password]').parent('.clickable').removeClass('has-error');
            if(password.val().length < 6){
                $('input[name=new-Password]').parent().next('.color--error').text('Пароль не может быть короче 6 символов');
				$('input[name=new-Password]').parent().next('.color--error').fadeIn('fast');
                $('input[name=new-Password]').parent('.clickable').addClass('has-error');
            } else {
				if(password.val().search(passwordPattern) == 0) {
					$('input[name=new-Password]').parent('.clickable').removeClass('has-error');
					$('input[name=new-Password]').parent().next('.color--error').text('');
					$('input[name=new-Password]').parent().next('.color--error').fadeOut('fast');
				} else {
					$('input[name=new-Password]').parent().next('.color--error').text('Введите более сложный пароль');
					$('input[name=new-Password]').parent().next('.color--error').fadeIn('fast');
					$('input[name=new-Password]').parent('.clickable').addClass('has-error');
				}
            }
        }else{
            $('input[name=new-Password]').parent().next('.color--error').text('Поле не может быть пустым');
			$('input[name=new-Password]').parent().next('.color--error').fadeIn('fast');
            $('input[name=new-Password]').parent('.clickable').addClass('has-error');
        }

        if(password.parents('.clickable').hasClass('has-error')) {
            passwordBull = false;
        } else {
            passwordBull = true;
        }
        if(repearPasswordBull && passwordBull) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });
	
	var repeatPassword = $('input[name=new-PasswordRepeat]');
    var repearPasswordBull = false;

    repeatPassword.blur(function(){
        if(repeatPassword.val().length != 0){
            $('input[name=new-PasswordRepeat]').parent('.clickable').removeClass('has-error');
            if(repeatPassword.val().length < 6){
                $('input[name=new-PasswordRepeat]').parent().next('.color--error').text('Поле не может содержать менее 6 символов');
				$('input[name=new-PasswordRepeat]').parent().next('.color--error').fadeIn('fast');
                $('input[name=new-PasswordRepeat]').parent('.clickable').addClass('has-error');

            }else{
                $('input[name=new-PasswordRepeat]').parent('.clickable').removeClass('has-error');
                $('input[name=new-PasswordRepeat]').parent().next('.color--error').text('');
				$('input[name=new-PasswordRepeat]').parent().next('.color--error').fadeOut('fast');
            }
        }else{
            $('input[name=new-PasswordRepeat]').parent().next('.color--error').text('Поле не может быть пустым');
			$('input[name=new-PasswordRepeat]').parent().next('.color--error').fadeIn('fast');
            $('input[name=new-PasswordRepeat]').parent('.clickable').addClass('has-error');
        }

        if(repeatPassword.parents('.clickable').hasClass('has-error')) {
            repearPasswordBull = false;
        } else {
            repearPasswordBull = true;
        }
        if(repearPasswordBull && passwordBull) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });
});

$("input").keypress(function(event){
    var ew = event.which;
    if(48 <= ew && ew <= 57)
        return true;
    if(65 <= ew && ew <= 90)
        return true;
    if(97 <= ew && ew <= 122)
        return true;
    return false;
});

$(document).on('submit', '#new-form', function(ev) {
	'use strict';
	ev.preventDefault();
	
	$.ajax({ 
		url: window.location.origin + '/model/ajax-handler.php',
		type: 'post',
		dataType: 'json',
		data: {post_data: $('#new-form').serialize(), lang: 'ru', action: 'setNewPassword'},
		success: function(response) {
            $('.error-area').remove();
			if(response.status == 'true') {
				console.log('Good response, redirecting...');
				location.replace("https://travelnet.tours/login");
			} else if(response.status == 'false') {
                var selector_heading = $('.auth-credentials').find('h2');
                $('<div class="row error-area"><div class="col-12 text-center error-message-session"><p class="mb-0">'+response.error+'</p></div></div>').insertAfter(selector_heading);
            }
		},
		error: function(err) {
			console.log(err);
		}
	});
});
$(document).ready(function() {
    if($(document).scrollTop()>50) {
		$("#menu2").addClass("flex-view");
		$("#flex-hide").hide();
	} else {
		$("#menu2").removeClass("flex-view");
		$("#flex-hide").show();
	}
	
	var hamburger = $('#menu-button');
    var menu = $('#hamb');
    hamburger.click(function(e) {
        menu.toggleClass('active');
        $('body').toggleClass('is-nav');
        if(menu.hasClass('active')) {
            $("#nav-toggle-mobile").prop("checked", true);
            $("#menu-button").css("background", "#4b2e82");
            $(".backdrop").fadeIn('slow');

        } else {
            $("#nav-toggle-mobile").prop("checked", false);
            $("#menu-button").css("background", "transparent");
            $(".backdrop").fadeOut('fast');
        }

        return false;
    });
});
$(document).on("scroll", function() {
    if($(document).scrollTop()>50) {
		$("#menu2").addClass("flex-view");
		$("#flex-hide").hide();
	} else {
		$("#menu2").removeClass("flex-view");
		$("#flex-hide").show();
	}
});
$(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();
    var width = window.innerWidth ? window.innerWidth : $(window).width() - 200;
    if(width > 624) {
        var topPadding = 90;
    } else {
        var topPadding = 50;
    }
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - topPadding
    }, 500);
});

function dropdown_activate() {
    document.getElementById("lang-dropdown").classList.toggle("show");
}
function dropdown_activate_mobile() {
    document.getElementById("lang-dropdown-mobile").classList.toggle("show");
}

window.onclick = function(event) {
  if (!event.target.matches('.dropdown-trigger')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('supa-show')) {
        openDropdown.classList.remove('supa-show');
      }
    }
  }
  if (!event.target.matches('.dropdown-trigger-mobile')) {

    var dropdowns = document.getElementsByClassName("dropdown-trigger-mobile");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('supa-show')) {
        openDropdown.classList.remove('supa-show');
      }
    }
  }
}
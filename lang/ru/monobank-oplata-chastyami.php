<?php

if (!defined('page_bank_header')) define('page_bank_header', 'Оплата частями от Моно Банка');
if (!defined('page_bank_desc')) define('page_bank_desc', 'Если Вы клиент Моно Банка и хотите купить тур при помощи услуги "Оплата частями", ознакомтесь с условиями прежде, чем совершать покупку.');
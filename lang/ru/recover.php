<?php

// login page info
if (!defined('page_main_header')) define('page_main_header', 'Восстановление пароля');
if (!defined('page_header')) define('page_header', 'Введите свой Email');
if (!defined('page_instr_header')) define('page_instr_header', 'Инструкция');
if (!defined('page_instr_desc')) define('page_instr_desc', 'Для восстановления доступа к учетной записи необходимо ввести Email-адрес, на который зарегестрирован Ваш аккаунт. На почту прийдет письмо с дальнейшими инструкциями.');
if (!defined('page_instr_warning')) define('page_instr_warning', '<strong>Подсказка:</strong> Используйте сложные пароли и всегда храните их в надежном месте.');
if (!defined('page_send')) define('page_send', 'Восстановить');

if (!defined('recover_warning')) define('recover_warning', 'Внимание');
if (!defined('recover_no_email')) define('recover_no_email', 'Вы не ввели Email');
if (!defined('recover_email')) define('recover_email', 'Введите корректный Email');
if (!defined('recover_email_not_found')) define('recover_email_not_found', 'Введенный Email не зарегистрирован');
if (!defined('recover_heading')) define('recover_heading', 'Восстановление пароля на');
if (!defined('recover_text')) define('recover_text', 'Здравствуйте, %s, <br/>Мы получили запрос на восстановление пароля от Вашего Аккаунта. Чтобы продолжить, перейдите по ссылке ниже:<br/><br/></p><p style="text-align:center;">
													<a href="%s" class="btn btn-primary btn-lg raised">Восстановить пароль</a><br/><br/></p><p>После перехода по ссылке, введите новый пароль.<br/> 
													Если Вы не запрашивали восстановление пароля, срочно <a href="%scontacts">свяжитесь с Администрацией</a>!<br/><br/>
													<small>Это сообщение было сгенерировано автоматически, не надо на него отвечать.<br/>С наилучшими пожеланиями, <br/>Команда TravelNet Tour</small>');
if (!defined('recover_success')) define('recover_success', 'Письмо с инструкциями по восстановлению пароля было отправлено на Ваш Email-адрес');
if (!defined('recover_error')) define('recover_error', 'Ой... Сейчас мы не можем отправить Вам письмо по восстановлению. Похоже, на сайте ведуться техничские работы. Пожалуйста, перезагрузите страницу и попробуйте ещё раз.');
if (!defined('recover_not_confirmed')) define('recover_not_confirmed', 'Ваш Email не подтвержден. Вы можете восстановить только активный Аккаунт. Проверьте свою почту, мы присылали Вам письмо с подтверждением.');


<?php

if (!defined('page_header')) define('page_header', 'Країнознавство');
if (!defined('page_desc')) define('page_desc', 'Не знаєте, куди відправитися в %s році? Виберіть країну, дізнайтеся більше і отримаєте рекомендації від TravelNet Tour.');
if (!defined('page_europe')) define('page_europe', 'Європа');
if (!defined('page_asia')) define('page_asia', 'Азія');
if (!defined('page_africa')) define('page_africa', 'Африка');
if (!defined('page_america')) define('page_america', 'Америка');
if (!defined('page_australia')) define('page_australia', 'Австралія');
if (!defined('countries_empty')) define('countries_empty', 'Поки країни в цьому регіоні відсутні');

if (!defined('page_video')) define('page_video', 'Дивитися відео');
<?php

if (!defined('page_bank_header')) define('page_bank_header', 'Оплата частинами від Моно Банку');
if (!defined('page_bank_desc')) define('page_bank_desc', 'Якщо Ви клієнт Моно Банку і хочете купити тур за допомогою послуги "Оплата частинами", ознайомтесь з умовами перш, ніж робити покупку.');
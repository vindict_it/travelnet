travelNetToursApp.controller('LowPricesController', function($scope, $http, $q) {
    var lowPrices = this;



    lowPrices.chart = null;
    lowPrices.chartDataOriginal = [];
    lowPrices.chartData = [];
    lowPrices.chartLegend = [];
    lowPrices.chartDates = [];



    lowPrices.parameters = {
        tourType: 0,
        days: 7,
        countryID: 37
    };



    lowPrices.countriesList = [];
    lowPrices.baseCountriesList = {
        avia: [
            {id: 37, name: 'Черногория', code: 'me'},
            {id: 1, name: 'Египет', code: 'eg'},
            {id: 31, name: 'Турция', code: 'tr'},
            {id: 9, name: 'Греция', code: 'gr'},
            {id: 5, name: 'Болгария', code: 'bg'},
            {id: 11, name: 'Испания', code: 'es'},
        ],
        bus: [
            {id: 21, name: 'Украина', code: 'ua'},
            {id: 22, name: 'Франция', code: 'fr'},
            {id: 2, name: 'Чехия', code: 'cz'},
            {id: 7, name: 'Германия', code: 'de'},
            {id: 6, name: 'Польша', code: 'pl'},
            {id: 3, name: 'Венгрия', code: 'hu'},
        ],
        cruise: [
            {id: 12, name: 'Италия', code: 'it'},
            {id: 9, name: 'Греция', code: 'gr'},
            {id: 11, name: 'Испания', code: 'es'},
            {id: 33, name: 'Португалия', code: 'pt'},
            {id: 22, name: 'Франция', code: 'fr'},
            {id: 31, name: 'Турция', code: 'tr'},
        ]
    };



    lowPrices.setTourType = function(type) {
        lowPrices.parameters.tourType = type;
        switch (type) {
            case 0: lowPrices.countriesList = lowPrices.baseCountriesList.avia; break;
            case 1: lowPrices.countriesList = lowPrices.baseCountriesList.bus; break;
            case 2: lowPrices.countriesList = lowPrices.baseCountriesList.cruise; break;
        }

        lowPrices.chartDates = [];
        lowPrices.setCountry(lowPrices.countriesList[0].id);
    };



    lowPrices.setTourDays = function(days) {
        lowPrices.parameters.days = days;

        lowPrices.chartDates = [];
        lowPrices.rebuildLowPrices();
    };



    lowPrices.setCountry = function(countryID) {
        lowPrices.parameters.countryID = countryID;

        lowPrices.chartDates = [];
        lowPrices.rebuildLowPrices();
    };



    lowPrices.initChart = function() {
        switch (lowPrices.parameters.tourType) {
            case 0: lowPrices.countriesList = lowPrices.baseCountriesList.avia; break;
            case 1: lowPrices.countriesList = lowPrices.baseCountriesList.bus; break;
            case 2: lowPrices.countriesList = lowPrices.baseCountriesList.cruise; break;
        }

        var ctx = $('#homeLowPricesChart')[0];
        var chartOptions = jQuery.extend(true, {}, generalChartOptions);
        chartOptions.legend = false;
        chartOptions.onClick = function(e, arr) {
            var activePoint = this.lastActive[0];

            if (activePoint !== undefined) {
                var datasetIndex = activePoint._datasetIndex;
                var index = activePoint._index;
                var airport = this.config.data.datasets[datasetIndex].label;
                var date = this.config.data.labels[index];

                var locTo = baseHostUrl+'/t?countryID='+lowPrices.parameters.countryID+'&tourType='+lowPrices.parameters.tourType+'&dateSince='+date+'.'+(new Date()).getFullYear();
                window.location = locTo;
            }
        };

        lowPrices.chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: chartOptions
        });
    };



    lowPrices.renderChartData = function() {
        var labels = [];
        var allDates = [];
        var origLabels = [];
        var chartData = lowPrices.chart.data;
        chartData.datasets = [];

        //form datasets
        lowPrices.chartData.forEach(function(item, i) {
            if (origLabels.indexOf(item.city_name) != -1) return;
            origLabels.push(item.city_name);

            //set labels from first item
            if (i == 0)
                item.prices.forEach(function(price) {
                    labels.push(formatDateForCharts(price.date));
                });

            //set data
            var newBaseDataset = Object.assign({}, baseChartDataset);

            newBaseDataset.label = item.city_name;
            newBaseDataset.data = [];
            newBaseDataset.borderColor = baseChartColors[chartData.datasets.length];
            newBaseDataset.pointBorderColor = baseChartColors[chartData.datasets.length];
            item.prices.forEach(function(price) {
                if (price.price >= 0)
                    newBaseDataset.data.push(price.price);
                else
                    newBaseDataset.data.push(NaN);
                allDates.push(price.date);
            });

            chartData.datasets.push(newBaseDataset);
        });
        allDates = unique(allDates);

        //build new data
        chartData.labels = labels;
        var duration = 0;
        if (lowPrices.chartDates.length != 0)
            duration = 1000;
        lowPrices.chart.update({duration: duration});

        //create custom legend
        lowPrices.chartLegend = [];
        chartData.datasets.forEach(function(dataset, i) {
            var minData = [];
            dataset.data.forEach(function(item) {if (!isNaN(item)) minData.push(item)});
            lowPrices.chartLegend.push({label: dataset.label, color: baseChartColors[i], price: Math.min(...minData)});
        });

        //style checkboxes
        if (lowPrices.chartDates.length != 0)
            setTimeout(function() {
                console.log('Reinit checkbox!');
                $('.chart-table td input').each(function(i, d) {
                    var obj = $(d);
                    if (!obj.parent('div').hasClass('icheckbox_square-grey')) {
                        var style = obj.attr('style'); //save color
                        obj.iCheck({
                            checkboxClass: 'icheckbox_square-grey',
                            radioClass: 'iradio_square-grey',
                            increaseArea: '-120%' // optional
                        });
                        obj.parent('div').attr('style', style);
                    }
                    obj.parent('div').parent('td')[0].setAttribute('data-clicked', '0');
                });
            }, 200);

        //build dates ruler
        if (lowPrices.chartDates.length == 0)
            lowPrices.organizeDates(allDates);
    };



    //Get data for graph
    lowPrices.rebuildLowPrices = function() {
        var sourceUrl = baseHostUrl+'/model/ajax-handler.php';
        var deferred = $q.defer();

        $http.get(sourceUrl, {params: {countryID: lowPrices.parameters.countryID, days: lowPrices.parameters.days, tourType: lowPrices.parameters.tourType, action: 'GetComparerPrices'}}).then(function success(response) {
            //console.log(response.data);
            lowPrices.chartData = response.data;
            lowPrices.chartDataOriginal = response.data;

            lowPrices.renderChartData();

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    lowPrices.toggleLowPricesChartDataset = function(label) {
        var obj = $('.chart-table td[data-label="' + label + '"]').find('input');

        var chartData = lowPrices.chart;
        chartData.data.datasets.forEach(function(dataset, i) {
            if (dataset.label == label) {
                if (chartData.data.datasets[i].hidden)
                    chartData.data.datasets[i].hidden = false;
                else
                    chartData.data.datasets[i].hidden = true;

                chartData.update({duration: 0});
            }
        });

        setTimeout(function() {
            $('.chart-table td[data-label="' + label + '"]')[0].setAttribute('data-clicked', '1');

            $('.chart-table td input').each(function(i, d) {
                var obj = $(d);
                if (!obj.parent('div').hasClass('icheckbox_square-grey')) {
                    var style = obj.attr('style'); //save color
                    obj.iCheck({
                        checkboxClass: 'icheckbox_square-grey',
                        radioClass: 'iradio_square-grey',
                        increaseArea: '-120%' // optional
                    });
                    obj.parent('div').attr('style', style);
                }

                console.log('Check');
                console.log(obj.parent('div').parent('td'));
                console.log(obj.parent('div').parent('td').attr('data-clicked'));
                if (obj.parent('div').parent('td').data('clicked') == '1') {
                    obj.parent('div').parent('td')[0].setAttribute('data-clicked', '0');

                    chartData.data.datasets.forEach(function(dataset, i) {
                        if (dataset.label == obj.parent('div').parent('td').data('label')) {
                            console.log('checked: ');
                            console.log(chartData.data.datasets[i].hidden);
                            if (chartData.data.datasets[i].hidden)
                                obj.iCheck('uncheck');
                            else
                                obj.iCheck('check');
                        }
                    });

                }
            });
        }, 200);
    };



    lowPrices.organizeDates = function(rawDates) {
        var datesFormatted = [];
        var monthNames = [
            "Январь", "Февраль", "Март",
            "Апрель", "Май", "Июнь", "Июль",
            "Август", "Сентябрь", "Октябрь",
            "Ноябрь", "Декабрь"
        ];

        rawDates.forEach(function(rawDate, i) {
            var date = new Date(rawDate);
            var monthIndex = date.getMonth();

            var label = '';
            if (date.getFullYear() != (new Date()).getFullYear())
                label = monthNames[monthIndex]+', '+date.getFullYear();
            else
                label = monthNames[monthIndex];

            if (datesFormatted.indexOf(label) == -1)
                datesFormatted.push(label);
        });

        lowPrices.chartDates = datesFormatted;

        //init first month
        setTimeout(function() {
            if ($('#home-low-prices-dates').length && lowPrices.chartDates.length)
                lowPrices.switchToDate(datesFormatted[0], '#home-low-prices-dates');
        }, 1000);
    };



    lowPrices.switchToDate = function(dateLabel, curSelector) {
        showLoader();

        //select item and move slider
        $(curSelector).find('a.active').removeClass('active');
        $(curSelector).find('a[data-date="'+dateLabel+'"]').addClass('active');
        var positionLeft = $(curSelector).find('a.active').parent('li').position().left;
        $(curSelector).siblings('.dateSlider').find('.dateHandle').css('left', positionLeft+'px');

        var monthNames = [
            "Январь", "Февраль", "Март",
            "Апрель", "Май", "Июнь", "Июль",
            "Август", "Сентябрь", "Октябрь",
            "Ноябрь", "Декабрь"
        ];

        var dateFormatted = dateLabel.split(', ');
        var dateObj = {};
        if (dateFormatted.length == 1) {
            dateObj['month'] = monthNames.indexOf(dateFormatted[0]);
            dateObj['year'] = (new Date()).getFullYear();
        } else {
            dateObj['month'] = monthNames.indexOf(dateFormatted[0]);
            dateObj['year'] = dateFormatted[1];
        }

        var originalData = copy(lowPrices.chartDataOriginal);

        originalData.forEach(function(item, i) {
            var newPrices = item.prices;

            var foundOldDate = true;
            while (foundOldDate) {
                foundOldDate = false;
                newPrices.forEach(function(price, j) {
                    var pdate = new Date(price.date);
                    if (!foundOldDate && (pdate.getFullYear() != dateObj.year || pdate.getMonth() != dateObj.month)) {
                        foundOldDate = true;
                        newPrices.splice(j, 1);
                    }
                });
            }
            originalData[i].prices = newPrices;
        });
        lowPrices.chartData = originalData;

        lowPrices.renderChartData();
        hideLoader();
    };



    //on load
    //lowPrices.initChart();
    //lowPrices.rebuildLowPrices();
});
<?php
$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$site = $protocol.$_SERVER['HTTP_HOST'];

if (!defined('DIR_CATEGORY_IMG')) define('DIR_CATEGORY_IMG', $_SERVER['DOCUMENT_ROOT'].'/view/assets/img/');
if (!defined('DIR_PATH')) define('DIR_PATH', $site.'/uk/');// SLASHES ARE IMPORTANT! If no directory - leave slash
if (!defined('DIR_IMG')) define('DIR_IMG', $site.'/view/assets/img/'); // Better no to change. Included in searches JS folder.
if (!defined('DIR_CSS')) define('DIR_CSS', $site.'/view/assets/css/');
if (!defined('DIR_JS')) define('DIR_JS', $site.'/view/assets/js/'); // Better no to change. Included in: register.js
if (!defined('DIR_ASSETS')) define('DIR_ASSETS', $site.'/view/assets/');
if (!defined('DIR_SRC')) define('DIR_SRC', $site.'/view/assets/src/');
if (!defined('DIR_LANGUAGE')) define('DIR_LANGUAGE', 'lang/');
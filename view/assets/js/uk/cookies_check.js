$(document).on('click', '.accept_cookies', function () {
    "use strict";
	$('.cookies-message').fadeOut("slow");
	var future = new Date();
	future.setDate(future.getDate() + 30);
	document.cookie = "cookie_agree=1; expires=" + future + "; path=/";
});
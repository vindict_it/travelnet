<?php

if (!defined('page_bank_header')) define('page_bank_header', 'Оплата частинами від ПриватБанку');
if (!defined('page_bank_desc')) define('page_bank_desc', 'Якщо Ви клієнт ПриватБанку і хочете купити тур за допомогою послуги "Оплата частинами", ознайомтесь з умовами перш, ніж робити покупку.');
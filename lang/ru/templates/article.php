<?php

if (!defined('blog_share')) define('blog_share', 'Поделиться статьей');
if (!defined('blog_title_word')) define('blog_title_word', 'Статья');
if (!defined('blog_read_word')) define('blog_read_word', 'Читать');
if (!defined('blog_from')) define('blog_from', 'Статья из блога');
if (!defined('blog_details')) define('blog_details', 'Подробнее');
if (!defined('blog_articles')) define('blog_articles', 'Другие статьи');
if (!defined('blog_comments')) define('blog_comments', 'Комментарии');
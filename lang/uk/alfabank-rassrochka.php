<?php

if (!defined('page_bank_header')) define('page_bank_header', 'Розстрочка платежів від Альфа банку');
if (!defined('page_bank_desc')) define('page_bank_desc', 'Умови розстрочки для клієнтів Альфа Банку. Перш ніж оплачувати замовлення, будь ласка, переконайтеся, що Ви підходите для видачі кредиту.');
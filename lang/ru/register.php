<?php

// login page info
if (!defined('page_main_header')) define('page_main_header', 'Новый аккаунт');
if (!defined('page_register')) define('page_register', 'Регистрация');
if (!defined('page_new_register')) define('page_new_register', 'Регистрируя новый аккаунт, Вы соглашаетесь с');
if (!defined('page_politics')) define('page_politics', 'Политикой конфиденциальности');
if (!defined('page_account_type')) define('page_account_type', 'Выберите тип аккаунта');
if (!defined('page_account_personal')) define('page_account_personal', 'Персональный');
if (!defined('page_account_agent')) define('page_account_agent', 'Турагент');
if (!defined('page_name')) define('page_name', 'Ваше имя');
if (!defined('page_name_plc')) define('page_name_plc', 'Александр');
if (!defined('page_name_error')) define('page_name_error', 'Имя должно быть от 2 до 64 символов');
if (!defined('page_email')) define('page_email', 'Ваш Email');
if (!defined('page_email_plc')) define('page_email_plc', 'yourmail@example.com');
if (!defined('page_email_error')) define('page_email_error', 'E-mail введен некорректно');
if (!defined('page_password')) define('page_password', 'Пароль');
if (!defined('page_password_error')) define('page_password_error', 'Пароль должен содержать хотя бы одну букву, цифру и быть длиннее 6-ти символов');
if (!defined('page_password_repeat')) define('page_password_repeat', 'Повторите пароль');
if (!defined('page_password_repeat_error')) define('page_password_repeat_error', 'Пароль должен содержать хотя бы одну букву, цифру и быть длиннее 6-ти символов');
if (!defined('page_phone')) define('page_phone', 'Номер телефона');
if (!defined('page_phone_error')) define('page_phone_error', 'Введите телефон в формате своей страны');
if (!defined('page_error_name')) define('page_error_name', 'Вы не указали Имя');
if (!defined('page_error_no_email')) define('page_error_no_email', 'Вы не ввели Email-адрес');
if (!defined('page_error_email')) define('page_error_email', 'Ваш Email введен с ошибками');
if (!defined('page_error_email_exists')) define('page_error_email_exists', 'Пользователь с этим Email уже зарегистрирован');
if (!defined('page_error_password')) define('page_error_password', 'Вы не ввели пароль');
if (!defined('page_error_password_repeat')) define('page_error_password_repeat', 'Повторно введенный пароль не совпадает');
if (!defined('email_confirm')) define('email_confirm', 'Подтверждение email на');
if (!defined('email_text')) define('email_text', 'Здравствуйте, %s!<br/>Чтобы подтвердить Ваш Email-адрес, пожалуйста, перейдите по ссылке ниже:<br/><br/></p><p style="text-align:center;">
									<a class="btn btn-primary btn-lg raised" href="%sactivation?token=%s&email=%s">Активировать</a><br/><br/></p><p>
									В противном случае, если Вы не регистрировались на TravelNet Tour, пожалуйста, удалите это сообщение.<br/>
									Если у Вас есть предложения, замечания или вопросы касательно сайта, 
									пожалуйста, свяжитесь с нами через <a href="%scontacts">Форму обратной связи</a> или другим удобным образом.<br/><br/>
									<small>Сообщение было сгенерировано автоматически, пожалуйста, не отвечайте на него.<br/>
									С наилучшими пожеланиями,<br/>Команда TravelNet Tour</small>');
if (!defined('register_success')) define('register_success', 'Вы успешно зарегистрировались в TravelNet Tour! Чтобы войти в Личный кабинет, подтвердите свою электронную почту. Инструкция в письме на Вашей почте.');
if (!defined('register_error')) define('register_error', 'Ой... Кажется, мы не можем создать Вам Аккаунт. Скорее всего на сайте ведуться технические работы. Пожалуйста, попробуйте пройти регистрацию ещё раз.');

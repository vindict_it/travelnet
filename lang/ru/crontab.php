<?php

if (!defined('cancel_order_header')) define('cancel_order_header', 'Ваш заказ был автоматически отменен');
if (!defined('cancel_order_text')) define('cancel_order_text', 'Ваш заказ №%s, который Вы создали %s был отменен в системе автоматически по 
причине того, что Вы его не завершили. Бронирование цены у туроператора отменено. Если Вы хотите сделать заказ, создайте его, перейдя в
<br/><br/></p><p style="text-align:center;"><a href="%s" class="btn btn-primary btn-lg raised">Личный кабинет</a><br/><br/></p><p>.');


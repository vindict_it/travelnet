<?php

if (!defined('order_flight_step1')) define('order_flight_step1', 'Бронювання');
if (!defined('order_flight_step2')) define('order_flight_step2', 'Підтвердження');
if (!defined('order_flight_step3')) define('order_flight_step3', 'Оплата');
if (!defined('order_flight_step4')) define('order_flight_step4', 'Видача документів');
if (!defined('order_method')) define('order_method', 'Оберіть спосіб оплати');
if (!defined('order_method_card')) define('order_method_card', 'Кредитною карткою (Visa / Mastercard)');
if (!defined('order_method_credit')) define('order_method_credit', 'Оплата в кредит');
if (!defined('order_method_many_payments')) define('order_method_many_payments', 'Оплата в розстрочку');
if (!defined('order_method_emoney')) define('order_method_emoney', 'Електронні платежі (Portmone, LiqPay та інші)');
if (!defined('order_method_bank')) define('order_method_bank', 'Оплата безготівкового рахунку через відділення банку');
if (!defined('order_method_agent')) define('order_method_agent', 'Оплата готівкою через турагента');
if (!defined('order_method_courier')) define('order_method_courier', 'Оплата готівкою через кур\'єра');
if (!defined('order_clients')) define('order_clients', 'Ваше замовлення');
if (!defined('order_price')) define('order_price', 'Ціна туру');
if (!defined('order_price_subheader')) define('order_price_subheader', 'З урахуванням всіх платежів');
if (!defined('order_price_discount')) define('order_price_discount', 'Знижка');
if (!defined('order_price_payment')) define('order_price_payment', 'Оплата туру');
if (!defined('order_do_payment')) define('order_do_payment', 'Сплатити');
if (!defined('order_ssl')) define('order_ssl', 'З\'єднання захищене SSL-шифруванням');
if (!defined('order_one_step')) define('order_one_step', 'Ви вже за крок до відпочинку!');
if (!defined('order_loading')) define('order_loading', 'завантаження даних');
if (!defined('order_hotel_info')) define('order_hotel_info', 'Інформація про готель');
if (!defined('order_tour_info')) define('order_tour_info', 'Інформація про тур');
if (!defined('order_flight_info')) define('order_flight_info', 'Інформація про перельот');
if (!defined('order_route_info')) define('order_route_info', 'Інформація про маршрут');
if (!defined('order_night1')) define('order_night1', 'нiч');
if (!defined('order_night2')) define('order_night2', 'ночi');
if (!defined('order_night3')) define('order_night3', 'ночей');
if (!defined('order_days1')) define('order_days1', 'день');
if (!defined('order_days2')) define('order_days2', 'днi');
if (!defined('order_days3')) define('order_days3', 'днiв');
if (!defined('order_flight_to')) define('order_flight_to', 'Туди');
if (!defined('order_flight_from')) define('order_flight_from', 'Назад');
if (!defined('order_clients')) define('order_clients', 'Інформація про клієнтів');
if (!defined('order_client_tourist')) define('order_client_tourist', 'Турист');
if (!defined('order_client_buyer')) define('order_client_buyer', 'Покупець');
if (!defined('order_client_buyer')) define('order_client_buyer', 'Інформація про клієнтів');
if (!defined('order_client_lname')) define('order_client_lname', 'Прізвище');
if (!defined('order_client_lname_placeholder')) define('order_client_lname_placeholder', 'Іванов');
if (!defined('order_client_fname')) define('order_client_fname', 'Iм\'я');
if (!defined('order_client_fname_placeholder')) define('order_client_fname_placeholder', 'Іван');
if (!defined('order_client_email')) define('order_client_email', 'E-mail');
if (!defined('order_client_phone')) define('order_client_phone', 'Номер телефону');
if (!defined('order_tourist_passport')) define('order_tourist_passport', 'За паспортом');
if (!defined('order_tourist_date')) define('order_tourist_date', 'Дата народження');
if (!defined('order_tourist_national')) define('order_tourist_national', 'Громадянство');
if (!defined('order_tourist_no_pass')) define('order_tourist_no_pass', 'Номер паспорта');
if (!defined('order_tourist_pass_dates')) define('order_tourist_pass_dates', 'Термін дії');
if (!defined('payment_not_listed')) define('payment_not_listed', 'не вказано');

if (!defined('month_1')) define('month_1', 'cічня');
if (!defined('month_2')) define('month_2', 'лютого');
if (!defined('month_3')) define('month_3', 'березня');
if (!defined('month_4')) define('month_4', 'квітня');
if (!defined('month_5')) define('month_5', 'травня');
if (!defined('month_6')) define('month_6', 'червня');
if (!defined('month_7')) define('month_7', 'липня');
if (!defined('month_8')) define('month_8', 'серпня');
if (!defined('month_9')) define('month_9', 'вересня');
if (!defined('month_10')) define('month_10', 'жовтня');
if (!defined('month_11')) define('month_11', 'листопада');
if (!defined('month_12')) define('month_12', 'грудня');

if (!defined('order_tour_img_alt')) define('order_tour_img_alt', 'Обирайте тур "%s" на сайтi %s');
if (!defined('order_cities')) define('order_cities', 'мiст');

if (!defined('adults_text')) define('adults_text', 'дорослих');
if (!defined('adult_text')) define('adult_text', 'дорослий');

if (!defined('auth_to_continue')) define('auth_to_continue', 'Авторизуйтесь, щоб продовжити');
if (!defined('enter_card')) define('enter_card', 'Введіть номер карти');
if (!defined('enter_holder_name')) define('enter_holder_name', 'Введіть власника карти');
if (!defined('enter_expires')) define('enter_expires', 'Введіть поле "закінчується"');
if (!defined('enter_cvc')) define('enter_cvc', 'Введіть cvc-код');
if (!defined('bank_not_found')) define('bank_not_found', 'Виберіть банк зі списку');

if (!defined('bank_privatbank')) define('bank_privatbank', 'Оплата через ПриватБанк на сайті TravelNet Tour');
if (!defined('bank_privatbank_title')) define('bank_privatbank_title', 'Оплатити через ПриватБанк');
if (!defined('pay_p24')) define('pay_p24', 'Оплата через Приват24');
if (!defined('pay_p24_alt')) define('pay_p24_alt', 'Оплата на сайті TravelNet Tour через Приват24');
if (!defined('pb_pieces')) define('pb_pieces', 'Оплата частинами');
if (!defined('pay_pb_pieces')) define('pay_pb_pieces', 'Оплата частинами ПриватБанк');
if (!defined('pay_pb_pieces_alt')) define('pay_pb_pieces_alt', 'Оплата на сайтi TravelNet Tour частинами у ПриватБанку');
if (!defined('pb_cahsbox')) define('pb_cahsbox', 'Каса');
if (!defined('pay_pb_cahsbox')) define('pay_pb_cahsbox', 'Оплата у касi ПриватБанку');
if (!defined('pay_pb_cahsbox_alt')) define('pay_pb_cahsbox_alt', 'Оплата заказа TravelNet Tour у касi ПриватБанку');

if (!defined('bank_alfabank')) define('bank_alfabank', 'Оплата через Альфа Банк на сайтi TravelNet Tour');
if (!defined('bank_alfabank_title')) define('bank_alfabank_title', 'Оплатити через Альфа Банк');
if (!defined('ab_acq')) define('ab_acq', 'Оплатити рахунок');
if (!defined('pay_ab_acq')) define('pay_ab_acq', 'Оплатити рахунок в Альфа Банк');
if (!defined('pay_ab_acq_alt')) define('pay_ab_acq_alt', 'Оплата рахунок на сайтi Альфа Банк через власний додаток');
if (!defined('ab_acq_online')) define('ab_acq_online', 'Оплатити онлайн');
if (!defined('pay_ab_acq_online')) define('pay_ab_acq_online', 'Оплатити онлайн в Альфа Банк');
if (!defined('pay_ab_acq_online_alt')) define('pay_ab_acq_online_alt', 'Оплата онлайн на сайтi TravelNet Tour через Альфа Банк');

if (!defined('pay_ab_acq_desc')) define('pay_ab_acq_desc', 'Оплата туристичних послуг за Замовлення #%s на сайтi TravelNet Tour.');

if (!defined('bank_monobank')) define('bank_monobank', 'Оплата через МоноБанк на сайтi TravelNet Tour');
if (!defined('bank_monobank_title')) define('bank_monobank_title', 'Оплатити через МоноБанк');
if (!defined('mono_pieces')) define('mono_pieces', 'Оплата частинами');
if (!defined('pay_mono_pieces')) define('pay_mono_pieces', 'Оплата частинами МоноБанк');
if (!defined('pay_mono_pieces_alt')) define('pay_mono_pieces_alt', 'Оплата на сайтi TravelNet Tour частинами у МоноБанку');
if (!defined('mono_mobile')) define('mono_mobile', 'Сплатити');
if (!defined('pay_mono_mobile')) define('pay_mono_mobile', 'Оплата в мобiльному додатку МоноБанк');
if (!defined('pay_mono_mobile_alt')) define('pay_mono_mobile_alt', 'Оплата на сайтi TravelNet Tour в мобiльному додатку МоноБанку');

if (!defined('cashbox_info_main_header')) define('cashbox_info_main_header', 'Оплата у каскасiсе');
if (!defined('cashbox_info_header')) define('cashbox_info_header', 'Як оплатити заказ у касi?');
if (!defined('cashbox_info_text')) define('cashbox_info_text', 'Для того, щоб оплатити замовлення в касі ПриватБанку, Вам необхідно прийти в 
                                    будь-якийнайближчого відділення. У касі необхідно вказати, що Ви хочете оплатити послуги компанії TravelNet 
                                    Tour іназвати свій код платника податків (ІПН), який Ви вказали заповнюючи замовлення. Ваше замовлення буде 
                                    знайдено автоматично, касир його додатково озвучить, після чого запропонує оплатити. Після цього - оплатiть 
                                    замовлення в повному обсязі. Після успішної оплапти, Ваше замовлення на сайті автоматично перейде в статус 
                                    "Оплачено" і Ваші дані будуть передані туроператору для підтвердження і видачі документів.');

if (!defined('pp_product_name')) define('pp_product_name', 'Тур #%s "%s" на TravelNet Tour');
if (!defined('transaction_failed')) define('transaction_failed', 'Банку не вдалося створити платіж, спробуйте повторити пізніше або перезавантажити сторінку.');
if (!defined('transaction_expired')) define('transaction_expired', 'Ваш запит був відхилений банком, спробуйте повторити пізніше або перезавантажити сторінку.');
if (!defined('db_fail')) define('db_fail', 'Сталася помилка на сайті, можливо ведуться регламентні роботи. Будь ласка, перезавантажте сторінку.');



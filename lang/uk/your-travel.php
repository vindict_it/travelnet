<?php

if (!defined('order_flight_step1')) define('order_flight_step1', 'Бронювання');
if (!defined('order_flight_step2')) define('order_flight_step2', 'Підтвердження');
if (!defined('order_flight_step3')) define('order_flight_step3', 'Оплата');
if (!defined('order_flight_step4')) define('order_flight_step4', 'Видача документів');
if (!defined('order_download_all')) define('order_download_all', 'Завантажити всі документи');
if (!defined('order_download')) define('order_download', 'Завантажити документ');
if (!defined('order_print')) define('order_print', 'Роздрукувати документ');
if (!defined('order_ready')) define('order_ready', 'Ваші документи готові');
if (!defined('order_ready_btn')) define('order_ready_btn', 'Для їх скачування натисніть на кнопку нижче');
if (!defined('order_bank')) define('order_bank', 'Виписка з банку');
if (!defined('order_avia')) define('order_avia', 'Авіаквитки');
if (!defined('order_insurance')) define('order_insurance', 'Страховка');
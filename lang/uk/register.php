<?php

// login page info
if (!defined('page_main_header')) define('page_main_header', 'Новий акаунт');
if (!defined('page_register')) define('page_register', 'Реєстрація');
if (!defined('page_new_register')) define('page_new_register', 'Реєструючи новий акаунт, Ви погоджуєтеся з');
if (!defined('page_politics')) define('page_politics', 'Політикою конфіденційності');
if (!defined('page_account_type')) define('page_account_type', 'Виберіть тип аккаунта');
if (!defined('page_account_personal')) define('page_account_personal', 'Персональний');
if (!defined('page_account_agent')) define('page_account_agent', 'Турагент');
if (!defined('page_name')) define('page_name', 'Ваше ім\'я');
if (!defined('page_name_plc')) define('page_name_plc', 'Олександр');
if (!defined('page_name_error')) define('page_name_error', 'Ім\'я повинно бути від 2 до 64 символів');
if (!defined('page_email')) define('page_email', 'Ваш Email');
if (!defined('page_email_plc')) define('page_email_plc', 'yourmail@example.com');
if (!defined('page_email_error')) define('page_email_error', 'E-mail введений некоректно');
if (!defined('page_password')) define('page_password', 'Пароль');
if (!defined('page_password_error')) define('page_password_error', 'Пароль повинен містити хоча б одну букву, цифру і бути довшим 6-ти символів');
if (!defined('page_password_repeat')) define('page_password_repeat', 'Повторіть пароль');
if (!defined('page_password_repeat_error')) define('page_password_repeat_error', 'Пароль повинен містити хоча б одну букву, цифру і бути довшим 6-ти символів');
if (!defined('page_phone')) define('page_phone', 'Номер телефону');
if (!defined('page_phone_error')) define('page_phone_error', 'Введіть телефон в форматі своєї країни');
if (!defined('page_error_name')) define('page_error_name', 'Ви не вказали Ім\'я');
if (!defined('page_error_no_email')) define('page_error_no_email', 'Ви не ввели Email-адресу');
if (!defined('page_error_email')) define('page_error_email', 'Ваш Email введений з помилками');
if (!defined('page_error_email_exists')) define('page_error_email_exists', 'Користувач з цим Email вже зареєстрований');
if (!defined('page_error_password')) define('page_error_password', 'Ви не ввели пароль');
if (!defined('page_error_password_repeat')) define('page_error_password_repeat', 'Повторно ведений пароль не збігається');
if (!defined('email_confirm')) define('email_confirm', 'Підтвердження Email на');
if (!defined('email_text')) define('email_text', 'Привіт %s! <br/> Щоб підтвердити Вашу Email-адресу, будь ласка, перейдіть по посиланню нижче:<br/><br/></p><p style="text-align:center;">
												<a class="btn btn-primary btn-lg raised" href="%sactivation?token=%s&email=%s">Активувати</a><br/><br/></p><p>
												В іншому випадку, якщо Ви не реєструвалися на TravelNet Tour, будь ласка, видаліть це повідомлення.<br/>
												Якщо у Вас є пропозиції, зауваження або питання щодо сайту, будь ласка, зв\'яжіться з нами через <a href="%scontacts"> Форму зворотнього зв\'язку</a> або іншим зручним способом.<br/><br/>
												<small>Повідомлення було згенеровано автоматично, будь ласка, не відповідайте на нього. <br/>
												З найкращими побажаннями,<br/> Команда TravelNet Tour</small>');
if (!defined('register_success')) define('register_success', 'Ви успішно зареєструвалися в Travelnet Tours! Щоб увійти в Особистий кабінет, підтвердіть свою електронну пошту. Інструкція в листі на Вашу пошту.');
if (!defined('register_error')) define('register_error', 'Ой ... Здається, ми не можемо створити Вам Аккаунт. Швидше за все на сайті ведуться технічні роботи. Будь ласка, спробуйте пройти реєстрацію ще раз.');

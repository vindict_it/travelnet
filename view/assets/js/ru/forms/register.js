var captcha_required = false;
var repeatPassword = $('#repeat_password');
var repeatPasswordBull = false;
var toggler = 'personal';

$(document).on('click', '.fieldLabel-swipe', function(ev) {
	'use strict';
	ev.preventDefault();
	
	if($(this).attr('data-id') == 'agent') {
		$('.mask-swipe').text('Турагент');
		$('.mask-swipe').css('width', $('label[data-id="agent"]').outerWidth());
		var transform = $('.wrapper-swipe').outerWidth() - $('label[data-id="agent"]').outerWidth();
		$('.mask-swipe').css('transform', 'translate('+transform+'px)');
		$("#toggler_agent")[0].checked = true;
		toggler = 'agent';
	} else {
		$('.mask-swipe').text('Персональный');
		$('.mask-swipe').css('width', $('label[data-id="personal"]').outerWidth());
		$('.mask-swipe').css('transform', 'translate(0px)');
		$("#toggler_tourist")[0].checked = true;
		toggler = 'personal';
	}
});

$(document).on('submit', '#register-form', function(ev) {
	'use strict';
	ev.preventDefault();
	
	if(repeatPasswordBull && captcha_required) {
		$('.err-msg').find('.error-message-text').html('');
		$('.err-msg').hide();
		$('#repeat_password').parent().find('.invalid-feedback').text('Пароль должен содержать хотя бы одну букву, цифру и быть длиннее 6-ти символов');

		$.ajax({
			url: window.location.origin + '/model/ajax-handler.php',
			type: 'post',
			dataType: 'json',
			data: {reg_data: $("#register-form").serialize() + '&toggler_type=' + toggler, lang: 'ru', action: 'start_register'},
			success: function(response) {
				$('.err-msg').attr('style', 'display:none!important');
				$('.error-message-text').html('');
				if(response.status == 'true') {
					window.location.href = window.location.origin + '/login';
				} else {
					$('.error-message-text').html(response.error);
					$('.err-msg').attr('style', 'display:flex!important');
				}
			}
		});
	} else if(!repeatPasswordBull) {
		$('.err-msg').find('.error-message-text').html('Пароли не совпадают. Пожалуйста, проверьте правильность ввода, и попробуйте ещё раз.');
		$('.err-msg').fadeIn('slow').css('display', 'flex');
		$('#repeat_password').parent().find('.invalid-feedback').text('Пароли не совпадают');
		document.getElementById('repeat_password').setCustomValidity('Пароли не совпадают');
		$([document.documentElement, document.body]).animate({
			scrollTop: $(".err-msg").offset().top - 80
		}, 1000);
	} else if(!captcha_required) {
		$('.err-msg').find('.error-message-text').html('Вы не прошли валидацию reCaptcha. Пожалуйста, подтвердите, что Вы не робот.');
		$('.err-msg').fadeIn('slow').css('display', 'flex');
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#captcha-container").offset().top - 80
		}, 1000);
	}
});

$('body').on("keyup", '#repeat_password', function(){
	if($('#repeat_password').val().length != 0){
		if($('#repeat_password').val() != $('#password').val()) {
			repeatPasswordBull = false;
		} else {
			repeatPasswordBull = true;
		}
	} else {
		repeatPasswordBull = false;
	}
});

(function() {
  'use strict';
  window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
		 $([document.documentElement, document.body]).animate({
				scrollTop: $(".form-control:invalid").offset().top - 100
			}, 1000);
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

function onloadCaptcha() {
	grecaptcha.render('captcha-container', {
		'theme': 'light',
		'sitekey': '6Lf-xngUAAAAAGNSp8SqD_pHd6fA5BG4v2vxCres',
		'expired-callback': captcha_expired,
		'callback': captcha_callback
	});
}

function captcha_callback() {
	captcha_required = true;
}

function captcha_expired() {
	captcha_required = false;
}
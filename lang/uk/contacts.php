<?php

//Contacts page
define("page_main_header", "Наші контакти");
define("page_emails", "Напишіть нам на Email");
define("page_phones", "Зателефонуйте нам");
define("page_socials", "Ми в соціальних мережах");
define("page_feedback", "Форма зворотного зв'язку");
define("page_message", "Ваше повідомлення");
define("page_name", "Ваше ім'я");
define("page_email", "Ваш Email");
define("page_auth", "Чим ми можемо Вам допомогти");
define("page_text_placeholder", "Я хочу сказати");
define("page_send", "Надіслати");
define("page_regards", "З найкращими побажаннями, Команда підтримки Travelnet Tour");
define("page_callcenter", "Графік роботи колл-центру");
define("page_from", "з");
define("page_to", "до");
define("page_saturday", "В суботу");
define("page_sunday", "В неділю");

define("page_code", "код ЄДРПОУ");
define("page_company", "ТОВ");
define("page_address", "Адресa: Україна, м. Харків, пр. Науки 31А, 61072");
$(document).ready(function () {
    "use strict";

    var userNamePattern = /^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i;
    var username = $('input[name=login-Email]');
    var usernameBull = false;

    username.blur(function(){
        if(username.val().length != 0){
            $('input[name=login-Email] + .color--error').text('');
            if(username.val().search(userNamePattern) == 0){
                $('input[name=login-Email]').parent().next('.color--error').text('');
                $('input[name=login-Email]').parent().next('.color--error').fadeOut('fast');
                $('input[name=login-Email]').parent('.clickable').removeClass('has-error');
            }else{
                $('input[name=login-Email]').parent().next('.color--error').text('Введіть правильний Email');
				$('input[name=login-Email]').parent().next('.color--error').fadeIn('fast');
                $('input[name=login-Email]').parent('.clickable').addClass('has-error');
            }
        }else{
            $('input[name=login-Email]').parent().next('.color--error').text('Email не може бути порожнім');
			$('input[name=login-Email]').parent().next('.color--error').fadeIn('fast');
            $('input[name=login-Email]').parent('.clickable').addClass('has-error');
        }
        if(username.parents('.clickable').hasClass('has-error')) {
            usernameBull = false;
        } else {
            usernameBull = true;
        }
        if(usernameBull && passwordBull) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });

    var password = $('input[name=login-Password]');
    var passwordBull = false;

    password.blur(function(){
        if(password.val().length != 0){
            $('input[name=login-Password]').parent('.clickable').removeClass('has-error');
            if(password.val().length < 6){
                $('input[name=login-Password]').parent().next('.color--error').text('Пароль не може бути коротше 6 символів');
				$('input[name=login-Password]').parent().next('.color--error').fadeIn('fast');
                $('input[name=login-Password]').parent('.clickable').addClass('has-error');

            }else{
                $('input[name=login-Password]').parent('.clickable').removeClass('has-error');
                $('input[name=login-Password]').parent().next('.color--error').text('');
				$('input[name=login-Password]').parent().next('.color--error').fadeOut('fast');
            }
        }else{
            $('input[name=login-Password]').parent().next('.color--error').text('Пароль не може бути порожнім');
			$('input[name=login-Password]').parent().next('.color--error').fadeIn('fast');
            $('input[name=login-Password]').parent('.clickable').addClass('has-error');
        }

        if(password.parents('.clickable').hasClass('has-error')) {
            passwordBull = false;
        } else {
            passwordBull = true;
        }
        if(usernameBull && passwordBull) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });

});
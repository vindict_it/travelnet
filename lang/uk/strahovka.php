<?php

if (!defined('insurance_header')) define('insurance_header', '����������� �������');
if (!defined('insurance_subheader')) define('insurance_subheader', '��� ���������� ������� ��������� ��� ���������� ��������, ���� �����, 
																	��������� ���� �����. �������� �������� ��� ����� � ����� ���������� �� 
																	������� ������, ���������� �� ����� ���������. �� ������� ��\'������� � 
																	���������� ��� ��������� ����-���� ������� � ������. ���������� 
																	���������� �� �������� �������� 1-3 ������� ����.');
if (!defined('insurance_session')) define('insurance_session', '�� ������� ��������������');
if (!defined('insurance_session_text')) define('insurance_session_text', '��� ����������� ������� ��������� ��� �������� ������,
																		��� ��������� ����� � ��� ��������� ������, ���� �� ������ ����������
																		������� ���������� � �� �� ������� ����������� ���������� ���������.');

if (!defined('insurance_types')) define('insurance_types', '������� ��� �����������');	
if (!defined('insurance_agreement')) define('insurance_agreement', '������ �����������');	

if (!defined('insurance_data')) define('insurance_data', '���� ��� ����������');	
if (!defined('insurance_directions')) define('insurance_directions', '�������� (���� �������)');	
if (!defined('insurance_terms')) define('insurance_terms', '����� �����������');	
if (!defined('insurance_date_start')) define('insurance_date_start', '���� ������� �������');	
if (!defined('insurance_date_end')) define('insurance_date_end', '���� ��������� �������');	

if (!defined('insurance_people')) define('insurance_people', '������������ �����');	
if (!defined('insurance_people_add')) define('insurance_people_add', '������ ������');	
if (!defined('insurance_people_insurance')) define('insurance_people_insurance', '�������������');	
if (!defined('insurance_people_birth')) define('insurance_people_birth', '���� ����������');	

if (!defined('insurance_calculate')) define('insurance_calculate', '����������� �������');																	  
if (!defined('insurance_dogovor')) define('insurance_dogovor', '�������� ������');	
																  
if (!defined('event_error')) define('event_error', '�����, �� �� ��������� ����� � �� ��������� �� ������. ���� �����, �������� �������� �����, ��� �� ������ ��������, ��� �� �� ������ �������������.');																	  
if (!defined('type_error')) define('type_error', '�����, �� �� ������ ��� ���������, ��� ��� ��������.');																	  
if (!defined('direction_error')) define('direction_error', '�����, �� �� ����� �������� ���� �������. �i� �����ty ���� �� 0 �� 180 �������.');																	  
if (!defined('dates_error')) define('dates_error', '�����, �� �� ������� ���� ���� �������.');																	  
if (!defined('db_error')) define('db_error', '������� �� ����������, ��� � ��� �� ������ ������� �������. ������ �� ���, �������� ������������� ������. ���� �����, ��������� ��������������� �������.');																	  
if (!defined('no_auth')) define('no_auth', '��� �������� ����� ����������� ��� ��������� �������������� �� ����.');																	  
$(document).ready(function () {
    "use strict";

    var emailPattern = /^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i;
    var email = $('input[name=recover-Email]');
    var emailBull = false;

    email.blur(function(){
        if(email.val().length != 0){
            $('input[name=recover-Email] + .color--error').text('');
            if(email.val().search(emailPattern) == 0){
                $('input[name=recover-Email]').parent().next('.color--error').text('');
                $('input[name=recover-Email]').parent().next('.color--error').fadeOut('fast');
                $('input[name=recover-Email]').parent('.clickable').removeClass('has-error');
            }else{
                $('input[name=recover-Email]').parent().next('.color--error').text('Введіть правильний Email');
				$('input[name=recover-Email]').parent().next('.color--error').fadeIn('fast');
                $('input[name=recover-Email]').parent('.clickable').addClass('has-error');
            }
        }else{
            $('input[name=recover-Email]').parent().next('.color--error').text('Email не може бути порожнім');
			$('input[name=recover-Email]').parent().next('.color--error').fadeIn('fast');
            $('input[name=recover-Email]').parent('.clickable').addClass('has-error');
        }
        if(email.parents('.clickable').hasClass('has-error')) {
            emailBull = false;
        } else {
            emailBull = true;
        }
        if(emailBull) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });

});
var captcha_required = false;
var repeatPassword = $('#repeat_password');
var repeatPasswordBull = false;

$(document).on('click', '.fieldLabel-swipe', function(ev) {
	'use strict';
	ev.preventDefault();
	
	if($(this).attr('data-id') == 'agent') {
		$('.mask-swipe').text('Турагент');
		$('.mask-swipe').css('width', $('label[data-id="agent"]').outerWidth());
		var transform = 260 - $('label[data-id="agent"]').outerWidth();
		$('.mask-swipe').css('transform', 'translate('+transform+'px)');
	} else {
		$('.mask-swipe').text('Персональний');
		$('.mask-swipe').css('width', $('label[data-id="personal"]').outerWidth());
		$('.mask-swipe').css('transform', 'translate(0px)');
	}
});

$(document).on('submit', '#register-form', function(ev) {
	'use strict';
	ev.preventDefault();
	if(repeatPasswordBull && captcha_required) {
		$('.err-msg').find('.error-message-text').html('');
		$('.err-msg').hide();
		$('#repeat_password').parent().find('.invalid-feedback').text('Пароль повинен містити хоча б одну букву, цифру і бути довшим 6-ти символів');
		
		document.getElementById("register-form").submit();
		location.reload();
	} else if(!repeatPasswordBull) {
		$('.err-msg').find('.error-message-text').html('Паролі не співпадають. Будь ласка, перевірте правильність введення, і спробуйте ще раз.');
		$('.err-msg').fadeIn('slow').css('display', 'flex');
		$('#repeat_password').parent().find('.invalid-feedback').text('Паролі не співпадають');
		document.getElementById('repeat_password').setCustomValidity('Паролі не співпадають');
		$([document.documentElement, document.body]).animate({
			scrollTop: $(".err-msg").offset().top - 80
		}, 1000);
	} else if(!captcha_required) {
		$('.err-msg').find('.error-message-text').html('Ви не пройшли валідацію reCaptcha. Будь ласка, підтвердіть, що Ви не робот.');
		$('.err-msg').fadeIn('slow').css('display', 'flex');
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#captcha-container").offset().top - 80
		}, 1000);
	}
});

$('body').on("keyup", '#repeat_password', function(){
	if($('#repeat_password').val().length != 0){
		if($('#repeat_password').val() != $('#password').val()) {
			repeatPasswordBull = false;
		} else {
			repeatPasswordBull = true;
		}
	} else {
		repeatPasswordBull = false;
	}
});

(function() {
  'use strict';
  window.addEventListener('load', function() {
    var forms = document.getElementsByClassName('needs-validation');
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
		 $([document.documentElement, document.body]).animate({
				scrollTop: $(".form-control:invalid").offset().top - 100
			}, 1000);
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

function onloadCaptcha() {
	grecaptcha.render('captcha-container', {
		'theme': 'light',
		'sitekey': '6Lf-xngUAAAAAGNSp8SqD_pHd6fA5BG4v2vxCres',
		'expired-callback': captcha_expired,
		'callback': captcha_callback
	});
}

function captcha_callback() {
	captcha_required = true;
}

function captcha_expired() {
	captcha_required = false;
}
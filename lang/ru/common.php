<?php

if (!defined('page_banks')) define('page_banks', 'Рассрочка платежа');

if (!defined('pb_alt')) define('pb_alt', 'Оплата частями от ПриватБанка на TravelNet Tour');
if (!defined('pb_title')) define('pb_title', 'Оплата частями ПриватБанк');
if (!defined('pb_header')) define('pb_header', 'Оплата частями ПриватБанк');
if (!defined('alfa_alt')) define('alfa_alt', 'Оплата в рассрочку от Альфа Банка на TravelNet Tour');
if (!defined('alfa_title')) define('alfa_title', 'Рассрочка Альфа Банк');
if (!defined('alfa_header')) define('alfa_header', 'Кредитование Альфа Банка');
if (!defined('mono_alt')) define('mono_alt', 'Оплата частями от Монобанка на TravelNet Tour');
if (!defined('mono_title')) define('mono_title', 'Оплата частями Монобанк');
if (!defined('mono_header')) define('mono_header', 'Оплата частями Монобанк');
if (!defined('any_bank_alt')) define('any_bank_alt', 'Оплата в кредит на TravelNet Tour');
if (!defined('any_bank_title')) define('any_bank_title', 'Кредитование на TravelNet');
if (!defined('any_bank_header')) define('any_bank_header', 'Кредитование при оплате заказа');

if (!defined('page_callback')) define('page_callback', 'Перезвоните мне');
if (!defined('page_email_me')) define('page_email_me', 'Написать нам');

if (!defined('page_support')) define('page_support', 'Поддержка');
if (!defined('page_low_prices')) define('page_low_prices', 'Минимальные цены');
if (!defined('page_low_graph')) define('page_low_graph', 'График цен');
if (!defined('page_directions')) define('page_directions', 'Направления');
if (!defined('directions_soon')) define('directions_soon', 'Скоро откроется');
if (!defined('directions_beach')) define('directions_beach', 'Пляжные туры');
if (!defined('directions_avia')) define('directions_avia', 'Авиа туры');
if (!defined('directions_bus')) define('directions_bus', 'Автобусные туры');
if (!defined('directions_cruise')) define('directions_cruise', 'Круизы');
if (!defined('directions_ski')) define('directions_ski', 'Горнолыжные туры');
if (!defined('directions_sales')) define('directions_sales', 'Акционные туры');
if (!defined('directions_booking')) define('directions_booking', 'Раннее бронирование');
if (!defined('directions_last_moment')) define('directions_last_moment', 'В последний момент');
if (!defined('page_menu')) define('page_menu', 'Меню');
if (!defined('page_details')) define('page_details', 'Подробнее');
if (!defined('page_charters')) define('page_charters', 'Чартеры');
if (!defined('page_services')) define('page_services', 'Сервисы');
if (!defined('services_about')) define('services_about', 'О Нас');
if (!defined('services_offer')) define('services_offer', 'Договор оферты');
if (!defined('services_insurance')) define('services_insurance', 'Страхование');
if (!defined('services_best_offers')) define('services_best_offers', 'Лучшие предложения');
if (!defined('services_operators_sales')) define('services_operators_sales', 'Акции туроператоров');
if (!defined('services_countries')) define('services_countries', 'Страноведение');
if (!defined('page_favourite')) define('page_favourite', 'Избранное');
if (!defined('page_blog')) define('page_blog', 'Блог');
if (!defined('page_tickets')) define('page_tickets', 'Билеты');
if (!defined('ticket_avia')) define('ticket_avia', 'Авиа перелеты');
if (!defined('ticket_train')) define('ticket_train', 'Поезд');
if (!defined('ticket_bus')) define('ticket_bus', 'Автобус');
if (!defined('page_choose_language')) define('page_choose_language', 'Выберите язык');
if (!defined('menu_choose_language')) define('menu_choose_language', 'Выбрать язык');
if (!defined('page_cancel')) define('page_cancel', 'Отмена');
if (!defined('page_apply')) define('page_apply', 'Применить');
if (!defined('page_close')) define('page_close', 'Закрыть');
if (!defined('page_back')) define('page_back', 'Назад');
if (!defined('page_next')) define('page_next', 'Далее');
if (!defined('page_watch')) define('page_watch', 'Посмотреть');
if (!defined('common_continue')) define('common_continue', 'Продолжить');
if (!defined('page_search')) define('page_search', 'Поиск');
if (!defined('page_add')) define('page_add', 'Добавить');
if (!defined('page_lang')) define('page_lang', 'Язык');
if (!defined('page_country')) define('page_country', 'Страна');
if (!defined('page_currency')) define('page_currency', 'Выберите валюту');
if (!defined('choose_currency')) define('choose_currency', 'Валюта');
if (!defined('page_loading')) define('page_loading', 'Загрузка');
if (!defined('page_choose_currency')) define('page_choose_currency', 'Выберите валюту');
if (!defined('footer_instruments')) define('footer_instruments', 'Инструменты');
if (!defined('instruments_discont')) define('instruments_discont', 'Акции туроператора');
if (!defined('instruments_podbor')) define('instruments_podbor', 'Страхование');
if (!defined('instruments_tickets')) define('instruments_tickets', 'Купить билеты');
if (!defined('instruments_bset_prices')) define('instruments_bset_prices', 'Выбор лучших цен');
if (!defined('footer_how_it_works')) define('footer_how_it_works', 'Как это работает?');
if (!defined('how_about')) define('how_about', 'О Нас');
if (!defined('how_dogovor')) define('how_dogovor', 'Договор оферты');
if (!defined('how_payment')) define('how_payment', 'Оплата услуг');
if (!defined('how_questions')) define('how_questions', 'Вопросы и ответы');
if (!defined('footer_users')) define('footer_users', 'Пользователям');
if (!defined('users_contacts')) define('users_contacts', 'Контакты');
if (!defined('users_feedback')) define('users_feedback', 'Поддержка');
if (!defined('users_account')) define('users_account', 'Личный кабинет');
if (!defined('users_policy')) define('users_policy', 'Политика конфиденциальности');
if (!defined('users_signin')) define('users_signin', 'Войти');
if (!defined('users_register')) define('users_register', 'Регистрация');
if (!defined('footer_copy')) define('footer_copy', date('Y').' &copy; Все права защищены');
if (!defined('footer_copy_text')) define('footer_copy_text', 'Копирование материалов сайта разрешено только с активной ссылкой на сайт');
if (!defined('footer_vd')) define('footer_vd', 'Разработка и поддержка проекта <a href="https://vindict.com.ua" rel="noopener" target="_blank" class="footer-link" rel="nofollow">Vindict</a>');
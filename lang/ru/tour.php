<?php

if (!defined('no_tour_id')) define('no_tour_id', 'ID тура не определен');
if (!defined('no_prices_found')) define('no_prices_found', 'Цена на тур не определена');
if (!defined('no_price_exists')) define('no_price_exists', 'Цены на тур не существует');
if (!defined('no_curency_exists')) define('no_curency_exists', 'Валюта не найдена в Базе');
if (!defined('orders_limit_signup')) define('orders_limit_signup', 'Сегодня Вам был создан новый аккаунт, авторизуйтесь.');
if (!defined('no_email_listed')) define('no_email_listed', 'Необходимо указать Email');
if (!defined('email_error')) define('email_error', 'Email введен с ошибками');
if (!defined('please_auth')) define('please_auth', 'Мы нашли Ваш Email, %s! Пожалуйста, введите пароль, чтобы купить тур:');
if (!defined('login_error')) define('login_error', 'Неправильный Email или пароль');
if (!defined('activate_account')) define('activate_account', 'Активируйте Аккаунт перед покупкой');
if (!defined('cant_order')) define('cant_order', 'Вы не можете покупать на TravelNet');

if (!defined('tour_go_email')) define('tour_go_email', 'Введите Email для бронирования');
if (!defined('tour_book_text')) define('tour_book_text', 'Чтобы продолжить бронирование, введите свой Email. На него будут приходить уведомления и подтверждения действий на сайте');
if (!defined('tour_email_pl')) define('tour_email_pl', 'Ваш Email');

if (!defined('tour_adults')) define('tour_adults', 'Взрослые');
if (!defined('tour_adults_pl')) define('tour_adults_pl', 'Кол-во взрослых');
if (!defined('tour_children')) define('tour_children', 'Дети');
if (!defined('tour_children_pl')) define('tour_children_pl', 'Кол-во детей');
if (!defined('tour_length')) define('tour_length', 'Продолжительность тура');
if (!defined('tour_cities')) define('tour_cities', 'Городов на маршруте');
if (!defined('tour_days')) define('tour_days', 'дней');
if (!defined('tour_cities_text')) define('tour_cities_text', 'городов');
if (!defined('tour_watch_prices')) define('tour_watch_prices', 'Посмотреть цены');
if (!defined('tour_currency')) define('tour_currency', 'Валюта');
if (!defined('tour_buy_header')) define('tour_buy_header', 'Купить тур');
if (!defined('tour_buy_subheader')) define('tour_buy_subheader', 'цена билетов указана за одного человека');
if (!defined('tour_book')) define('tour_book', 'Забронировать');
if (!defined('tour_info_header')) define('tour_info_header', 'Информация о туре');
if (!defined('tour_info_desc')) define('tour_info_desc', 'Описание');
if (!defined('tour_info_program')) define('tour_info_program', 'Программа');
if (!defined('tour_star')) define('tour_star', 'В избранное');
if (!defined('tour_bus_tour')) define('tour_bus_tour', 'Автобусный тур');
if (!defined('tour_bus_about')) define('tour_bus_about', 'на TravelNet Tour - маршрут, цены, информация');
if (!defined('tour_bus_details')) define('tour_bus_details', 'Подробнее');
if (!defined('tour_bus_email_text')) define('tour_bus_email_text', 'Информация об автобусном туре');
if (!defined('text_tour_on')) define('text_tour_on', 'на TravelNet Tour');
if (!defined('tour_contacts')) define('tour_contacts', 'Контакты');
if (!defined('tour_no_details')) define('tour_no_details', 'Информация о туре уточняется на сайте Туроператора');
if (!defined('tour_goto_operator')) define('tour_goto_operator', 'Описание на сайте Туроператора');
if (!defined('tour_day_no')) define('tour_day_no', 'День');
if (!defined('tour_no_programm')) define('tour_no_programm', 'Программа тура доступна на сайте Туроператора');
if (!defined('tour_currency')) define('tour_go_email', 'Введите Email для бронирования');
if (!defined('tour_book_text')) define('tour_book_text', 'Чтобы продолжить бронирование, введите свой Email. На него будут приходить уведомления и подтверждения действий на сайте');
if (!defined('tour_email_pl')) define('tour_email_pl', 'Ваш Email');

if (!defined('text_pieces')) define('text_pieces', 'Купить в рассрочку');
if (!defined('text_tour')) define('text_tour', 'Тур');
if (!defined('text_info')) define('text_info', 'Подробная информация о туре %s - цены, дорога, фото и даты выезда. Сравните цены разных туроператоров и выберите самую дешевую поездку.');
if (!defined('text_ograph_img')) define('text_ograph_img', 'выбор туров всех туроператоров Украины Ograph image');

if (!defined('email_confirm')) define('email_confirm', 'Подтверждение Email на');
if (!defined('email_text')) define('email_text', 'Здравствуйте, Вы только что начали заказ своего тура на TravelNet Tour!<br/>
									После оформления заказа Вам необходимо будет перейти в Личный кабинет. Чтобы это сделать, необходимо активировать свой аккаунт:<br/><br/></p><p style="text-align:center;">
									<a class="btn btn-primary btn-lg raised" href="%sactivation?token=%s&email=%s">Активировать</a><br/><br/></p><p>
									Для первого доступа в Ваш Аккаунт мы сгенерировали Вам пароль: <strong>%s</strong>. Настоятельно рекомендуем его поменять 
									при первом входе в Личный кабинет, в разделе "Профиль" -> "Изменить пароль".<br/><br/>
									В противном случае, если Вы не совершали покупок на TravelNet Tour, пожалуйста, удалите это сообщение.<br/>
									Если у Вас есть предложения, замечания или вопросы касательно сайта, 
									пожалуйста, свяжитесь с нами через <a href="%scontacts">Форму обратной связи</a> или другим удобным образом.<br/><br/>
									<small>Сообщение было сгенерировано автоматически, пожалуйста, не отвечайте на него.<br/>
									С наилучшими пожеланиями,<br/>Команда TravelNet Tour</small>');

if (!defined('tour_info_credit')) define('tour_info_credit', 'Как купить в рассрочку?');
if (!defined('tour_info_credit_header')) define('tour_info_credit_header', 'Краткая инструкция по покупке в рассрочку');
if (!defined('tour_info_credit_text')) define('tour_info_credit_text', 'На сайте TravelNet Tour доступны разнообразные формы оплаты, в том числе
									и оплата частями или в рассрочку. Чтобы оплатить в рассрочку необходимо нажать кнопку "Забронировать", пройти 
									процесс оформления заказа и на этапе оплаты выбрать желаемый банк для оплаты частями. Можно проверить,
									можете ли Вы воспользоваться подобной услугой, введя номер телефона, перед выбором количества платежей. 
									Далее Вы будете перенаправлены на страницу банка, где можно детально ознакомится с их условиями рассрочки.<br/><br/>
									Обратите внимание, что TravelNet Tour не гарантирует, что Вы можете купить тур в рассрочку, поскольку окончательное
									решение остается за банком. Поэтому, пожалуйста, прежде чем оплачивать заказ частями, убедитесь, что Вам доступна такая услуга в банке, где Вы обсуживаетесь.');



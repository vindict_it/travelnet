<?php

// login page info
if (!defined('page_main_header')) define('page_main_header', 'Відновлення паролю');
if (!defined('page_header')) define('page_header', 'Введіть свій Email');
if (!defined('page_instr_header')) define('page_instr_header', 'Інструкція');
if (!defined('page_instr_desc')) define('page_instr_desc', 'Для відновлення доступу до облікового запису необхідно ввести Email-адреса, на який зареєстрований Ваш аккаунт. На пошту прийде лист з подальшими інструкціями.');
if (!defined('page_instr_warning')) define('page_instr_warning', '<strong>Підказка:</strong> Використовуйте складні паролі і завжди зберігайте їх у надійному місці.');
if (!defined('page_send')) define('page_send', 'Відновити');

if (!defined('recover_warning')) define('recover_warning', 'Увага');
if (!defined('recover_no_email')) define('recover_no_email', 'Ви не ввели Email');
if (!defined('recover_email')) define('recover_email', 'Введіть коректний Email');
if (!defined('recover_email_not_found')) define('recover_email_not_found', 'Введений Email не зареєстрований');
if (!defined('recover_heading')) define('recover_heading', 'Відновлення пароля на');
if (!defined('recover_text')) define('recover_text', 'Привіт %s, <br/> Ми отримали запит на відновлення пароля від Вашого Акаунта. Щоб продовжити, перейдіть за посиланням нижче:<br/><br/></p><p style="text-align:center;">
													<a class="btn btn-primary btn-lg raised" href="%s"> Відновити пароль</a><br/><br/></p><p> Після переходу за посиланням, введіть новий пароль. <br/>
													Якщо Ви не запитували відновлення пароля, терміново <a href="%scontacts"> зв\'яжіться з Адміністрацією </a>!<br/><br/>
													<small>Це повідомлення було згенеровано автоматично, не треба на нього відповідати. <br/> З найкращими побажаннями, <br/> Команда TravelNet Tour</small>');
if (!defined('recover_success')) define('recover_success', 'Письмо с инструкциями по восстановлению пароля было отправлено на Ваш Email-адрес');
if (!defined('recover_error')) define('recover_error', 'Ой... Сейчас мы не можем отправить Вам письмо по восстановлению. Похоже, на сайте ведуться техничские работы. Пожалуйста, перезагрузите страницу и попробуйте ещё раз.');
if (!defined('recover_not_confirmed')) define('recover_not_confirmed', 'Ваш Email не подтвержден. Вы можете восстановить только активный Аккаунт. Проверьте свою почту, мы присылали Вам письмо с подтверждением.');

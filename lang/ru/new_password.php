<?php

// login page info
if (!defined('page_main_header')) define('page_main_header', 'Введите новый пароль');
if (!defined('page_header')) define('page_header', 'Новый пароль');
if (!defined('page_header2')) define('page_header2', 'Повторите пароль');
if (!defined('page_instr_header')) define('page_instr_header', 'Инструкция');
if (!defined('page_instr_desc')) define('page_instr_desc', 'Введите в первом поле Ваш новый пароль и повторите его во втором поле. Пароль должен отвечать следующим требованиям:');
if (!defined('page_instr_1')) define('page_instr_1', 'быть длиннее шести символов');
if (!defined('page_instr_2')) define('page_instr_2', 'содержать хотя бы одну букву');
if (!defined('page_instr_3')) define('page_instr_3', 'содержать хотя бы одну цифру');
if (!defined('page_send')) define('page_send', 'Восстановить');
if (!defined('page_auth')) define('page_auth', 'Прежде чем восстановить пароль, Вам необходимо выйти из Аккаунта');
if (!defined('err_invalid')) define('err_invalid', 'Внимание! Неправильный или устаревший код восстановления пароля.');
if (!defined('err_invalid_code')) define('err_invalid_code', 'Неправильный или устаревший код восстановления пароля.');
if (!defined('err_none')) define('err_none', 'Внимание! Вы не запрашивали восстановление пароля.');
if (!defined('err_account')) define('err_account', 'Вы не запрашивали восстановление пароля.');
if (!defined('err_fail')) define('err_fail', 'Восстановление пароля возможно на <a href="'.DIR_PATH.'recover">этой странице</a>.');
if (!defined('new_no_email')) define('new_no_email', 'Отсутствует Email. Перейдите по ссылке из письма на Вашей почте.');
if (!defined('new_email')) define('new_email', 'Указан некорректный Email. Перейдите по ссылке из письма на Вашей почте.');
if (!defined('new_password')) define('new_password', 'Вы не ввели пароль');
if (!defined('new_password_repeat')) define('new_password_repeat', 'Пароли не совпадают');
if (!defined('new_email_header')) define('new_email_header', 'Изменение пароля на');
if (!defined('new_pass_wrong')) define('new_pass_wrong', 'Пароль должен отвечать инструкции');
if (!defined('new_warning')) define('new_warning', 'Внимание');
if (!defined('new_success')) define('new_success', 'Пароль был успешно изменен! Теперь, используя его, Вы можете зайти в Личный кабинет.');
if (!defined('new_text')) define('new_text', 'Здравствуйте, %s, <br/>Ваш пароль на сайте TravelNet Tour (%s) был изменен %s с IP-адреса: %s.<br/>
											Чтобы войти в свой Личный кабинет, перейдите по ссылке:<br/><br/></p><p style="text-align:center;"> <a href="%slogin" class="btn btn-primary btn-lg raised">Личный кабинет</a><br/><br/></p><p>
											Спасибо, что выбрали travelNet Tours!<br/>Если Вы не меняли пароль, пожалуйста, немедленно <a href="%scontacts">свяжитесь с нами</a>!<br/><br/>
											<small>Это сообщение было сгенерировано автоматически, пожалуйста, не надо на него отвечать.<br/>С наилучшими пожеланиями,<br/> Команда TravelNet Tour</small>');

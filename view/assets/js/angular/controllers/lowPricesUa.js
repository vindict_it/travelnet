travelNetToursApp.controller('LowPricesUaController', function($scope, $http, $q) {
    var lowPricesUa = this;



    lowPricesUa.chart = null;
    lowPricesUa.chartDataOriginal = [];
    lowPricesUa.chartData = [];
    lowPricesUa.chartLegend = [];
    lowPricesUa.chartDates = [];



    lowPricesUa.parameters = {
        tourType: 1,
        days: 7,
        countryID: 509
    };



    lowPricesUa.countriesList = [];
    lowPricesUa.baseCountriesList = {
        bus: [
            {id: 509, name: 'Одесса', code: 'ua'},
            {id: 544, name: 'Киев', code: 'ua'},
            {id: 1586, name: 'Харьков', code: 'ua'},
            {id: 1587, name: 'Днепр', code: 'ua'}
        ],
    };



    lowPricesUa.setTourType = function(type) {
        lowPricesUa.parameters.tourType = type;
        switch (type) {
            case 0: lowPricesUa.countriesList = lowPricesUa.baseCountriesList.avia; break;
            case 1: lowPricesUa.countriesList = lowPricesUa.baseCountriesList.bus; break;
            case 2: lowPricesUa.countriesList = lowPricesUa.baseCountriesList.cruise; break;
        }

        lowPricesUa.chartDates = [];
        lowPricesUa.setCountry(lowPricesUa.countriesList[0].id);
    };



    lowPricesUa.setTourDays = function(days) {
        lowPricesUa.parameters.days = days;

        lowPricesUa.chartDates = [];
        lowPricesUa.rebuildLowPrices();
    };



    lowPricesUa.setCountry = function(countryID) {
        lowPricesUa.parameters.countryID = countryID;

        lowPricesUa.chartDates = [];
        lowPricesUa.rebuildLowPrices();
    };



    lowPricesUa.initChart = function() {
        switch (lowPricesUa.parameters.tourType) {
            case 0: lowPricesUa.countriesList = lowPricesUa.baseCountriesList.avia; break;
            case 1: lowPricesUa.countriesList = lowPricesUa.baseCountriesList.bus; break;
            case 2: lowPricesUa.countriesList = lowPricesUa.baseCountriesList.cruise; break;
        }

        var ctx = $('#homeLowPricesUaChart')[0];
        var chartOptions = jQuery.extend(true, {}, generalChartOptions);
        chartOptions.legend = false;
        chartOptions.onClick = function(e, arr) {
            var activePoint = this.lastActive[0];

            if (activePoint !== undefined) {
                var datasetIndex = activePoint._datasetIndex;
                var index = activePoint._index;
                var airport = this.config.data.datasets[datasetIndex].label;
                var date = this.config.data.labels[index];

                var locTo = baseHostUrl+'/t?countryID='+lowPricesUa.parameters.countryID+'&tourType='+lowPricesUa.parameters.tourType+'&dateSince='+date+'.'+(new Date()).getFullYear();
                window.location = locTo;
            }
        };

        lowPricesUa.chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: chartOptions
        });
    };



    lowPricesUa.renderChartData = function() {
        var labels = [];
        var allDates = [];
        var origLabels = [];
        var chartData = lowPricesUa.chart.data;
        chartData.datasets = [];

        //form datasets
        lowPricesUa.chartData.forEach(function(item, i) {
            if (origLabels.indexOf(item.city_name) != -1) return;
            origLabels.push(item.city_name);

            //set labels from first item
            if (i == 0)
                item.prices.forEach(function(price) {
                    labels.push(formatDateForCharts(price.date));
                });

            //set data
            var newBaseDataset = Object.assign({}, baseChartDataset);

            newBaseDataset.label = item.city_name;
            newBaseDataset.data = [];
            newBaseDataset.borderColor = baseChartColors[chartData.datasets.length];
            newBaseDataset.pointBorderColor = baseChartColors[chartData.datasets.length];
            item.prices.forEach(function(price) {
                if (price.price >= 0)
                    newBaseDataset.data.push(price.price);
                else
                    newBaseDataset.data.push(NaN);
                allDates.push(price.date);
            });

            chartData.datasets.push(newBaseDataset);
        });
        allDates = unique(allDates);

        //build new data
        chartData.labels = labels;
        var duration = 0;
        if (lowPricesUa.chartDates.length != 0)
            duration = 1000;
        lowPricesUa.chart.update({duration: duration});

        //create custom legend
        lowPricesUa.chartLegend = [];
        chartData.datasets.forEach(function(dataset, i) {
            var minData = [];
            dataset.data.forEach(function(item) {if (!isNaN(item)) minData.push(item)});
            lowPricesUa.chartLegend.push({label: dataset.label, color: baseChartColors[i], price: Math.min(...minData)});
        });

        //style checkboxes
        if (lowPricesUa.chartDates.length != 0)
            setTimeout(function() {
                console.log('Reinit checkbox!');
                $('.chart-table td input').each(function(i, d) {
                    var obj = $(d);
                    if (!obj.parent('div').hasClass('icheckbox_square-grey')) {
                        var style = obj.attr('style'); //save color
                        obj.iCheck({
                            checkboxClass: 'icheckbox_square-grey',
                            radioClass: 'iradio_square-grey',
                            increaseArea: '-120%' // optional
                        });
                        obj.parent('div').attr('style', style);
                    }
                    obj.parent('div').parent('td')[0].setAttribute('data-clicked', '0');
                });
            }, 200);

        //build dates ruler
        if (lowPricesUa.chartDates.length == 0)
            lowPricesUa.organizeDates(allDates);
    };



    //Get data for graph
    lowPricesUa.rebuildLowPrices = function() {
        var sourceUrl = baseHostUrl+'/model/ajax-handler.php';
        var deferred = $q.defer();

        $http.get(sourceUrl, {params: {countryID: lowPricesUa.parameters.countryID, days: lowPricesUa.parameters.days, tourType: lowPricesUa.parameters.tourType, action: 'GetComparerPricesCity'}}).then(function success(response) {
            console.log('Bus tours UA:');
            console.log({countryID: lowPricesUa.parameters.countryID, days: lowPricesUa.parameters.days, tourType: lowPricesUa.parameters.tourType});
            console.log(response.data);
            lowPricesUa.chartData = response.data;
            lowPricesUa.chartDataOriginal = response.data;

            lowPricesUa.renderChartData();

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    lowPricesUa.toggleLowPricesChartDataset = function(label) {
        var obj = $('.chart-table td[data-label="' + label + '"]').find('input');

        var chartData = lowPricesUa.chart;
        chartData.data.datasets.forEach(function(dataset, i) {
            if (dataset.label == label) {
                if (chartData.data.datasets[i].hidden)
                    chartData.data.datasets[i].hidden = false;
                else
                    chartData.data.datasets[i].hidden = true;

                chartData.update({duration: 0});
            }
        });

        setTimeout(function() {
            $('.chart-table td[data-label="' + label + '"]')[0].setAttribute('data-clicked', '1');

            $('.chart-table td input').each(function(i, d) {
                var obj = $(d);
                if (!obj.parent('div').hasClass('icheckbox_square-grey')) {
                    var style = obj.attr('style'); //save color
                    obj.iCheck({
                        checkboxClass: 'icheckbox_square-grey',
                        radioClass: 'iradio_square-grey',
                        increaseArea: '-120%' // optional
                    });
                    obj.parent('div').attr('style', style);
                }

                console.log('Check');
                console.log(obj.parent('div').parent('td'));
                console.log(obj.parent('div').parent('td').attr('data-clicked'));
                if (obj.parent('div').parent('td').data('clicked') == '1') {
                    obj.parent('div').parent('td')[0].setAttribute('data-clicked', '0');

                    chartData.data.datasets.forEach(function(dataset, i) {
                        if (dataset.label == obj.parent('div').parent('td').data('label')) {
                            console.log('checked: ');
                            console.log(chartData.data.datasets[i].hidden);
                            if (chartData.data.datasets[i].hidden)
                                obj.iCheck('uncheck');
                            else
                                obj.iCheck('check');
                        }
                    });

                }
            });
        }, 200);
    };



    lowPricesUa.organizeDates = function(rawDates) {
        var datesFormatted = [];
        var monthNames = [
            "Январь", "Февраль", "Март",
            "Апрель", "Май", "Июнь", "Июль",
            "Август", "Сентябрь", "Октябрь",
            "Ноябрь", "Декабрь"
        ];

        rawDates.forEach(function(rawDate, i) {
            var date = new Date(rawDate);
            var monthIndex = date.getMonth();

            var label = '';
            if (date.getFullYear() != (new Date()).getFullYear())
                label = monthNames[monthIndex]+', '+date.getFullYear();
            else
                label = monthNames[monthIndex];

            if (datesFormatted.indexOf(label) == -1)
                datesFormatted.push(label);
        });

        lowPricesUa.chartDates = datesFormatted;

        //init first month
        setTimeout(function() {
            if ($('#home-low-prices-ua-dates').length && lowPricesUa.chartDates.length)
                lowPricesUa.switchToDate(datesFormatted[0], '#home-low-prices-ua-dates');
        }, 1000);
    };



    lowPricesUa.switchToDate = function(dateLabel, curSelector) {
        showLoader();

        //select item and move slider
        $(curSelector).find('a.active').removeClass('active');
        $(curSelector).find('a[data-date="'+dateLabel+'"]').addClass('active');
        var positionLeft = $(curSelector).find('a.active').parent('li').position().left;
        $(curSelector).siblings('.dateSlider').find('.dateHandle').css('left', positionLeft+'px');

        var monthNames = [
            "Январь", "Февраль", "Март",
            "Апрель", "Май", "Июнь", "Июль",
            "Август", "Сентябрь", "Октябрь",
            "Ноябрь", "Декабрь"
        ];

        var dateFormatted = dateLabel.split(', ');
        var dateObj = {};
        if (dateFormatted.length == 1) {
            dateObj['month'] = monthNames.indexOf(dateFormatted[0]);
            dateObj['year'] = (new Date()).getFullYear();
        } else {
            dateObj['month'] = monthNames.indexOf(dateFormatted[0]);
            dateObj['year'] = dateFormatted[1];
        }

        var originalData = copy(lowPricesUa.chartDataOriginal);

        originalData.forEach(function(item, i) {
            var newPrices = item.prices;

            var foundOldDate = true;
            while (foundOldDate) {
                foundOldDate = false;
                newPrices.forEach(function(price, j) {
                    var pdate = new Date(price.date);
                    if (!foundOldDate && (pdate.getFullYear() != dateObj.year || pdate.getMonth() != dateObj.month)) {
                        foundOldDate = true;
                        newPrices.splice(j, 1);
                    }
                });
            }
            originalData[i].prices = newPrices;
        });
        lowPricesUa.chartData = originalData;

        lowPricesUa.renderChartData();
        hideLoader();
    };



    //on load
    lowPricesUa.initChart();
    lowPricesUa.rebuildLowPrices();
});
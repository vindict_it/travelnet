<?php

if (!defined('page_bank_header')) define('page_bank_header', 'Рассрочка платежей от Альфа банка');
if (!defined('page_bank_desc')) define('page_bank_desc', 'Условия рассрочки для клиентов Альфа Банка. Прежде чем оплaчивать заказ, пожалуйста, убедитесь, что Вы подходите для выдачи кредита.');
<?php

if (!defined('page_header')) define('page_header', 'Страноведение');
if (!defined('page_desc')) define('page_desc', 'Не знаете, куда отправиться в %s году? Выберите страну, узнайте больше и получите рекомендации от TravelNet tour.');
if (!defined('page_europe')) define('page_europe', 'Европа');
if (!defined('page_asia')) define('page_asia', 'Азия');
if (!defined('page_africa')) define('page_africa', 'Африка');
if (!defined('page_america')) define('page_america', 'Америка');
if (!defined('page_australia')) define('page_australia', 'Австралия');
if (!defined('countries_empty')) define('countries_empty', 'Пока страны в этом регионе отсутствуют');

if (!defined('page_video')) define('page_video', 'Смотреть видео');
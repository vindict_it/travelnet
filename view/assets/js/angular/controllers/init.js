var travelNetToursApp = angular.module('TravelNetTours', []);

travelNetToursApp.filter('range', function() {
    return function(input, total) {
        total = parseInt(total);
        for (var i=0; i<total; i++) {
            input.push(i);
        }
        return input;
    };
});

travelNetToursApp.filter('num2str', function() {
    return function(n, text_forms) {
        n = Math.abs(n) % 100;
        var n1 = n % 10;
        if (n > 10 && n < 20) return text_forms[2];
        if (n1 > 1 && n1 < 5) return text_forms[1];
        if (n1 == 1) return text_forms[0];
        return text_forms[2];
    };
});

travelNetToursApp.filter('color2black', function() {
    return function(color, price) {
        var newC = '#000000';
        if (price != 'Infinity')
            newC = color;
        return newC;
    };
});

travelNetToursApp.filter('formatDate', function() {
    return function(date) {
        var dateObj = new Date(date);
        return dateObj.toLocaleString("ru", {year: 'numeric', month: 'long', day: 'numeric'});
    };
});

travelNetToursApp.factory("autoCompleteDataService", ["$http", function ($http) {
    return {
        getSource: function (term) {
            return $http.get(baseHostUrl+'/api/hotels-list?term='+term).then(function (response) {
                return response.data;
            });
        }
    };
}]);

travelNetToursApp.directive('autoComplete', function(autoCompleteDataService) {
    return {
        restrict: 'A',
        scope: false,
        link: function(scope, elem, attr) {
            elem.autocomplete({
                minLength: 2,
                source: function (searchTerm, response) {
                    autoCompleteDataService.getSource(searchTerm.term).then(function (autocompleteResults) {
                        response(autocompleteResults);
                    });
                },
                select: function( event, ui ) {
                    scope.listingSearch.parameters.hotelsAvailable.push(ui.item);
                    scope.$apply();
                }
            }).autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li>" ).append( "<div>" + item.name + "</div>" ).appendTo( ul );
            };
        }
    };
});

travelNetToursApp.factory("autoCompleteLocationDataService", ["$http", function ($http) {
    return {
        getSource: function (term) {
            return $http.get(baseHostUrl+'/api/get-autocomplete-locations?q='+term).then(function (response) {
                return response.data;
            });
        }
    };
}]);

travelNetToursApp.directive('autoCompleteLocation', function(autoCompleteLocationDataService) {
    return {
        restrict: 'A',
        scope: false,
        link: function(scope, elem, attr) {
            elem.autocomplete({
                minLength: 2,
                source: function (searchTerm, response) {
                    autoCompleteLocationDataService.getSource(searchTerm.term).then(function (autocompleteResults) {
                        response(autocompleteResults);
                    });
                },
                select: function( event, ui ) {
                    scope.listingSearch.selectModalCountry({id: ui.item.value, name: ui.item.label});
                    scope.listingSearch.submitLocationSelection();
                    $(event.target).val('');
                    scope.$apply();
                    return false;
                }
            }).autocomplete( "instance" )._renderItem = function( ul, item ) {
                console.log(item);
                return $( "<li>" ).append( "<div>" + item.label + "</div>" ).appendTo( ul );
            };
        }
    };
});
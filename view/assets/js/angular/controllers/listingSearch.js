travelNetToursApp.controller('ListingSearchController', function($scope, $http, $q, $filter, $timeout) {
    var listingSearch = this;



    listingSearch.baseChartColors = baseChartColors;
    listingSearch.lowPricesModalChart = null;
    listingSearch.lowPricesModalChartData = [];
    listingSearch.lowPricesModalChartDataOriginal = [];
    listingSearch.lowPricesModalChartDates = [];
    listingSearch.lowPricesModalChartLegend = [];



    listingSearch.locations = [];
    listingSearch.locationsSortedL = [];
    listingSearch.locationsSortedR = [];
    listingSearch.locationsSortedC = [];
    listingSearch.selectedModalCountry={id: ''};
    listingSearch.selectedModalRegion={id: ''};



    listingSearch.allOperators = [];
    listingSearch.allPansions = [];
    listingSearch.operatorsByType = [];
    listingSearch.hotels = [];



    listingSearch.initialized = false;
    listingSearch.parameters = {
        specialTourType: 0,
        tourType: 0,
        operators: [/*{
            id: 1,
            name: 'Роза Ветров'
        }, {
            id: 2,
            name: 'TUI Travel'
        }*/],
        regions: {value: [/*{
            id: 1,
            name: 'Шарм-эш-Шейх',
            location: 'Южный Синай, Египет',
            icon: 'bullseye'
        }, {
            id: 2,
            name: 'Kempinski Hotel',
            location: 'Белек, Турция',
            icon: 'map-marker-alt'
        }*/]},
        hotelsAvailable: [/*{
            id: 2,
            name: 'Kempinski Hotel',
        }*/],
        country: '',
        countryID: 0,
        countriesNumber: 3,
        departureCity: [],
        departureCitiesFormatted: '',
        citiesTo: [/*{
            id: 2,
            name: 'Paris'
        }*/],
        stars: [],
        food: [],
        line: [],
        adults: '',
        children: '',
        adultsFormatted: '',
        dateSince: '',
        dateShift: 3,
        dateNights: 7,
        priceFrom: 0,
        priceTo: 40000
    };



    listingSearch.setTourType = function(type) {
        listingSearch.parameters.tourType = type;
        listingSearch.parameters.operators = [];
        listingSearch.getOperatorsByType();
        markerShowcaseOn = false;
        curMarkerDataPoint = 0;
        globalTourType = type;

        if (!markerShowcaseOn && $('#home-map').length && screen && screen.width > 768) {
            //remove existing markers
            markersOnMap.forEach(function(marker) {
                homeMap.removeLayer(marker);
            });
            markersOnMap = [];

            //show new markers
            markerShowcaseOn = true;
            showMarkerData();
        }

        if ($('#btnGroup2').length) {
            //trigger low prices Tour Type
            $timeout(function() {
                angular.element('#btnGroup2 a[data-type="'+type+'"]').triggerHandler('click');
            });
            console.log('fired!');
        }
    };



    listingSearch.getAllPansions = function() {
        var deferred = $q.defer();

        $http({method: 'GET', url: baseHostUrl+'/view/assets/src/pansions.json'}).then(function success(response) {
            listingSearch.allPansions = response.data;

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    listingSearch.getAllOperators = function() {
        var deferred = $q.defer();

        $http({method: 'GET', url: baseHostUrl+'/view/assets/src/tour-operators.json'}).then(function success(response) {
            listingSearch.allOperators = response.data;
            listingSearch.getOperatorsByType();

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    listingSearch.getOperatorsByType = function() {
        var operators = [];
        switch (listingSearch.parameters.tourType) {
            case 1: operators = listingSearch.allOperators.bus; break;
            case 2: operators = listingSearch.allOperators.cruise; break;
            default: operators = listingSearch.allOperators.avia; break;
        }

        listingSearch.operatorsByType = operators;
    };



    listingSearch.showOperatorSelector = function() {
        var deferred = $q.defer();
        if (listingSearch.allOperators.length != 0) {
            $('#modal-operator-selector').modal({backdrop: 'static'});

            deferred.resolve('Success');
        } else {
            showLoader();
            var promise = listingSearch.getAllOperators();

            promise.then(function() {
                $('#modal-operator-selector').modal({backdrop: 'static'});

                hideLoader();
                deferred.resolve('Success');
            });
        }

        return deferred.promise;
    };



    listingSearch.submitOperatorSelection = function() {
        $('#modal-operator-selector').modal('hide');
    };



    listingSearch.toggleOperator = function(operator) {
        var active = -1;
        listingSearch.parameters.operators.forEach(function(op, i) {
            if (op.id == operator.id) active = i;
        });

        var opObj = null;
        $('body #modal-operator-selector .modal-operators-list a').each(function(i, d) {
            if ($(d).data('operator') == operator.id) opObj = $(d);
        });

        if (active >= 0) {
            listingSearch.parameters.operators.splice(active, 1);
            opObj.removeClass('active');
            opObj.find('i').addClass('fa-plus').removeClass('fa-check');
        } else {
            listingSearch.parameters.operators.push(operator);
            opObj.addClass('active');
            opObj.find('i').removeClass('fa-plus').addClass('fa-check');
        }
    };



    listingSearch.removeOperator = function(operatorId) {
        var active = -1;
        listingSearch.parameters.operators.forEach(function(op, i) {
            if (op.id == operatorId) active = i;
        });

        if (active >= 0)
            listingSearch.parameters.operators.splice(active, 1);
    };



    listingSearch.operatorActive = function(operatorID) {
        var found = false;
        listingSearch.parameters.operators.forEach(function(op) {
            if (op.id == operatorID) found = true;
        });

        return found;
    };



    listingSearch.selectModalCountry = function(location) {
        listingSearch.selectedModalCountry = {id: location.id, name: location.name};
        listingSearch.selectedModalRegion = {};
        $('#spl-locations a').removeClass('active');
        $('#spl-locations a[data-location="'+location.id+'"]').addClass('active');
    };



    listingSearch.selectModalRegion = function(region) {
        listingSearch.selectedModalRegion = {id: region.id, name: region.name};
        $('#spl-regions a').removeClass('active');
        $('#spl-regions a[data-region="'+region.id+'"]').addClass('active');
    };



    listingSearch.addRegion = function(cityObj) {
        $('#spl-cities a').removeClass('active');
        $('#spl-cities a[data-city="'+cityObj.id+'"]').addClass('active');
        var region = {
            id: cityObj.id,
            name: cityObj.name,
            location: /*listingSearch.selectedModalRegion.name+', '+*/listingSearch.selectedModalCountry.name,
            icon: 'bullseye'
        };
        listingSearch.parameters.regions.value.push(region);
        listingSearch.parameters.country = /*region.name+', '+*/region.location;
        listingSearch.parameters.countryID = listingSearch.selectedModalCountry.id;

        if ($('#inputPlace').length)
            $('#inputPlace').addClass('closed');
        $('#modal-location-selector').modal('hide');
        listingSearch.departureAvailable();
    };



    listingSearch.removeRegion = function(regionId) {
        listingSearch.parameters.regions.value.forEach(function(region, i, values) {
            if (region.id == regionId)
                listingSearch.parameters.regions.value.splice(i, 1);
        });

        console.log('Removed region: '+regionId);
    };



    listingSearch.getCountryID = function() {
        var deferred = $q.defer();
        if (listingSearch.parameters.countryID != 0)
            deferred.resolve('Success');
        else {
            $http({method: 'GET', url: baseHostUrl+'/view/assets/src/countries.json'}).then(function success(response) {
                var finalCountryParts = listingSearch.parameters.country.split(', ');
                var finalCountry = finalCountryParts[finalCountryParts.length-1];
                for (var countryName in response.data) {
                    if (response.data.hasOwnProperty(countryName))
                        if (countryName == finalCountry) {
                            listingSearch.parameters.regions = {value: [{id: response.data[countryName].id, name: countryName, icon: 'bullseye'}]};
                            listingSearch.parameters.countryID = response.data[countryName].id;
                        }
                }

                console.log('Country: '+finalCountry+', ID: '+listingSearch.parameters.countryID);
                deferred.resolve('Success');
            }, function failed() {
                deferred.reject('Fail');
            });
        }

        return deferred.promise;
    };



    listingSearch.countryChanged = function() {
        var deferred = $q.defer();
        listingSearch.parameters.countryID = 0;

        listingSearch.departureAvailable().then(function success(response) {
            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        if ($('#inputPlace').length && $('#inputPlace').val() == '')
            $('#inputPlace').removeClass('closed');

        return deferred.promise;
    };



    listingSearch.getCitiesTo = function() {
        var deferred = $q.defer();

        var tourName = '';
        switch (listingSearch.parameters.tourType) {
            case '0': tourName = 'avia'; break;
            case '1': tourName = 'bus'; break;
        }

        console.log('Selected country:');
        console.log(listingSearch.parameters.countryID);
        var sourceUrl = baseHostUrl+'/model/ajax-handler.php?countryID='+listingSearch.parameters.countryID+'&tourType='+listingSearch.parameters.tourType+'&action=GetDepartureCities';
        
		$http({method: 'GET', url: sourceUrl}).then(function success(response) {
            /*for (var countryName in response.data) {
                if (response.data.hasOwnProperty(countryName))
                    if (countryName == listingSearch.parameters.country) {
                        listingSearch.parameters.regions = {value: [{id: response.data[countryName].id, name: countryName}]};
                        listingSearch.parameters.countryID = response.data[countryName].id;
                    }
            }*/
            listingSearch.parameters.citiesTo = [];
            response.data.forEach(function(city) {
                listingSearch.parameters.citiesTo.push({
                    id: city.id,
                    name: city.orig_name
                });
            });
            console.log('Departure cities:');
            console.log(listingSearch.parameters.citiesTo);
            setTimeout(function() {
                $('.m-custom-scrollbar').mCustomScrollbar({theme: "minimal-dark"});
            }, 500);

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    listingSearch.loadLocations = function() {
        var deferred = $q.defer();
        if (listingSearch.locations.length != 0)
            deferred.resolve('Success');
        else {
            $http({method: 'GET', url: baseHostUrl+'/view/assets/src/locations_sorted.json'}).then(function success(response) {
                listingSearch.locations = response.data.original;
                listingSearch.locationsSortedL = response.data.locations;
                listingSearch.locationsSortedR = response.data.regions;

                //rebuild cities list to remove regions
                var locs = {};
                for (var countryName in response.data.cities) {
                    if (response.data.cities.hasOwnProperty(countryName)) {
                        var countryItem = response.data.cities[countryName];

                        for (var regionName in countryItem.regions) {
                            if (countryItem.regions.hasOwnProperty(regionName)) {
                                var regionItem = countryItem.regions[regionName];

                                regionItem.cities.forEach(function(cityItem) {
                                    if (!locs.hasOwnProperty(countryName))
                                        locs[countryName] = regionItem.cities;
                                    else
                                        locs[countryName].concat(regionItem.cities);
                                });
                            }
                        };
                    }
                };
                console.log(locs);
                listingSearch.locationsSortedC = locs;

                deferred.resolve('Success');
            }, function failed() {
                deferred.reject('Fail');
            });
        }

        return deferred.promise;
    };



    listingSearch.searchHome = function() {
        if (listingSearch.parameters.country.length == 0)
            listingSearch.showRegionSelector('main');
        else if (listingSearch.parameters.dateSince.length == 0)
            listingSearch.showDatesSelector();
        else {
            var allowedParams = new Array('tourType', 'country', 'adults', 'children', 'dateSince', 'dateShift', 'dateNights', 'operators');
            var arr = [];
            for (var name in listingSearch.parameters) {
                if (listingSearch.parameters.hasOwnProperty(name) && allowedParams.indexOf(name)>=0) {
                    if (name == 'operators')
                        arr.push('ops=' + listingSearch.parameters[name].join(','));
                    else
                        arr.push(name + '=' + listingSearch.parameters[name]);
                }
            };

            if (listingSearch.parameters.departureCity.length)
                arr.push('departureCity='+JSON.stringify(listingSearch.parameters.departureCity));

            window.location = baseHostUrl+'/t?'+arr.join('&');
        }
    };



    listingSearch.search = function() {
        var deferred = $q.defer();
        showLoader();

        console.log('Search parameters:');
        console.log(listingSearch.parameters);

        var json = JSON.stringify(listingSearch.parameters);
        var sourceUrl = baseHostUrl+'/api/search?search='+json;
        console.log(sourceUrl);
        $http({method: 'GET', url: sourceUrl}).then(function success(response) {
            console.log('Search results:');
            console.log(response);

            listingSearch.hotels = response.data;

            hideLoader();
            deferred.resolve('Success');
        }, function failed() {
            hideLoader();
            deferred.reject('Fail');
        });

        return deferred.promise;
    };

    listingSearch.submitLocationSelection = function() {
        var locObj = {};
        var location = '';
        if (listingSearch.selectedModalRegion.name != undefined) {
            locObj = listingSearch.selectedModalRegion;
            location = listingSearch.selectedModalRegion.name+', '+listingSearch.selectedModalCountry.name;
        } else if (listingSearch.selectedModalCountry.name != undefined) {
            locObj = listingSearch.selectedModalCountry;
            location = listingSearch.selectedModalCountry.name;
        }

        if (locObj.name != undefined) {
            var region = {
                id: locObj.id,
                name: locObj.name,
                location: location,
                icon: 'bullseye'
            };
            listingSearch.parameters.regions.value.push(region);

            if ($('#modal-location-selector').data('area') != undefined && $('#modal-location-selector').data('area') == 'main') {
                listingSearch.parameters.country = region.location;
                listingSearch.parameters.countryID = listingSearch.selectedModalCountry.id;
            }

            listingSearch.departureAvailable();
        }

        if ($('#inputPlace').length)
            $('#inputPlace').addClass('closed');
        $('#modal-location-selector').modal('hide');
    };



    listingSearch.formatAdults = function() {
        var parts = [];

        if (listingSearch.parameters.adults != '')
            parts.push(listingSearch.parameters.adults+' взр.');

        if (listingSearch.parameters.children != '')
            parts.push(listingSearch.parameters.children+' реб.');

        listingSearch.parameters.adultsFormatted = parts.join(', ');
    };



    listingSearch.submitDatesSelection = function() {
        var dayNames = ['ночь', 'ночи', 'ночей'];
        if (listingSearch.parameters.tourType != 0)
            dayNames = ['день', 'дня', 'дней'];

        var dateStr = 'с '+listingSearch.parameters.dateSince;
        dateStr += ' на '+listingSearch.parameters.dateNights+' '+num2str(listingSearch.parameters.dateNights, dayNames);
        if (listingSearch.parameters.dateShift > 0)
            dateStr += ' со сдвигом на '+listingSearch.parameters.dateShift+' '+num2str(listingSearch.parameters.dateShift, ['день', 'дня', 'дней']);
        $('#inputDate').val(dateStr);

        $('#modal-dates-selector').modal('hide');
    };



    listingSearch.showDepartureSelector = function() {
        showLoader();
        var promise = listingSearch.getCitiesTo();

        promise.then(function() {
            $('#modal-departure-selector').modal({backdrop: 'static'});

            setTimeout(function() {
                $('body').find('#modal-departure-selector .search-place-list input[type="checkbox"]').each(function(i, d) {
                    if ($(d).siblings('ins').length == 0) {
                        var style = $(d).attr('style'); //save color
                        $(d).iCheck({
                            checkboxClass: 'icheckbox_square-grey',
                            radioClass: 'iradio_square-grey',
                            increaseArea: '-120%' // optional
                        });
                        $(d).parent('div').attr('style', style); //add color to checkbox
                    }
                });

                hideLoader();
            }, 500);
        });
    };



    listingSearch.setDepartureCity = function(city) {
        var found = -1;
        listingSearch.parameters.departureCity.forEach(function(c, i) {
            if (c.id == city.id) found = i;
        });
        if (found >= 0) {
            listingSearch.parameters.departureCity.splice(found, 1);

            $('#modal-departure-selector').find('.search-place-list').find('a').each(function(i, d) {
                if ($(d).data('city') == city.id)
                    $(d).find('input').iCheck('uncheck');
            });
        } else {
            listingSearch.parameters.departureCity.push({id: city.id, name: city.name});

            $('#modal-departure-selector').find('.search-place-list').find('a').each(function(i, d) {
                if ($(d).data('city') == city.id)
                    $(d).find('input').iCheck('check');
            });
        }

        //format
        var cities = [];
        listingSearch.parameters.departureCity.forEach(function(c) {
            cities.push(c.name);
        });
        listingSearch.parameters.departureCitiesFormatted = cities.join(', ');

        //$('#modal-departure-selector').modal('hide');
    };



    listingSearch.submitDepartureCity = function() {
        $('#modal-departure-selector').modal('hide');
    };



    listingSearch.showDatesSelector = function() {
        showLoader();
        var ctx = $('#lowPricesModal')[0];
        var deferred = $q.defer();

        listingSearch.lowPricesModalChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [],
                datasets: []
            },
            options: generalChartOptions
        });

        listingSearch.rebuildLowPrices().then(function success(response) {
            $('#modal-dates-selector').modal({backdrop: 'static'});
            hideLoader();

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    listingSearch.showAdultsSelector = function() {
        $('#modal-adults-selector').modal({backdrop: 'static'});
    };



    listingSearch.submitAdultsSelection = function() {
        listingSearch.formatAdults();

        $('#modal-adults-selector').modal('hide');
    };



    //Check if departure city and date may be shown
    listingSearch.isDepartureAvailable = true;
    listingSearch.departureAvailable = function() {
        var deferred = $q.defer();
        listingSearch.getCountryID().then(function success(response) {
            var status = (listingSearch.parameters.countryID == 0);
            listingSearch.isDepartureAvailable = status;

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    //Get data for graph
    listingSearch.renderLowPrices = function(response) {
        var labels = [];
        var allDates = [];
        var origLabels = [];
        var chartData = listingSearch.lowPricesModalChart.data;
        chartData.datasets = [];

        listingSearch.lowPricesModalChartData.forEach(function(item, i) {
            if (origLabels.indexOf(item.city_name) != -1) return;
            origLabels.push(item.city_name);

            //set labels from first item
            if (i == 0)
                item.prices.forEach(function(price) {
                    labels.push(formatDateForCharts(price.date));
                });

            //set data
            var newBaseDataset = Object.assign({}, baseChartDataset);

            newBaseDataset.label = item.city_name;
            newBaseDataset.data = [];
            newBaseDataset.borderColor = baseChartColors[chartData.datasets.length];
            newBaseDataset.pointBorderColor = baseChartColors[chartData.datasets.length];
            item.prices.forEach(function(price) {
                if (price.price >= 0)
                    newBaseDataset.data.push(price.price);
                else
                    newBaseDataset.data.push(NaN);
                allDates.push(price.date);
            });

            chartData.datasets.push(newBaseDataset);
        });
        allDates = unique(allDates);

        //build new data
        chartData.labels = labels;
        listingSearch.lowPricesModalChart.update({duration: 1000});

        //create custom legend
        listingSearch.lowPricesModalChartLegend = [];
        chartData.datasets.forEach(function(dataset, i) {
            var minData = [];
            dataset.data.forEach(function(item) {if (!isNaN(item)) minData.push(item)});
            listingSearch.lowPricesModalChartLegend.push({label: dataset.label, color: baseChartColors[i], price: Math.min(...minData)});
        });

        //style checkboxes
        if (listingSearch.lowPricesModalChartDates.length != 0)
            setTimeout(function() {
                console.log('Reinit checkbox!');
                $('.chart-table td input').each(function(i, d) {
                    var obj = $(d);
                    if (!obj.parent('div').hasClass('icheckbox_square-grey')) {
                        var style = obj.attr('style'); //save color
                        obj.iCheck({
                            checkboxClass: 'icheckbox_square-grey',
                            radioClass: 'iradio_square-grey',
                            increaseArea: '-120%' // optional
                        });
                        obj.parent('div').attr('style', style);
                    }
                    obj.parent('div').parent('td')[0].setAttribute('data-clicked', '0');
                });
            }, 200);

        //build dates ruler
        if (listingSearch.lowPricesModalChartDates.length == 0)
            listingSearch.organizeDates(allDates);
    };



    //Get data for graph
    listingSearch.rebuildLowPrices = function() {
        var sourceUrl = baseHostUrl+'/model/ajax-handler.php';
        var deferred = $q.defer();

        $http.get(sourceUrl, {params: {countryID: listingSearch.parameters.countryID, days: listingSearch.parameters.dateNights, tourType: listingSearch.parameters.tourType, action: 'GetComparerPrices'}}).then(function success(response) {
            console.log('Low prices for #'+listingSearch.parameters.countryID+':');
            console.log(response.data);
            listingSearch.lowPricesModalChartData = response.data;
            listingSearch.lowPricesModalChartDataOriginal = response.data;

            listingSearch.renderLowPrices(response);

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    listingSearch.organizeDates = function(rawDates) {
        var datesFormatted = [];
        var monthNames = [
            "Январь", "Февраль", "Март",
            "Апрель", "Май", "Июнь", "Июль",
            "Август", "Сентябрь", "Октябрь",
            "Ноябрь", "Декабрь"
        ];

        rawDates.forEach(function(rawDate, i) {
            var date = new Date(rawDate);
            var monthIndex = date.getMonth();

            var label = '';
            if (date.getFullYear() != (new Date()).getFullYear())
                label = monthNames[monthIndex]+', '+date.getFullYear();
            else
                label = monthNames[monthIndex];

            if (datesFormatted.indexOf(label) == -1)
                datesFormatted.push(label);
        });

        listingSearch.lowPricesModalChartDates = datesFormatted;

        //init first month
        setTimeout(function() {
            if ($('#modal-low-prices-dates').length)
                listingSearch.switchToDate(datesFormatted[0], '#modal-low-prices-dates');
            if ($('#listing-low-prices-dates').length)
                listingSearch.switchToDate(datesFormatted[0], '#listing-low-prices-dates');
        }, 1500);
    };



    listingSearch.switchToDate = function(dateLabel, curSelector) {
        //select item and move slider
        $(curSelector).find('a.active').removeClass('active');
        $(curSelector).find('a[data-date="'+dateLabel+'"]').addClass('active');
        if ($(curSelector).find('a.active').length)
            var positionLeft = $(curSelector).find('a.active').parent('li').position().left;
        else if ($(curSelector).find('a').first().length)
            var positionLeft = $(curSelector).find('a.active').parent('li').position().left;
        else
            var positionLeft = 0;
        $(curSelector).siblings('.dateSlider').find('.dateHandle').css('left', positionLeft+'px');

        var monthNames = [
            "Январь", "Февраль", "Март",
            "Апрель", "Май", "Июнь", "Июль",
            "Август", "Сентябрь", "Октябрь",
            "Ноябрь", "Декабрь"
        ];

        var dateFormatted = [];
        if (dateLabel != undefined)
            dateFormatted = dateLabel.split(', ');
        var dateObj = {};
        if (dateFormatted.length == 1) {
            dateObj['month'] = monthNames.indexOf(dateFormatted[0]);
            dateObj['year'] = (new Date()).getFullYear();
        } else {
            dateObj['month'] = monthNames.indexOf(dateFormatted[0]);
            dateObj['year'] = dateFormatted[1];
        }

        var originalData = copy(listingSearch.lowPricesModalChartDataOriginal);

        originalData.forEach(function(item, i) {
            var newPrices = item.prices;

            var foundOldDate = true;
            while (foundOldDate) {
                foundOldDate = false;
                newPrices.forEach(function(price, j) {
                    var pdate = new Date(price.date);
                    if (!foundOldDate && (pdate.getFullYear() != dateObj.year || pdate.getMonth() != dateObj.month)) {
                        foundOldDate = true;
                        newPrices.splice(j, 1);
                    }
                });
            }
            originalData[i].prices = newPrices;
        });
        listingSearch.lowPricesModalChartData = originalData;

        listingSearch.renderLowPrices();
    };



    listingSearch.sortByName = function(a, b) {
        return a.name.localeCompare(b.name);
    };



    listingSearch.propertyActive = function(prop, value) {
        var active = false;
        if (listingSearch.parameters[prop].indexOf(value) != -1) active = true;

        return active;
    };



    listingSearch.propertyToggle = function(prop, value) {
        var active = listingSearch.parameters[prop].indexOf(value);
        if (active != -1)
            listingSearch.parameters[prop].splice(active, 1);
        else
            listingSearch.parameters[prop].push(value);

        if ($('.filters-block input[data-name="'+prop+value+'"]').length) {
            if (active != -1)
                $('.filters-block input[data-name="'+prop+value+'"]').iCheck('uncheck');
            else
                $('.filters-block input[data-name="'+prop+value+'"]').iCheck('check');
        }


        return true;
    };



    listingSearch.initFromGetVars = function() {
        if (listingSearch.initialized) return;
        setTimeout('showLoader', 100);
        listingSearch.initialized = true;
        console.log('Init by vars...');

        var deferred = $q.defer();

        var pansionPromise = listingSearch.getAllPansions();
        pansionPromise.then(function success(response) {
            setTimeout(function() {
                var style = $('#sidebar-pansions input').attr('style'); //save color
                $('#sidebar-pansions input').iCheck({
                    checkboxClass: 'icheckbox_square-grey',
                    radioClass: 'iradio_square-grey',
                    increaseArea: '-120%' // optional
                });
                $('#sidebar-pansions input').parent('div').attr('style', style); //add color to checkbox
            }, 500);
        });

        var allowedParams = new Array('tourType', 'tt', 'country', 'line', 'adults', 'children', 'dateSince', 'dateShift', 'dateNights', 'departureCity', 'food', 'stars', 'ops');
        var query = window.location.search.substring(1);
        var vars = query.split("&");

        var opsPromise = listingSearch.getAllOperators();
        opsPromise.then(function success(response) {
            for (var i=0;i<vars.length;i++) {
                var pair = vars[i].split("=");
                if (allowedParams.indexOf(pair[0]) < 0) continue;
                varsInitialized = true;

                if (pair[0] == 'ops') {
                    var opIds = pair[1].split(',');
                    var opsList = [];
                    listingSearch.allOperators.avia.forEach(function(op) {
                        if (opIds.indexOf(op.id+'') != -1) opsList.push(op);
                    });
                    listingSearch.allOperators.bus.forEach(function(op) {
                        if (opIds.indexOf(op.id+'') != -1) opsList.push(op);
                    });
                    listingSearch.allOperators.cruise.forEach(function(op) {
                        if (opIds.indexOf(op.id+'') != -1) opsList.push(op);
                    });
                    var opsList = opsList.filter( onlyUnique );
                    listingSearch.parameters['operators'] = opsList;
                    console.log('opsList');
                    console.log(opsList);
                }
            }
        });

        var varsInitialized = false;
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if (allowedParams.indexOf(pair[0]) < 0) continue;
            varsInitialized = true;

            if (pair[0] == 'departureCity') {
                listingSearch.parameters.departureCity = JSON.parse(decodeURI(pair[1]));
            } else if (pair[0] == 'food') {
                listingSearch.parameters['food'] = pair[1].split(',');
            } else if (pair[0] == 'stars') {
                listingSearch.parameters['stars'] = pair[1].split(',');
            } else if (pair[0] == 'line') {
                listingSearch.parameters['line'] = pair[1].split(',');
            } else if (pair[0] == 'tt') {
                listingSearch.parameters['specialTourType'] = decodeURI(pair[1]);
            } else {
                listingSearch.parameters[pair[0]] = decodeURI(pair[1]);
            }
        }
        listingSearch.formatAdults();

        //format departure cities list
        var cities = [];
        listingSearch.parameters.departureCity.forEach(function(c) {
            cities.push(c.name);
        });
        listingSearch.parameters.departureCitiesFormatted = cities.join(', ');

        listingSearch.getOperatorsByType();

        if (varsInitialized)
            listingSearch.countryChanged().then(function success(response) {
                listingSearch.search().then(function success(response) {
                    var ctx = $('#lowPricesListing')[0];
                    var chartOptions = jQuery.extend(true, {}, generalChartOptions);
                    chartOptions.legend = false;
                    chartOptions.onClick = function(e, arr) {
                        var activePoint = this.lastActive[0];

                        if (activePoint !== undefined) {
                            var datasetIndex = activePoint._datasetIndex;
                            var index = activePoint._index;
                            var airport = this.config.data.datasets[datasetIndex].label;
                            var date = this.config.data.labels[index];

                            var locTo = baseHostUrl+'/t?countryID='+listingSearch.parameters.countryID+'&tourType='+listingSearch.parameters.tourType+'&dateSince='+date+'.'+(new Date()).getFullYear();
                            window.location = locTo;
                        }
                    };
                    listingSearch.lowPricesModalChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: [],
                            datasets: []
                        },
                        options: chartOptions
                    });

                    listingSearch.rebuildLowPrices().then(function success(response) {
                        hideLoader();

                        deferred.resolve('Success');
                    }, function failed() {
                        deferred.reject('Fail');
                    });
                }, function failed() {
                    deferred.reject('Fail');
                });
            }, function failed() {
                deferred.reject('Fail');
            });
        else {
            console.log('No vars...');
            hideLoader();
            deferred.resolve('Success');
        }

        return deferred.promise;
    };



    listingSearch.toggleLowPricesChartDataset = function(label) {
        var obj = $('.chart-table td[data-label="' + label + '"]').find('input');

        var chartData = listingSearch.lowPricesModalChart;
        chartData.data.datasets.forEach(function(dataset, i) {
            if (dataset.label == label) {
                if (chartData.data.datasets[i].hidden)
                    chartData.data.datasets[i].hidden = false;
                else
                    chartData.data.datasets[i].hidden = true;

                chartData.update({duration: 0});
            }
        });

        setTimeout(function() {
            $('.chart-table td[data-label="' + label + '"]')[0].setAttribute('data-clicked', '1');

            $('.chart-table td input').each(function(i, d) {
                var obj = $(d);
                if (!obj.parent('div').hasClass('icheckbox_square-grey')) {
                    var style = obj.attr('style'); //save color
                    obj.iCheck({
                        checkboxClass: 'icheckbox_square-grey',
                        radioClass: 'iradio_square-grey',
                        increaseArea: '-120%' // optional
                    });
                    obj.parent('div').attr('style', style);
                }

                console.log('Check');
                console.log(obj.parent('div').parent('td'));
                console.log(obj.parent('div').parent('td').attr('data-clicked'));
                if (obj.parent('div').parent('td').data('clicked') == '1') {
                    obj.parent('div').parent('td')[0].setAttribute('data-clicked', '0');

                    chartData.data.datasets.forEach(function(dataset, i) {
                        if (dataset.label == obj.parent('div').parent('td').data('label')) {
                            console.log('checked: ');
                            console.log(chartData.data.datasets[i].hidden);
                            if (chartData.data.datasets[i].hidden)
                                obj.iCheck('uncheck');
                            else
                                obj.iCheck('check');
                        }
                    });

                }
            });
        }, 200);
    };



    listingSearch.resetParameter = function(parameter) {
        if (parameter == 'price') {
            listingSearch.parameters.priceFrom = 500;
            listingSearch.parameters.priceTo = 1500;
        } else if (parameter == 'stars') {
            listingSearch.parameters.stars = [];
            $('.filters-block input').each(function(i, d) {
                if ($(d).data('name') != undefined && $(d).data('name').indexOf('stars') != -1)
                    $(d).iCheck('uncheck');
            });
        } else if (parameter == 'food') {
            listingSearch.parameters.food = [];
            $('.filters-block input').each(function(i, d) {
                if ($(d).data('name') != undefined && $(d).data('name').indexOf('food') != -1)
                    $(d).iCheck('uncheck');
            });
        } else if (parameter == 'operators') {
            listingSearch.parameters.operators = [];
            listingSearch.getOperatorsByType();
        }

        return true;
    };



    listingSearch.getFilterValue = function(parameter) {
        var output = '';

        if (parameter == 'price') {
            output = 'от '+listingSearch.parameters.priceFrom+' до '+listingSearch.parameters.priceTo+' EUR';
        } else if (parameter == 'stars') {
            if (listingSearch.parameters.stars.length) {
                var stars = [];
                listingSearch.parameters.stars.forEach(function(s) {stars.push(parseInt(s));});
                var min = Math.min(...stars);
                var max = Math.max(...stars);
                if (min != max) output = min+'-'+max;
                else output = max;

                output += ' '+$filter('num2str')(max, ['звезда', 'звезды', 'звезд']);
            } else
                output = '2-5 звезд';
        } else if (parameter == 'food') {
            if (listingSearch.parameters.food.length) {
                output = listingSearch.parameters.food.join(', ');
            } else
                output = 'любое';
        } else if (parameter == 'operators') {
            if (listingSearch.parameters.operators.length) {
                var operators = [];
                listingSearch.parameters.operators.forEach(function(o) {operators.push(o.name);});
                output = operators.join(', ');
            } else
                output = 'не выбран';
        }

        return output;
    };



    listingSearch.searchByOperator = function(opID) {
        listingSearch.parameters.operators = [opID];
        listingSearch.searchHome();
    };



    listingSearch.getOperatorName = function(tour) {
        var name = '';
        var ops = listingSearch.allOperators[tour.transport];
        ops.forEach(function(op) {
            if (op.id == tour.operator_id)
                name = op.name;
        });

        return name;
    };



    listingSearch.getOperatorImg = function(tour) {
        var img = '';
        var ops = listingSearch.allOperators[tour.transport];
        ops.forEach(function(op) {
            if (op.id == tour.operator_id)
                img = op.logo;
        });

        return img;
    };



    listingSearch.initFromGetVars();
});
<?php

if (!defined('selector_country_choose')) define('selector_country_choose', 'Виберіть країну...');
if (!defined('selector_country_choose_graph')) define('selector_country_choose_graph', 'Для відображення графіка - спочатку виберіть країну');
if (!defined('selector_kurort_header')) define('selector_kurort_header', 'Виберіть курорт / регіон');
if (!defined('selector_country_header')) define('selector_country_header', 'Виберіть країну призначення');
if (!defined('selector_date_choose')) define('selector_date_choose', 'Дата поїздки...');
if (!defined('selector_lowcost_header')) define('selector_lowcost_header', 'Графік низьких цін');
if (!defined('selector_lowcost_subheader')) define('selector_lowcost_subheader', 'Ціни на 2 осіб при замовленні туру на 7 днів');
if (!defined('selector_lowcost_from')) define('selector_lowcost_from', 'Тур на');
if (!defined('selector_lowcost_to')) define('selector_lowcost_to', 'днів');
if (!defined('selector_kurort_country')) define('selector_kurort_country', 'Виберіть країну');
if (!defined('selector_kurort_arrival')) define('selector_kurort_arrival', 'Виберіть місце призначення');
if (!defined('selector_kurort_search')) define('selector_kurort_search', 'Швидкий пошук');
if (!defined('selector_kurort_city_choose')) define('selector_kurort_city_choose', 'Виберіть місто');
if (!defined('selector_kurort_city_avia')) define('selector_kurort_city_avia', 'вильоту');
if (!defined('selector_kurort_city_available')) define('selector_kurort_city_available', 'доступні міста');
if (!defined('selector_kurort_dates_header')) define('selector_kurort_dates_header', 'Виберіть дати');
if (!defined('selector_kurort_dates_sdvig')) define('selector_kurort_dates_sdvig', 'із зсувом');
if (!defined('selector_kurort_dates_first')) define('selector_kurort_dates_first', 'дня від обраної дати');
if (!defined('selector_group_pick')) define('selector_group_pick', 'Вкажіть склад групи');
if (!defined('selector_group_adults')) define('selector_group_adults', 'Дорослі');
if (!defined('selector_group_children')) define('selector_group_children', 'Діти');
if (!defined('selector_operator_pick')) define('selector_operator_pick', 'Виберіть операторів');
if (!defined('kurort_placehodler_search')) define('kurort_placehodler_search', 'Париж');
if (!defined('main_not_found')) define('main_not_found', 'Дані по туру не були знайдені');
if (!defined('main_country_choose_graph')) define('main_country_choose_graph', 'Для відображення графіка - спочатку виберіть країну');
if (!defined('selector_header')) define('selector_header', 'Cелектор турів');
if (!defined('selector_subheader')) define('selector_subheader', 'Магазин для швидкого пошуку відповідного туру');
if (!defined('selector_filter')) define('selector_filter', 'Фільтр турів');
if (!defined('selector_operators')) define('selector_operators', 'Туроператори');
if (!defined('selector_operators_pick')) define('selector_operators_pick', 'Вибрати туроператора');
if (!defined('selector_regions_pick')) define('selector_regions_pick', 'Додати регіон');
if (!defined('selector_hotels')) define('selector_hotels', 'Готель');
if (!defined('selector_codes')) define('selector_codes', 'Код туру');
if (!defined('selector_hotels_name')) define('selector_hotels_name', 'Назва готелю');
if (!defined('selector_hotels_star')) define('selector_hotels_star', 'Зірковість готелю');
if (!defined('selector_codes_number')) define('selector_codes_number', 'Числовий код');
if (!defined('selector_food')) define('selector_food', 'Харчування');
if (!defined('selector_hotel_rating')) define('selector_hotel_rating', 'Рейтинг готелю');
if (!defined('selector_hotel_rating_from')) define('selector_hotel_rating_from', 'від');
if (!defined('selector_hotel_sea_line')) define('selector_hotel_sea_line', 'Відстань до моря');
if (!defined('selector_hotel_line')) define('selector_hotel_line', 'лінія');
if (!defined('selector_cruise_line')) define('selector_cruise_line', 'Круїзна лінія');
if (!defined('selector_cruise_liner')) define('selector_cruise_liner', 'Лайнер');
if (!defined('selector_countires')) define('selector_countires', 'країн');
if (!defined('selector_countires_in_tour')) define('selector_countires_in_tour', 'в турі');
if (!defined('selector_period')) define('selector_period', 'Тривалість до');
if (!defined('selector_cost_two')) define('selector_cost_two', 'Ціна на двох');
if (!defined('selector_picked_filters')) define('selector_picked_filters', 'Вибрані фільтри');
if (!defined('selector_country_to')) define('selector_country_to', 'Країна призначення');
if (!defined('selector_country_to_pick')) define('selector_country_to_pick', 'Виберіть країну');
if (!defined('selector_dates')) define('selector_dates', 'Дати поїздки');
if (!defined('selector_loading')) define('selector_loading', 'Завантаження');
if (!defined('selector_tourists')) define('selector_tourists', 'Туристи');
if (!defined('selector_results')) define('selector_results', 'Результати пошуку');
if (!defined('selector_load_more')) define('selector_load_more', 'Завантажити ще');
if (!defined('selector_open_filter')) define('selector_open_filter', 'Відкрити фільтр');

var captcha_required = false;
var firstnameBull = false;
var mailBull = false;
$(document).ready(function () {
    "use strict";
	
	var site_name = window.location.protocol + '//' + window.location.hostname;
    var firstname = $('input[name=u_name]');
    firstname.blur(function(){
        if(firstname.val().length != 0){
            if(firstname.val().length < 2){
                $('input[name=u_name]').parent().next('.color--error').text('Имя не короче 2-х символов');
                $('input[name=u_name]').parent().next('.color--error').fadeIn('fast');
                $('input[name=u_name]').parent('.clickable').addClass('has-error');

            }else if(firstname.val().length > 64){
                $('input[name=u_name]').parent().next('.color--error').text('Имя длиннее 64-х символов');
                $('input[name=u_name]').parent().next('.color--error').fadeIn('fast');
                $('input[name=u_name]').parent('.clickable').addClass('has-error');
            } else {
                $('input[name=u_name]').parent().next('.color--error').text('');
                $('input[name=u_name]').parent().next('.color--error').fadeOut('fast');
                $('input[name=u_name]').parent('.clickable').removeClass('has-error');
            }
        }else{
            $('input[name=u_name]').parent().next('.color--error').text('Введите Ваше имя');
            $('input[name=u_name]').parent().next('.color--error').fadeIn('fast');
            $('input[name=u_name]').parent('.clickable').addClass('has-error');
        }
        if(firstname.parents('.clickable').hasClass('has-error')) {
            firstnameBull = false;
        } else {
            firstnameBull = true;
        }
        if(firstnameBull && mailBull && captcha_required) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });

    var mailPattern = /^[a-z0-9][a-z0-9\._-]*[a-z0-9]*@([a-z0-9]+([a-z0-9-]*[a-z0-9]+)*\.)+[a-z]+/i;
    var mail = $('input[name=u_email]');

    mail.blur(function(){
        if(mail.val().length != 0){
            $('input[name=u_email]').parent().next('.color--error').text('');
            $('input[name=u_email]').parent().next('.color--error').fadeOut('fast');
            if(mail.val().search(mailPattern) == 0){
                $('input[name=u_email]').parent().next('.color--error').text('');
                $('input[name=u_email]').parent().next('.color--error').fadeOut('fast');
                $('input[name=u_email]').parent('.clickable').removeClass('has-error');
            }else{
                $('input[name=u_email]').parent().next('.color--error').text('Введите корректный Email');
                $('input[name=u_email]').parent().next('.color--error').fadeIn('fast');
                $('input[name=u_email]').parent('.clickable').addClass('has-error');
            }
        }else{
            $('input[name=u_email]').parent().next('.color--error').text('Введите Email-адрес');
            $('input[name=u_email]').parent().next('.color--error').fadeIn('fast');
            $('input[name=u_email]').parent('.clickable').addClass('has-error');
        }
        if(mail.parents('.clickable').hasClass('has-error')) {
            mailBull = false;
        } else {
            mailBull = true;
        }
        if(firstnameBull && mailBull && captcha_required) {
            $('button[type=submit]').attr('disabled', false);
        } else {
            $('button[type=submit]').attr('disabled', true);
        }
    });


    var buttonSubmit = $('button[type=submit]');
    var ContactForm = $('#contacts-form-ajax');

    ContactForm.on('submit', function(e){
        e.preventDefault();
            $.ajax({
                type: 'post',
                url: site_name + '/model/ajax-handler.php', 
                data: {form_data: $('#contacts-form-ajax').serialize(), locale: 'ru', action: 'contacts_send'}
            }).done( function () {
                swal({
                    title: "Спасибо за Ваше сообщение",
                    text: "Мы получили Ваше сообщение и ответим на него по Email. Наша команда поддержки обработает Ваш запрос в порядке очереди.",
                    type: "success",
                    confirmButtonText: "Продолжить",
                });
                $('#contacts-form-ajax').closest('form').find("input, textarea").val("");
            }).fail(function() {
                swal({
                    title: "Ой, что-то пошло не так...",
                   text: "Во время отправки запроса сервер ответил с ошибкой. Пожалуйста, првоерьте введеннные Вами данные и попробуйте ещё раз. Если ошибка повторяется - свяжитесь с нами по другому каналу связи.",
				  type: "warning",
                    confirmButtonText: "Повторить",
                });
            });
    });
});

function onloadCaptcha() {
	grecaptcha.render('captcha-container', {
		'theme': 'light',
		'sitekey': '6Lf-xngUAAAAAGNSp8SqD_pHd6fA5BG4v2vxCres',
		'expired-callback': captcha_expired,
		'callback': captcha_callback,
		'lang' : 'ru'
	});
}

function captcha_callback() {
	captcha_required = true;
	if(firstnameBull && mailBull && captcha_required) {
		$('button[type=submit]').attr('disabled', false);
	} else {
		$('button[type=submit]').attr('disabled', true);
	}
}

function captcha_expired() {
	captcha_required = false;
	if(firstnameBull && mailBull && captcha_required) {
		$('button[type=submit]').attr('disabled', false);
	} else {
		$('button[type=submit]').attr('disabled', true);
	}
}
<?php

if (!defined('no_tour_id')) define('no_tour_id', 'ID туру не визначений');
if (!defined('no_prices_found')) define('no_prices_found', 'Ціна туру не визначена');
if (!defined('no_price_exists')) define('no_price_exists', 'Ціни на тур не існує');
if (!defined('no_curency_exists')) define('no_curency_exists', 'Валюта не знайденa в Базі');
if (!defined('orders_limit_signup')) define('orders_limit_signup', 'Сьогодні Вам був створений новий акаунт, авторизуйтесь.');
if (!defined('no_email_listed')) define('no_email_listed', 'Необхідно вказати Email');
if (!defined('email_error')) define('email_error', 'Email введений з помилками');
if (!defined('please_auth')) define('please_auth', 'Ми знайшли Ваш Email, %s! Будь ласка, введіть пароль, щоб купити тур:');
if (!defined('login_error')) define('login_error', 'Неправильний Email або пароль');
if (!defined('activate_account')) define('activate_account', 'Активуйте Аккаунт перед покупкою');
if (!defined('cant_order')) define('cant_order', 'Ви не можете покупати на TravelNet');

if (!defined('tour_go_email')) define('tour_go_email', 'Введіть Email для бронювання');
if (!defined('tour_book_text')) define('tour_book_text', 'Щоб продовжити бронювання, введіть свій Email. На нього будуть приходити повідомлення і підтвердження дій на сайті');
if (!defined('tour_email_pl')) define('tour_email_pl', 'Ваш Email');

if (!defined('tour_adults')) define('tour_adults', 'Дорослi');
if (!defined('tour_adults_pl')) define('tour_adults_pl', 'Кiл-ть дорослих');
if (!defined('tour_children')) define('tour_children', 'Дiти');
if (!defined('tour_children_pl')) define('tour_children_pl', 'Кiл-ть дiтей');
if (!defined('tour_length')) define('tour_length', 'Тривалість туру');
if (!defined('tour_cities')) define('tour_cities', 'Міст на маршруті');
if (!defined('tour_days')) define('tour_days', 'днів');
if (!defined('tour_cities_text')) define('tour_cities_text', 'міст');
if (!defined('tour_watch_prices')) define('tour_watch_prices', 'Подивитися ціни');
if (!defined('tour_currency')) define('tour_currency', 'Валюта');
if (!defined('tour_buy_header')) define('tour_buy_header', 'Купити тур');
if (!defined('tour_buy_subheader')) define('tour_buy_subheader', 'ціна квитків вказана за одну людину');
if (!defined('tour_book')) define('tour_book', 'Забронювати');
if (!defined('tour_info_header')) define('tour_info_header', 'Інформація про тур');
if (!defined('tour_info_desc')) define('tour_info_desc', 'Опис');
if (!defined('tour_info_program')) define('tour_info_program', 'Програма');
if (!defined('tour_star')) define('tour_star', 'До обранного');
if (!defined('tour_bus_tour')) define('tour_bus_tour', 'Автобусний тур');
if (!defined('tour_bus_about')) define('tour_bus_about', 'на TravelNet Tour - маршрут, цiни, iнформацiя');
if (!defined('tour_bus_details')) define('tour_bus_details', 'Детальніше');
if (!defined('tour_bus_email_text')) define('tour_bus_email_text', 'Інформація про автобусний тур');
if (!defined('text_tour_on')) define('text_tour_on', 'на TravelNet Tour');
if (!defined('tour_contacts')) define('tour_contacts', 'Контакти');
if (!defined('tour_no_details')) define('tour_no_details', 'Інформація про тур уточнюється на сайті Туроператора');
if (!defined('tour_goto_operator')) define('tour_goto_operator', 'Опис на сайті Туроператора');
if (!defined('tour_day_no')) define('tour_day_no', 'День');
if (!defined('tour_no_programm')) define('tour_no_programm', 'Програма туру доступна на сайті Туроператора');
if (!defined('tour_currency')) define('tour_go_email', 'Введіть Email для бронювання');
if (!defined('tour_book_text')) define('tour_book_text', 'Щоб продовжити бронювання, введіть свій Email. На нього будуть приходити повідомлення і підтвердження дій на сайті');
if (!defined('tour_email_pl')) define('tour_email_pl', 'Ваш Email');

if (!defined('text_pieces')) define('text_pieces', 'Купити у розстрочку');
if (!defined('text_tour')) define('text_tour', 'Тур');
if (!defined('text_info')) define('text_info', 'Детальна інформація про тур %s - ціни, дорога, фото і дати виїзду. Порівняйте ціни різних туроператорів і виберіть найдешевшу поїздку.');
if (!defined('text_ograph_img')) define('text_ograph_img', 'вибір турів всіх туроператорів України Ograph image');

if (!defined('email_confirm')) define('email_confirm', 'Підтвердження Email на');
if (!defined('email_text')) define('email_text', 'Привіт, Ви тільки що почали замовлення свого туру на TravelNet Tour! <br/> 
												  Після оформлення замовлення Вам необхідно буде перейти в Особистий кабінет. Щоб це зробити, 
												  необхідно активувати свій акаунт: <br/><br/></p><p style="text-align:center;">
												  <a class="btn btn-primary btn-lg raised" href="%sactivation?token=%s&email=%s">Активувати</a> 
												  <br/><br/></p><p>Для першого доступу y Ваш Аккаунт ми сгенерували Вам пароль: <strong>%s
												  </strong>. Ми дуже рекомендуємо його поміняти при першому вході в Особистий кабінет, в розділі 
												  "Профіль" -> "Змінити пароль". <br/> <br/>В іншому випадку, якщо Ви не робили покупок на 
												  TravelNet Tour, будь ласка, видаліть це повідомлення. <br/>Якщо у Вас є пропозиції, зауваження 
												  або питання щодо сайту, будь ласка, зв\'яжіться з нами через <a href="%scontacts">Форму 
												  зворотнього зв\'язку</a> або іншим зручним чином. <br/><br/><small>Повідомлення було 
												  згенеровано автоматично, будь ласка, не відповідайте на нього. <br/>З найкращими побажаннями, 
												  <br/> Команда TravelNet Tour </small>');

if (!defined('tour_info_credit')) define('tour_info_credit', 'Як купити в розстрочку?');
if (!defined('tour_info_credit_header')) define('tour_info_credit_header', 'Коротка інструкція по покупці в розстрочку');
if (!defined('tour_info_credit_text')) define('tour_info_credit_text', 'На сайті TravelNet Tour доступні різноманітні форми оплати, в тому числі
												і оплата частинами або в розстрочку. Щоб оплатити в розстрочку необхідно натиснути кнопку "Забронювати", пройти
												процес оформлення замовлення і на етапі оплати вибрати бажаний банк для оплати частинами. Можна перевірити,
												чи можете Ви скористатися подібною послугою, ввівши номер телефону, перед вибором кількості платежів.
												Далі Ви будете перенаправлені на сторінку банку, де можна детально ознайомиться з їх умовами розстрочки. <br/> <br/>
												Зверніть увагу, що TravelNet Tour не гарантує, що Ви можете купити тур в розстрочку, оскільки остаточне
												рішення залишається за банком. Тому, будь ласка, перш ніж оплачувати замовлення частинами, переконайтеся, що Вам доступна така послуга в банку, де Ви обслуговуєтесь.');

									
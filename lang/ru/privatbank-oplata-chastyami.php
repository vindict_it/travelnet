<?php

if (!defined('page_bank_header')) define('page_bank_header', 'Оплата частями от ПриватБанка');
if (!defined('page_bank_desc')) define('page_bank_desc', 'Если Вы клиент ПриватБанка и хотите купить тур при помощи услуги "Оплата частями", ознакомтесь с условиями прежде, чем совершать покупку.');
<?php

if (!defined('page_banks')) define('page_banks', 'Розстрочка платежу');

if (!defined('pb_alt')) define('pb_alt', 'Оплата частинами от ПриватБанка на TravelNet Tour');
if (!defined('pb_title')) define('pb_title', 'Оплата частинами ПриватБанк');
if (!defined('pb_header')) define('pb_header', 'Оплата частинами ПриватБанк');
if (!defined('alfa_alt')) define('alfa_alt', 'Оплата у розстрочку вiд Альфа Банку на TravelNet Tour');
if (!defined('alfa_title')) define('alfa_title', 'Розстрочка Альфа Банк');
if (!defined('alfa_header')) define('alfa_header', 'Кредитування Альфа Банку');
if (!defined('mono_alt')) define('mono_alt', 'Оплата частинами вiд Монобанка на TravelNet Tour');
if (!defined('mono_title')) define('mono_title', 'Оплата частинами Монобанк');
if (!defined('mono_header')) define('mono_header', 'Оплата частинами Монобанк');
if (!defined('any_bank_alt')) define('any_bank_alt', 'Оплата у кредит на TravelNet Tour');
if (!defined('any_bank_title')) define('any_bank_title', 'Кредитування на TravelNet');
if (!defined('any_bank_header')) define('any_bank_header', 'Кредитування при оплатi замовлення');

if (!defined('page_callback')) define('page_callback', 'Передзвонiть менi');
if (!defined('page_email_me')) define('page_email_me', 'Написати нам');

if (!defined('page_support')) define('page_support', 'Пiдтримка');
if (!defined('page_low_prices')) define('page_low_prices', 'Мінімальні ціни');
if (!defined('page_low_graph')) define('page_low_graph', 'Графік цін');
if (!defined('page_directions')) define('page_directions', 'Напрями');
if (!defined('directions_soon')) define('directions_soon', 'Скоро відкриється');
if (!defined('directions_beach')) define('directions_beach', 'Пляжні тури');
if (!defined('directions_avia')) define('directions_avia', 'Авіа тури');
if (!defined('directions_bus')) define('directions_bus', 'Автобусні тури');
if (!defined('directions_cruise')) define('directions_cruise', 'Круїзи');
if (!defined('directions_ski')) define('directions_ski', 'Гірськолижні тури');
if (!defined('directions_sales')) define('directions_sales', 'Акційні тури');
if (!defined('directions_booking')) define('directions_booking', 'Раннє бронювання');
if (!defined('directions_last_moment')) define('directions_last_moment', 'В останню мить');
if (!defined('page_menu')) define('page_menu', 'Меню');
if (!defined('page_details')) define('page_details', 'Детальніше');
if (!defined('page_charters')) define('page_charters', 'Чартери');
if (!defined('page_services')) define('page_services', 'Сервіси');
if (!defined('services_about')) define('services_about', 'Про нас');
if (!defined('services_offer')) define('services_offer', 'Договір оферти');
if (!defined('services_insurance')) define('services_insurance', 'Страхування');
if (!defined('services_best_offers')) define('services_best_offers', 'Кращі пропозиції');
if (!defined('services_operators_sales')) define('services_operators_sales', 'Акції туроператорів');
if (!defined('services_countries')) define('services_countries', 'Країнознавство');
if (!defined('page_favourite')) define('page_favourite', 'Улюблене');
if (!defined('page_blog')) define('page_blog', 'Блог');
if (!defined('page_tickets')) define('page_tickets', 'Квитки');
if (!defined('ticket_avia')) define('ticket_avia', 'Авіа перельоти');
if (!defined('ticket_train')) define('ticket_train', 'Потяг');
if (!defined('ticket_bus')) define('ticket_bus', 'Автобус');
if (!defined('page_choose_language')) define('page_choose_language', 'Виберіть мову');
if (!defined('menu_choose_language')) define('menu_choose_language', 'Обрати мову');
if (!defined('page_cancel')) define('page_cancel', 'Відміна');
if (!defined('page_apply')) define('page_apply', 'Застосувати');
if (!defined('page_close')) define('page_close', 'Закрити');
if (!defined('page_back')) define('page_back', 'Назад');
if (!defined('page_next')) define('page_next', 'Далі');
if (!defined('page_add')) define('page_add', 'Додати');
if (!defined('page_search')) define('page_search', 'Пошук');
if (!defined('common_continue')) define('common_continue', 'Продовжити');
if (!defined('page_lang')) define('page_lang', 'Мова');
if (!defined('page_country')) define('page_country', 'Країна');
if (!defined('page_currency')) define('page_currency', 'Оберiть валюту');
if (!defined('page_watch')) define('page_watch', 'Подивитись');
if (!defined('page_loading')) define('page_loading', 'Завантаження');
if (!defined('page_choose_currency')) define('page_choose_currency', 'Виберіть валюту');
if (!defined('choose_currency')) define('choose_currency', 'Валюта');
if (!defined('footer_instruments')) define('footer_instruments', 'Інструменти');
if (!defined('instruments_discont')) define('instruments_discont', 'Дисконт-тур');
if (!defined('instruments_podbor')) define('instruments_podbor', 'Підбір туру');
if (!defined('instruments_bset_prices')) define('instruments_bset_prices', 'Вибір кращих цін');
if (!defined('footer_how_it_works')) define('footer_how_it_works', 'Як це працює?');
if (!defined('how_about')) define('how_about', 'Про нас');
if (!defined('how_dogovor')) define('how_dogovor', 'Договір оферти');
if (!defined('how_payment')) define('how_payment', 'Оплата послуг');
if (!defined('how_questions')) define('how_questions', 'Питання та вiдповiдi');
if (!defined('footer_users')) define('footer_users', 'Користувачам');
if (!defined('users_contacts')) define('users_contacts', 'Контакти');
if (!defined('users_feedback')) define('users_feedback', 'Зворотній зв\'язок');
if (!defined('users_policy')) define('users_policy', 'Політика конфіденційності');
if (!defined('users_account')) define('users_account', 'Особистий кабінет');
if (!defined('users_signin')) define('users_signin', 'Увiйти');
if (!defined('users_register')) define('users_register', 'Реєстрація');
if (!defined('footer_copy')) define('footer_copy', date('Y').' &copy; Всі права захищені');
if (!defined('footer_copy_text')) define('footer_copy_text', 'Копіювання матеріалів сайту дозволяється лише з активним посиланням на сайт');
if (!defined('footer_vd')) define('footer_vd', 'Розробка і підтримка проекту <a href="https://vindict.com.ua" target="_blank" rel="noopener" class="footer-link" rel="nofollow">Vindict</a>');
<?php

if (!defined('order_flight_step1')) define('order_flight_step1', 'Бронирование');
if (!defined('order_flight_step2')) define('order_flight_step2', 'Подтверждение');
if (!defined('order_flight_step3')) define('order_flight_step3', 'Оплата');
if (!defined('order_flight_step4')) define('order_flight_step4', 'Выдача документов');
if (!defined('processing_specialists')) define('processing_specialists', 'Данные о выбранном туре обрабатываются нашими специалистами');
if (!defined('processing_doing')) define('processing_doing', 'Обработка займет не более суток');
if (!defined('processing_got')) define('processing_got', 'Ваши данные поступили в обработку. Как только они будут проверены - Вы получите соответствующее уведомление и инструкции на электронную почту и в');
if (!defined('processing_lk')) define('processing_lk', 'личном кабинете');
if (!defined('processing_reading')) define('processing_reading', 'Пока Вы ожидаете, рекомендуем к прочтению');
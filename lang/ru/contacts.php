<?php

//Contacts page
define("page_main_header", "Наши котнтакты");
define("page_emails", "Напишите нам на Email");
define("page_phones", "Позвоните нам");
define("page_socials", "Мы в социальных сетях");
define("page_feedback", "Форма обратной связи");
define("page_message", "Ваше сообщение");
define("page_name", "Ваше имя");
define("page_email", "Ваш Email");
define("page_auth", "Чем мы можем Вам помочь");
define("page_text_placeholder", "Я хочу сказать");
define("page_send", "Отправить");
define("page_regards", "С наилучшими пожеланиями, Команда поддержки Travelnet Tour");
define("page_callcenter", "График работы колл-центра");
define("page_from", "c");
define("page_to", "до");
define("page_saturday", "В субботу");
define("page_sunday", "В воскресенье");

define("page_code", "код ЕГРПОУ");
define("page_company", "ООО");
define("page_address", "Адрес: Украина, г. Харьков, пр. Науки 31А, 61072");
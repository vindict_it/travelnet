<?php

// login page info
if (!defined('page_main_header')) define('page_main_header', 'Личный кабинет');
if (!defined('page_header')) define('page_header', 'Войти в аккаунт');
if (!defined('page_details')) define('page_details', 'Подробнее');
if (!defined('page_log_in')) define('page_log_in', 'Авторизация');
if (!defined('page_pass_forgot')) define('page_pass_forgot', 'Забыли пароль?');
if (!defined('login_empty')) define('login_empty', 'Вы не можеет войти, пока не активируете Аккаунт. Проверьте почту, мы прислали Вам письмо с инструкциями активации.');
if (!defined('login_invalid')) define('login_invalid', 'Вы ввели неправильные Email или пароль');
if (!defined('login_warning')) define('login_warning', 'Внимание');
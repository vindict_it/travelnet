<?php

// login page info
if (!defined('page_main_header')) define('page_main_header', 'Особистий кабінет');
if (!defined('page_header')) define('page_header', 'Увійти в акаунт');
if (!defined('page_details')) define('page_details', 'Детальніше');
if (!defined('page_log_in')) define('page_log_in', 'Авторизація');
if (!defined('page_pass_forgot')) define('page_pass_forgot', 'Забули пароль?');
if (!defined('login_empty')) define('login_empty', 'Ви не можеет увійти, поки не активуєте Аккаунт. Перевірте пошту, ми надіслали Вам лист з інструкціями активації.');
if (!defined('login_invalid')) define('login_invalid', 'Ви ввели неправильні Email або пароль');
if (!defined('login_warning')) define('login_warning', 'Увага');

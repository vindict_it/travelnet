<?php

if (!defined('blog_share')) define('blog_share', 'Подiлитися статею');
if (!defined('blog_title_word')) define('blog_title_word', 'Стаття');
if (!defined('blog_read_word')) define('blog_read_word', 'Читати');
if (!defined('blog_from')) define('blog_from', 'Статтi iз блога');
if (!defined('blog_details')) define('blog_details', 'Детальнiше');
if (!defined('blog_articles')) define('blog_articles', 'Iншi статті');
if (!defined('blog_comments')) define('blog_comments', 'Коментарi');
<?php

if (!defined('order_flight_step1')) define('order_flight_step1', 'Бронирование');
if (!defined('order_flight_step2')) define('order_flight_step2', 'Подтверждение');
if (!defined('order_flight_step3')) define('order_flight_step3', 'Оплата');
if (!defined('order_flight_step4')) define('order_flight_step4', 'Выдача документов');
if (!defined('order_method')) define('order_method', 'Выберите способ оплаты');
if (!defined('order_method_card')) define('order_method_card', 'Оплата картой любого другого банка');
if (!defined('order_method_credit')) define('order_method_credit', 'Оплата в кредит');
if (!defined('order_method_many_payments')) define('order_method_many_payments', 'Оплата в рассрочку');
if (!defined('order_method_emoney')) define('order_method_emoney', 'Электронные платежи (Portmone, LiqPay и другие)');
if (!defined('order_method_bank')) define('order_method_bank', 'Оплата безналичного счета через отделение банка');
if (!defined('order_method_agent')) define('order_method_agent', 'Оплата наличными через турагента');
if (!defined('order_method_courier')) define('order_method_courier', 'Оплата наличными через курьера');
if (!defined('order_clients')) define('order_clients', 'Ваш заказ');
if (!defined('order_price')) define('order_price', 'Цена тура');
if (!defined('order_price_subheader')) define('order_price_subheader', 'С учетом всех платежей');
if (!defined('order_price_discount')) define('order_price_discount', 'Скидка');
if (!defined('order_price_payment')) define('order_price_payment', 'Оплата тура');
if (!defined('order_do_payment')) define('order_do_payment', 'Оплатить');
if (!defined('order_ssl')) define('order_ssl', 'Соединение защищено SSL-шифрованием');
if (!defined('order_one_step')) define('order_one_step', 'Вы уже в шаге от отдыха!');
if (!defined('order_loading')) define('order_loading', 'загрузка данных');
if (!defined('order_hotel_info')) define('order_hotel_info', 'Информация об отеле');
if (!defined('order_tour_info')) define('order_tour_info', 'Информация о туре');
if (!defined('order_flight_info')) define('order_flight_info', 'Информация о перелете');
if (!defined('order_route_info')) define('order_route_info', 'Информация о маршруте');
if (!defined('order_night1')) define('order_night1', 'ночь');
if (!defined('order_night2')) define('order_night2', 'ночи');
if (!defined('order_night3')) define('order_night3', 'ночей');
if (!defined('order_days1')) define('order_days1', 'день');
if (!defined('order_days2')) define('order_days2', 'дня');
if (!defined('order_days3')) define('order_days3', 'дней');
if (!defined('order_flight_to')) define('order_flight_to', 'Туда');
if (!defined('order_flight_from')) define('order_flight_from', 'Обратно');
if (!defined('order_clients')) define('order_clients', 'Информация о клиентах');
if (!defined('order_client_tourist')) define('order_client_tourist', 'Турист');
if (!defined('order_client_buyer')) define('order_client_buyer', 'Покупатель');
if (!defined('order_client_buyer')) define('order_client_buyer', 'Информация о клиентах');
if (!defined('order_client_lname')) define('order_client_lname', 'Фамилия');
if (!defined('order_client_lname_placeholder')) define('order_client_lname_placeholder', 'Иванов');
if (!defined('order_client_fname')) define('order_client_fname', 'Имя');
if (!defined('order_client_fname_placeholder')) define('order_client_fname_placeholder', 'Иван');
if (!defined('order_client_email')) define('order_client_email', 'E-mail');
if (!defined('order_client_phone')) define('order_client_phone', 'Номер телефона');
if (!defined('order_tourist_passport')) define('order_tourist_passport', 'По паспорту');
if (!defined('order_tourist_date')) define('order_tourist_date', 'Дата рождения');
if (!defined('order_tourist_national')) define('order_tourist_national', 'Гражданство');
if (!defined('order_tourist_no_pass')) define('order_tourist_no_pass', 'Номер паспорта');
if (!defined('order_tourist_pass_dates')) define('order_tourist_pass_dates', 'Срок действия');
if (!defined('payment_not_listed')) define('payment_not_listed', 'не указано');

if (!defined('month_1')) define('month_1', 'января');
if (!defined('month_2')) define('month_2', 'февраля');
if (!defined('month_3')) define('month_3', 'марта');
if (!defined('month_4')) define('month_4', 'апреля');
if (!defined('month_5')) define('month_5', 'мая');
if (!defined('month_6')) define('month_6', 'июня');
if (!defined('month_7')) define('month_7', 'июля');
if (!defined('month_8')) define('month_8', 'августа');
if (!defined('month_9')) define('month_9', 'сентября');
if (!defined('month_10')) define('month_10', 'октября');
if (!defined('month_11')) define('month_11', 'ноября');
if (!defined('month_12')) define('month_12', 'декабря');

if (!defined('order_tour_img_alt')) define('order_tour_img_alt', 'Выбирайте тур "%s" на сайте %s');
if (!defined('order_cities')) define('order_cities', 'городов');

if (!defined('adults_text')) define('adults_text', 'взрослых');
if (!defined('adult_text')) define('adult_text', 'взрослый');

if (!defined('auth_to_continue')) define('auth_to_continue', 'Авторизуйтесь, чтобы продолжить');
if (!defined('enter_card')) define('enter_card', 'Введите номер карты');
if (!defined('enter_holder_name')) define('enter_holder_name', 'Введите владельца карты');
if (!defined('enter_expires')) define('enter_expires', 'Введите поле "истекает"');
if (!defined('enter_cvc')) define('enter_cvc', 'Введите cvc-код');
if (!defined('bank_not_found')) define('bank_not_found', 'Выберите банк из списка');

if (!defined('bank_privatbank')) define('bank_privatbank', 'Оплата через ПриватБанк на сайте TravelNet Tour');
if (!defined('bank_privatbank_title')) define('bank_privatbank_title', 'Оплатить через ПриватБанк');
if (!defined('pay_p24')) define('pay_p24', 'Оплата через Приват24');
if (!defined('pay_p24_alt')) define('pay_p24_alt', 'Оплата на сайте TravelNet Tour через Приват24');
if (!defined('pb_pieces')) define('pb_pieces', 'Оплата частями');
if (!defined('pay_pb_pieces')) define('pay_pb_pieces', 'Оплата частями ПриватБанк');
if (!defined('pay_pb_pieces_alt')) define('pay_pb_pieces_alt', 'Оплата на сайте TravelNet Tour часятми в ПриватБанке');
if (!defined('pb_cahsbox')) define('pb_cahsbox', 'Касса');
if (!defined('pay_pb_cahsbox')) define('pay_pb_cahsbox', 'Оплата в кассе ПриватБанка');
if (!defined('pay_pb_cahsbox_alt')) define('pay_pb_cahsbox_alt', 'Оплата заказа TravelNet Tour в кассе ПриватБанка');

if (!defined('bank_alfabank')) define('bank_alfabank', 'Оплата через Альфа Банк на сайте TravelNet Tour');
if (!defined('bank_alfabank_title')) define('bank_alfabank_title', 'Оплатить через Альфа Банк');
if (!defined('ab_acq')) define('ab_acq', 'Оплатить счет');
if (!defined('pay_ab_acq')) define('pay_ab_acq', 'Оплатить счет в Альфа Банк');
if (!defined('pay_ab_acq_alt')) define('pay_ab_acq_alt', 'Оплата счет на сайте Альфа Банк через своё приложение');
if (!defined('ab_acq_online')) define('ab_acq_online', 'Оплатить онлайн');
if (!defined('pay_ab_acq_online')) define('pay_ab_acq_online', 'Оплатить онлайн в Альфа Банк');
if (!defined('pay_ab_acq_online_alt')) define('pay_ab_acq_online_alt', 'Оплата онлайн на сайте TravelNet Tour через Альфа Банк');

if (!defined('pay_ab_acq_desc')) define('pay_ab_acq_desc', 'Оплата туристических услуг за Заказ #%s на сайте TravelNet Tour.');

if (!defined('bank_monobank')) define('bank_monobank', 'Оплата через МоноБанк на сайте TravelNet Tour');
if (!defined('bank_monobank_title')) define('bank_monobank_title', 'Оплатить через МоноБанк');
if (!defined('mono_pieces')) define('mono_pieces', 'Оплата частями');
if (!defined('pay_mono_pieces')) define('pay_mono_pieces', 'Оплата частями МоноБанк');
if (!defined('pay_mono_pieces_alt')) define('pay_mono_pieces_alt', 'Оплата на сайте TravelNet Tour частями в МоноБанке');
if (!defined('mono_mobile')) define('mono_mobile', 'Оплатить');
if (!defined('pay_mono_mobile')) define('pay_mono_mobile', 'Оплата в мобильном приложении МоноБанк');
if (!defined('pay_mono_mobile_alt')) define('pay_mono_mobile_alt', 'Оплата на сайте TravelNet Tour в мобильном приложении МоноБанк');

if (!defined('cashbox_info_main_header')) define('cashbox_info_main_header', 'Оплата в кассе');
if (!defined('cashbox_info_header')) define('cashbox_info_header', 'Как оплатить заказ в кассе?');
if (!defined('cashbox_info_text')) define('cashbox_info_text', 'Для того, чтобы оплатить заказ в кассе ПриватБанка, Вам необходимо прийти в любое
                                        ближайшее отделение. В кассе необходимо указать, что Вы хотите оплатить услуги компании TravelNet Tour и 
                                        назвать свой код налогоплательщика (ИНН), который Вы указали при заполении заказа. Ваш заказ будет найден 
                                        автоматически, кассир его дополнительно озвучит, после чего предложит оплатить. После чего оплатить 
                                        заказ в полном объеме. После успешной оплапты, Ваш заказ на сайте автоматически перейдет в статус "Оплачено" и 
                                        Ваши данные будут переданы туроператору для подтверждения и выдачи документов.');

if (!defined('pp_product_name')) define('pp_product_name', 'Тур #%s "%s" на TravelNet Tour');
if (!defined('transaction_failed')) define('transaction_failed', 'Банку не удалось создать платеж, попробуйте повторить позже или перезагрузить страницу.');
if (!defined('transaction_expired')) define('transaction_expired', 'Ваш запрос был отклонен банком, попробуйте повторить позже или перезагрузить страницу.');
if (!defined('db_fail')) define('db_fail', 'Произошла ошибка на сайте, возможно ведуться регламентные работы. Пожалуйста, перезагрузите страницу.');



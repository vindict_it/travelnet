<?php

if (!defined('selector_kurort_city_available')) define('selector_kurort_city_available', 'Доступные города');
if (!defined('not_found_header')) define('not_found_header', 'На выбранный Вами период туры не найдены');
if (!defined('not_found_text')) define('not_found_text', 'Похоже, туров по выбранным параметрам нет, или их раскупили. Попробуйте выставить другую дату, или выбрать из предложенных ниже.
														  Если хотите автоматически найти ближайшие доступные туры - нажмите кнопку ниже и мы найдем все доступные предложения!');
if (!defined('not_found_btn')) define('not_found_btn', 'Автоматический поиск туров');

if (!defined('tour_go_email')) define('tour_go_email', 'Введите Email для бронирования');
if (!defined('tour_book_text')) define('tour_book_text', 'Чтобы продолжить бронирование, введите свой Email. На него будут приходить уведомления и подтверждения действий на сайте');
if (!defined('tour_email_pl')) define('tour_email_pl', 'Ваш Email');
if (!defined('cant_order')) define('cant_order', 'Вы не можете покупать на TravelNet');

if (!defined('hotel_favorites')) define('hotel_favorites', 'В избранное');
if (!defined('hotel_share')) define('hotel_share', 'Поделиться');
if (!defined('hotel_hotel')) define('hotel_hotel', 'Отель');
if (!defined('hotel_credentials')) define('hotel_credentials', 'на TravelNet Tour - вылет, цены, информация');
if (!defined('hotel_details')) define('hotel_details', 'Подробнее');
if (!defined('hotel_sitename')) define('hotel_sitename', 'на TravelNet Tour');
if (!defined('hotel_meta_desc')) define('hotel_meta_desc', 'Сравните цены разных туроператоров на это направление и выберите самую дешевую поездку.');
if (!defined('hotel_details_meta')) define('hotel_details_meta', 'Подробности об отеле');
if (!defined('hotel_info_text')) define('hotel_info_text', 'Информация об отеле');
if (!defined('hotel_prices')) define('hotel_prices', 'Посмотреть цены');
if (!defined('hotel_photos_map')) define('hotel_photos_map', 'Фото + карта');
if (!defined('hotel_about')) define('hotel_about', 'Об отеле');
if (!defined('hotel_services')) define('hotel_services', 'Услуги отеля');
if (!defined('hotel_room_types')) define('hotel_room_types', 'Типы номеров');
if (!defined('hotel_main_info')) define('hotel_main_info', 'Основная информация');
if (!defined('hotel_not_specified')) define('hotel_not_specified', 'Не указанo');
if (!defined('hotel_phone')) define('hotel_phone', 'Телефон');
if (!defined('hotel_website')) define('hotel_website', 'Веб-сайт');
if (!defined('hotel_location')) define('hotel_location', 'Местоположение');
if (!defined('hotel_build_date')) define('hotel_build_date', 'Дата постройки');
if (!defined('hotel_rennovate_date')) define('hotel_rennovate_date', 'Последняя реновация');
if (!defined('hotel_buildings')) define('hotel_buildings', 'Количество корпусов');
if (!defined('hotel_description')) define('hotel_description', 'Описание');
if (!defined('hotel_allowed')) define('hotel_allowed', 'Разрешено');
if (!defined('hotel_forbiden')) define('hotel_forbiden', 'Запрещено');
if (!defined('hotel_yes')) define('hotel_yes', 'Есть');
if (!defined('hotel_no')) define('hotel_no', 'Нету');
if (!defined('hotel_zoo')) define('hotel_zoo', 'Размещение с животными');
if (!defined('hotel_smoking')) define('hotel_smoking', 'Номера для некурящих');
if (!defined('hotel_no_info')) define('hotel_no_info', 'Информация об отеле не указана');
if (!defined('hotel_meals')) define('hotel_meals', 'Питание');
if (!defined('hotel_no_text')) define('hotel_no_text', 'Информация отсутствует');
if (!defined('hotel_additional_services')) define('hotel_additional_services', 'Другие услуги');
if (!defined('hotel_free')) define('hotel_free', 'Бесплатно');
if (!defined('hotel_paid')) define('hotel_paid', 'Платно');
if (!defined('hotel_laundry')) define('hotel_laundry', 'Прачечная');
if (!defined('hotel_parking')) define('hotel_parking', 'Парковка');
if (!defined('hotel_tv')) define('hotel_tv', 'Кабельное телевидение');
if (!defined('hotel_free_dryer')) define('hotel_free_dryer', 'Бесплатный фен');
if (!defined('hotel_paid_dryer')) define('hotel_paid_dryer', 'Платный фен');
if (!defined('hotel_no_rooms_info')) define('hotel_no_rooms_info', 'Информация по номерам не предоставляется');
if (!defined('hotel_line')) define('hotel_line', 'линия');
if (!defined('hotel_to_airport')) define('hotel_to_airport', 'до аэропорта');
if (!defined('hotel_guests')) define('hotel_guests', 'гостей');
if (!defined('hotel_before')) define('hotel_before', 'До');
if (!defined('hotel_guests_text')) define('hotel_guests_text', 'взрослых и/или до');
if (!defined('hotel_childs')) define('hotel_childs', 'детей');
if (!defined('hotel_no_short')) define('hotel_no_short', 'Короткая информация отсутствует');
if (!defined('hotel_departure_date')) define('hotel_departure_date', 'Дата вылета');
if (!defined('hotel_nights')) define('hotel_nights', 'Ночей');
if (!defined('hotel_adults')) define('hotel_adults', 'Взрослые');
if (!defined('hotel_departure_from')) define('hotel_departure_from', 'Вылет из');
if (!defined('hotel_nights_pl')) define('hotel_nights_pl', 'Кол-во ночей');
if (!defined('hotel_adults_pl')) define('hotel_adults_pl', 'Кол-во взрослых');
if (!defined('hotel_cities_pl')) define('hotel_cities_pl', 'Все города');
if (!defined('hotel_touroperator')) define('hotel_touroperator', 'Туроператор');
if (!defined('hotel_count_nights')) define('hotel_count_nights', 'Количество ночей');
if (!defined('hotel_reviews_about')) define('hotel_reviews_about', 'Отзывы о');
if (!defined('hotel_no_reviews')) define('hotel_no_reviews', 'Отзывов об отеле нет');
if (!defined('hotel_last_news')) define('hotel_last_news', 'Последние новости');
if (!defined('hotel_repairs')) define('hotel_repairs', 'Ремонтные работы');
if (!defined('hotel_events')) define('hotel_events', 'Важное событие');
if (!defined('hotel_news')) define('hotel_news', 'Новость');
if (!defined('hotel_no_news')) define('hotel_no_news', 'За последний год ни одной новости в этом отеле');

if (!defined('no_tour_id')) define('no_tour_id', 'ID тура не определен');
if (!defined('no_prices_found')) define('no_prices_found', 'Цена на тур не определена');
if (!defined('no_price_exists')) define('no_price_exists', 'Цены на тур не существует');
if (!defined('no_curency_exists')) define('no_curency_exists', 'Валюта не найдена в Базе');
if (!defined('orders_limit_signup')) define('orders_limit_signup', 'Сегодня Вам был создан новый аккаунт, авторизуйтесь.');
if (!defined('no_email_listed')) define('no_email_listed', 'Необходимо указать Email');
if (!defined('email_error')) define('email_error', 'Email введен с ошибками');
if (!defined('please_auth')) define('please_auth', 'Мы нашли Ваш Email, %s! Пожалуйста, введите пароль, чтобы купить тур:');
if (!defined('login_error')) define('login_error', 'Неправильный Email или пароль');
if (!defined('activate_account')) define('activate_account', 'Активируйте Аккаунт перед покупкой');

if (!defined('email_confirm')) define('email_confirm', 'Подтверждение Email на');
if (!defined('email_text')) define('email_text', 'Здравствуйте, Вы только что начали заказ своего тура на TravelNet Tour!<br/>
									После оформления заказа Вам необходимо будет перейти в Личный кабинет. Чтобы это сделать, необходимо активировать свой аккаунт:<br/><br/></p><p style="text-align:center;">
									<a class="btn btn-primary btn-lg raised" href="%sactivation?token=%s&email=%s">Активировать</a><br/><br/></p><p>
									Для первого доступа в Ваш Аккаунт мы сгенерировали Вам пароль: <strong>%s</strong>. Настоятельно рекомендуем его поменять 
									при первом входе в Личный кабинет, в разделе "Профиль" -> "Изменить пароль".<br/><br/>
									В противном случае, если Вы не совершали покупок на TravelNet Tour, пожалуйста, удалите это сообщение.<br/>
									Если у Вас есть предложения, замечания или вопросы касательно сайта, 
									пожалуйста, свяжитесь с нами через <a href="%scontacts">Форму обратной связи</a> или другим удобным образом.<br/><br/>
									<small>Сообщение было сгенерировано автоматически, пожалуйста, не отвечайте на него.<br/>
									С наилучшими пожеланиями,<br/>Команда TravelNet Tour</small>');

if (!defined('text_pieces')) define('text_pieces', 'Купить в рассрочку');
if (!defined('tour_info_credit')) define('tour_info_credit', 'Как купить в рассрочку?');
if (!defined('tour_info_credit_header')) define('tour_info_credit_header', 'Краткая инструкция по покупке в рассрочку');
if (!defined('tour_info_credit_text')) define('tour_info_credit_text', 'На сайте TravelNet Tour доступны разнообразные формы оплаты, в том числе
									и оплата частями или в рассрочку. Чтобы оплатить в рассрочку необходимо нажать кнопку "Забронировать", пройти 
									процесс оформления заказа и на этапе оплаты выбрать желаемый банк для оплаты частями. Можно проверить,
									можете ли Вы воспользоваться подобной услугой, введя номер телефона, перед выбором количества платежей. 
									Далее Вы будете перенаправлены на страницу банка, где можно детально ознакомится с их условиями рассрочки.<br/><br/>
									Обратите внимание, что TravelNet Tour не гарантирует, что Вы можете купить тур в рассрочку, поскольку окончательное
									решение остается за банком. Поэтому, пожалуйста, прежде чем оплачивать заказ частями, убедитесь, что Вам доступна такая услуга в банке, где Вы обсуживаетесь.');

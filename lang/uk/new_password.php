<?php

// login page info
if (!defined('page_main_header')) define('page_main_header', 'Введіть новий пароль');
if (!defined('page_header')) define('page_header', 'Новий пароль');
if (!defined('page_header2')) define('page_header2', 'Повторіть пароль');
if (!defined('page_instr_header')) define('page_instr_header', 'Інструкція');
if (!defined('page_instr_desc')) define('page_instr_desc', 'Введіть в першому полі Ваш новий пароль і повторіть його у другому полі. Пароль повинен відповідати наступним вимогам:');
if (!defined('page_instr_1')) define('page_instr_1', 'бути довшим шести символів');
if (!defined('page_instr_2')) define('page_instr_2', 'містити хоча б одну букву');
if (!defined('page_instr_3')) define('page_instr_3', 'містити хоча б одну цифру');
if (!defined('page_send')) define('page_send', 'Відновити');
if (!defined('page_auth')) define('page_auth', 'Перш ніж відновити пароль, Вам необхідно вийти з Акаунту');
if (!defined('err_invalid')) define('err_invalid', 'Увага! Неправильний або застарілий код відновлення пароля.');
if (!defined('err_invalid_code')) define('err_invalid_code', 'Неправильний або застарілий код відновлення пароля.');
if (!defined('err_none')) define('err_none', 'Увага! Ви не запитували відновлення пароля.');
if (!defined('err_account')) define('err_account', 'Ви не запитували відновлення пароля.');
if (!defined('err_fail')) define('err_fail', 'Відновлення пароля можливо на <a href="'.DIR_PATH.'recover">цій сторінці</a>.');
if (!defined('new_no_email')) define('new_no_email', 'Відсутнiй Email. Перейдіть по посиланню з листа на Вашій пошті.');
if (!defined('new_email')) define('new_email', 'Вказаний некоректний Email. Перейдіть по посиланню з листа на Вашій пошті.');
if (!defined('new_password')) define('new_password', 'Ви не ввели пароль');
if (!defined('new_password_repeat')) define('new_password_repeat', 'Паролі не співпадають');
if (!defined('new_email_header')) define('new_email_header', 'Зміна пароля на');
if (!defined('new_pass_wrong')) define('new_pass_wrong', 'Пароль повинен відповідати інструкції');
if (!defined('new_warning')) define('new_warning', 'Увага');
if (!defined('new_success')) define('new_success', 'Пароль був успішно змінено! Тепер, використовуючи його, Ви можете зайти в Особистий кабінет.');
if (!defined('new_text')) define('new_text', 'Привіт %s,<br/> Ваш пароль на сайті TravelNet Tour (%s) був змінений %s з IP-адреси: %s.<br/>
											Щоб увійти в свій Особистий кабінет, перейдіть за посиланням:<br/><br/></p><p style="text-align:center;"> <a href="%slogin" class="btn btn-primary btn-lg raised">Особистий кабінет</a><br/><br/></p><p>
											Спасибі, що вибрали travelNet Tours!<br/> Якщо Ви не змінювали пароль, будь ласка, негайно <a href="%scontacts"> зв\'яжіться з нами</a>!<br/><br/>
											<small>Це повідомлення було згенеровано автоматично, будь ласка, не треба на нього відповідати.<br/> З найкращими побажаннями,<br/> Команда TravelNet Tour</small>');

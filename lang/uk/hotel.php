<?php

if (!defined('selector_kurort_city_available')) define('selector_kurort_city_available', 'Доступні міста');
if (!defined('not_found_header')) define('not_found_header', 'На обраний Вами період тури не знайдені');
if (!defined('not_found_text')) define('not_found_text', 'Схоже, турів за обраними параметрами немає, або їх розкупили. 
														  Спробуйте виставити іншу дату, або вибрати із запропонованих нижче. Якщо хочете 
														  автоматично знайти найближчі доступні тури - натисніть кнопку нижче і ми знайдемо 
														  всі доступні пропозиції!');
if (!defined('not_found_btn')) define('not_found_btn', 'Автоматичний пошук турів');

if (!defined('tour_currency')) define('tour_go_email', 'Введіть Email для бронювання');
if (!defined('tour_book_text')) define('tour_book_text', 'Щоб продовжити бронювання, введіть свій Email. На нього будуть приходити повідомлення і підтвердження дій на сайті');
if (!defined('tour_email_pl')) define('tour_email_pl', 'Ваш Email');
if (!defined('cant_order')) define('cant_order', 'Ви не можете покупати на TravelNet');

if (!defined('hotel_favorites')) define('hotel_favorites', 'До обраного');
if (!defined('hotel_share')) define('hotel_share', 'Поділитися');
if (!defined('hotel_hotel')) define('hotel_hotel', 'Готель');
if (!defined('hotel_credentials')) define('hotel_credentials', 'на TravelNet Tour - виліт, ціни, інформація');
if (!defined('hotel_details')) define('hotel_details', 'Детальніше');
if (!defined('hotel_sitename')) define('hotel_sitename', 'на TravelNet Tour');
if (!defined('hotel_meta_desc')) define('hotel_meta_desc', 'Порівняти ціни різних туроператорів на цей напрямок і виберіть найдешевшу поїздку.');
if (!defined('hotel_details_meta')) define('hotel_details_meta', 'Подробиці про готель');
if (!defined('hotel_info_text')) define('hotel_info_text', 'Інформація про готель');
if (!defined('hotel_prices')) define('hotel_prices', 'Подивитися ціни');
if (!defined('hotel_photos_map')) define('hotel_photos_map', 'Фото + мапа');
if (!defined('hotel_about')) define('hotel_about', 'Про готель');
if (!defined('hotel_services')) define('hotel_services', 'Послуги готеля');
if (!defined('hotel_room_types')) define('hotel_room_types', 'Типи номерів');
if (!defined('hotel_main_info')) define('hotel_main_info', 'Основна інформація');
if (!defined('hotel_not_specified')) define('hotel_not_specified', 'Не вказанo');
if (!defined('hotel_phone')) define('hotel_phone', 'Телефон');
if (!defined('hotel_website')) define('hotel_website', 'Веб-сайт');
if (!defined('hotel_location')) define('hotel_location', 'Розташування');
if (!defined('hotel_build_date')) define('hotel_build_date', 'Дата будування');
if (!defined('hotel_rennovate_date')) define('hotel_rennovate_date', 'Остання реновація');
if (!defined('hotel_buildings')) define('hotel_buildings', 'Кількість корпусів');
if (!defined('hotel_description')) define('hotel_description', 'Опис');
if (!defined('hotel_allowed')) define('hotel_allowed', 'Дозволено');
if (!defined('hotel_forbiden')) define('hotel_forbiden', 'Заборонено');
if (!defined('hotel_yes')) define('hotel_yes', 'Присутнє');
if (!defined('hotel_no')) define('hotel_no', 'Вiдсутнє');
if (!defined('hotel_zoo')) define('hotel_zoo', 'Розміщення з тваринами');
if (!defined('hotel_smoking')) define('hotel_smoking', 'Номери для некурящих');
if (!defined('hotel_no_info')) define('hotel_no_info', 'Інформація про готель не вказана');
if (!defined('hotel_meals')) define('hotel_meals', 'Харчування');
if (!defined('hotel_no_text')) define('hotel_no_text', 'Інформація відсутня');
if (!defined('hotel_additional_services')) define('hotel_additional_services', 'Інші послуги');
if (!defined('hotel_free')) define('hotel_free', 'Безкоштовно');
if (!defined('hotel_paid')) define('hotel_paid', 'За додаткову плату');
if (!defined('hotel_laundry')) define('hotel_laundry', 'Пральня');
if (!defined('hotel_parking')) define('hotel_parking', 'Парковка');
if (!defined('hotel_tv')) define('hotel_tv', 'Кабельне телебачення');
if (!defined('hotel_free_dryer')) define('hotel_free_dryer', 'Безкоштовний фен');
if (!defined('hotel_paid_dryer')) define('hotel_paid_dryer', 'Платный фен');
if (!defined('hotel_no_rooms_info')) define('hotel_no_rooms_info', 'Інформація за номерами не надається');
if (!defined('hotel_line')) define('hotel_line', 'лінія');
if (!defined('hotel_to_airport')) define('hotel_to_airport', 'до аеропорту');
if (!defined('hotel_guests')) define('hotel_guests', 'гостей');
if (!defined('hotel_before')) define('hotel_before', 'До');
if (!defined('hotel_guests_text')) define('hotel_guests_text', 'дорослих и/или до');
if (!defined('hotel_childs')) define('hotel_childs', 'дiтей');
if (!defined('hotel_no_short')) define('hotel_no_short', 'Коротка інформація відсутня');
if (!defined('hotel_departure_date')) define('hotel_departure_date', 'Дата вильоту');
if (!defined('hotel_nights')) define('hotel_nights', 'Ночей');
if (!defined('hotel_adults')) define('hotel_adults', 'Дорослi');
if (!defined('hotel_departure_from')) define('hotel_departure_from', 'Виліт з');
if (!defined('hotel_nights_pl')) define('hotel_nights_pl', 'Кількість ночей');
if (!defined('hotel_adults_pl')) define('hotel_adults_pl', 'Кількість дорослих');
if (!defined('hotel_cities_pl')) define('hotel_cities_pl', 'Усі міста');
if (!defined('hotel_touroperator')) define('hotel_touroperator', 'Туроператор');
if (!defined('hotel_count_nights')) define('hotel_count_nights', 'Кількість ночей');
if (!defined('hotel_reviews_about')) define('hotel_reviews_about', 'Відгуки про');
if (!defined('hotel_no_reviews')) define('hotel_no_reviews', 'Відгуків про готель немає');
if (!defined('hotel_last_news')) define('hotel_last_news', 'Останні новини');
if (!defined('hotel_repairs')) define('hotel_repairs', 'Ремонтні роботи');
if (!defined('hotel_events')) define('hotel_events', 'Важлива подія');
if (!defined('hotel_news')) define('hotel_news', 'Новина');
if (!defined('hotel_no_news')) define('hotel_no_news', 'За останній рік жодних оновлень у цьому готелі');

if (!defined('no_tour_id')) define('no_tour_id', 'ID туру не визначений');
if (!defined('no_prices_found')) define('no_prices_found', 'Ціна туру не визначена');
if (!defined('no_price_exists')) define('no_price_exists', 'Ціни на тур не існує');
if (!defined('no_curency_exists')) define('no_curency_exists', 'Валюта не знайдено в Базі');
if (!defined('orders_limit_signup')) define('orders_limit_signup', 'Сьогодні Вам був створений новий акаунт, авторизуйтесь.');
if (!defined('no_email_listed')) define('no_email_listed', 'Необхідно вказати Email');
if (!defined('email_error')) define('email_error', 'Email введений з помилками');
if (!defined('please_auth')) define('please_auth', 'Ми знайшли Ваш Email, %s! Будь ласка, введіть пароль, щоб купити тур:');
if (!defined('login_error')) define('login_error', 'Неправильний Email або пароль');
if (!defined('activate_account')) define('activate_account', 'Активуйте Аккаунт перед покупкою');

if (!defined('email_confirm')) define('email_confirm', 'Підтвердження Email на');
if (!defined('email_text')) define('email_text', 'Привіт, Ви тільки що почали замовлення свого туру на TravelNet Tour!<br/>
Після оформлення замовлення Вам необхідно буде перейти в Особистий кабінет. Щоб це зробити, необхідно активувати свій акаунт:<br/><br/></p><p style="text-align:center;">
									<a class="btn btn-primary btn-lg raised" href="%sactivation?token=%s&email=%s">Активувати</a><br/><br/></p><p>
									Для першого доступу в Ваш Аккаунт ми згенерували Вам пароль: <strong>%s</strong>. Настійно рекомендуємо його поміняти
									при першому вході в Особистий кабінет, в розділі "Профіль" -> "Змінити пароль".<br/><br/>
									В іншому випадку, якщо Ви не робили покупок на TravelNet Tour, будь ласка, видаліть це повідомлення.<br/>
									Якщо у Вас є пропозиції, зауваження або питання щодо сайту,
									будь ласка, зв\'яжіться з нами через <a href="%scontacts">Форму зворотнього зв\'язку</a> або іншим зручним чином.<br/><br/>
									<small>Повідомлення було згенеровано автоматично, будь ласка, не відповідайте на нього.<br/>
									З найкращими побажаннями,<br/>Команда TravelNet Tour</small>');

if (!defined('text_pieces')) define('text_pieces', 'Купити в розстрочку');
if (!defined('tour_info_credit')) define('tour_info_credit', 'Як купити в розстрочку?');
if (!defined('tour_info_credit_header')) define('tour_info_credit_header', 'Коротка інструкція по покупці в розстрочку');
if (!defined('tour_info_credit_text')) define('tour_info_credit_text', 'На сайті TravelNet Tour доступні різноманітні форми оплати, в тому числі
												і оплата частинами або в розстрочку. Щоб оплатити в розстрочку необхідно натиснути кнопку "Забронювати", пройти
												процес оформлення замовлення і на етапі оплати вибрати бажаний банк для оплати частинами. Можна перевірити,
												чи можете Ви скористатися подібною послугою, ввівши номер телефону, перед вибором кількості платежів.
												Далі Ви будете перенаправлені на сторінку банку, де можна детально ознайомиться з їх умовами розстрочки. <br/> <br/>
												Зверніть увагу, що TravelNet Tour не гарантує, що Ви можете купити тур в розстрочку, оскільки остаточне
												рішення залишається за банком. Тому, будь ласка, перш ніж оплачувати замовлення частинами, переконайтеся, що Вам доступна така послуга в банку, де Ви обслуговуєтесь.');



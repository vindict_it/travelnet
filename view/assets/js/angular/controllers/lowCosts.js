travelNetToursApp.controller('LowCostsController', function($scope, $http, $q) {
    var lowCosts = this;
    showLoader();

    lowCosts.data = [];
    lowCosts.plainData = [];
    lowCosts.countries = {};
    lowCosts.countryID = 0;
    lowCosts.stars = [];
    lowCosts.pansions = [];

    lowCosts.initTable = function() {
        var sourceUrl = baseHostUrl+'/model/ajax-handler.php';
        var deferred = $q.defer();

        $http.get(sourceUrl, {params: {countryID: lowCosts.countryID, action: 'GetMinPricesByCountries'}}).then(function success(response) {
            //sort pansions
			console.log(response);
            response.data.forEach(function(country, i) {
                country.hotel_stars.forEach(function(star, j) {
                    response.data[i].hotel_stars[j].pans_prices.sort(lowCosts.sortByPansionCodes);
                });
            });

            lowCosts.data = response.data;

            //format data for header
            lowCosts.formatHeader();
            console.log('Sorted Min Prices by Country:');
            console.log(lowCosts.data);

            //format plain data by country
            lowCosts.getCountries().then(function success(response) {
                lowCosts.formatPlainData();
            });

            hideLoader();
            deferred.resolve('Success');
        }, function failed() {
            hideLoader();
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    lowCosts.formatHeader = function() {
        lowCosts.stars = [];
        lowCosts.data.forEach(function(country, i) {
            country.hotel_stars.forEach(function(star, j) {
                var exst = -1;
                lowCosts.stars.forEach(function(exstStar, i) {
                    if (exstStar.star == parseInt(star.star))
                        exst = i;
                });

                var objStar;
                if (exst == -1) {
                    lowCosts.stars.push({star: parseInt(star.star), pans: []});
                    objStar = lowCosts.stars[lowCosts.stars.length-1];

                    if (isNaN(objStar.star)) objStar.star = 1;
                }
                else
                    objStar = lowCosts.stars[exst];

                star.pans_prices.forEach(function(pansion) {
                    if (objStar.pans.indexOf(pansion.pansion_code) == -1)
                        objStar.pans.push(pansion.pansion_code);
                });
            });
        });

        lowCosts.stars.sort(lowCosts.sortStars);
        lowCosts.pansions = [];
        lowCosts.stars.forEach(function(star, i) {
            star.pans.forEach(function(pan) {
                lowCosts.pansions.push(pan);
            });
        });
    };



    lowCosts.getCountries = function() {
        var deferred = $q.defer();

        $http({method: 'GET', url: baseHostUrl+'/view/assets/src/countries.json'}).then(function success(response) {
            for (var countryName in response.data) {
                if (countryName != "" && response.data.hasOwnProperty(countryName))
                    lowCosts.countries[response.data[countryName].id] = {
                        id: response.data[countryName].id,
                        name: countryName,
                        code: response.data[countryName].code
                    };
            }

            console.log('Low costs:');
            console.log(lowCosts.countries);

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    lowCosts.formatPlainData = function() {
        var deferred = $q.defer();
        $http({method: 'GET', url: baseHostUrl+'/view/assets/src/countries.json'}).then(function success(response) {
            console.log('Plain data:');
            console.log(lowCosts.data);

            lowCosts.data.forEach(function(country, i) {
                if (lowCosts.countries[country.country_id] == undefined) return;
                
                var plainCountry = {
                    country: {id: country.country_id, code: country.country_code, name: lowCosts.countries[country.country_id].name},
                    prices: []
                };

                lowCosts.stars.forEach(function(star) {
                    star.pans.forEach(function(pansion) {
                        var price = '-';

                        country.hotel_stars.forEach(function(curStar) {
                            var curStarValue = parseInt(curStar.star);
                            curStar.pans_prices.forEach(function(curPansion) {
                                if (
                                    curPansion.pansion_code == pansion &&
                                    curStarValue == star.star
                                )
                                    price = curPansion.price;
                            });
                        });

                        plainCountry.prices.push(price);
                    });
                });

                lowCosts.plainData.push(plainCountry);
            });
            console.log('Plain data:');
            console.log(lowCosts.plainData);

            deferred.resolve('Success');
        }, function failed() {
            deferred.reject('Fail');
        });

        return deferred.promise;
    };



    lowCosts.relocate = function(countryName, index) {
        var c = 0;
        var star = 1;
        lowCosts.stars.forEach(function(item) {
            if (index >= c) star = item.star;
            c += item.pans.length;
        });
        var query = 'stars='+star+'&food='+lowCosts.pansions[index]+'&country='+countryName;

        window.location = baseHostUrl+'/t?'+query;
    };



    lowCosts.sortStars = function(a, b) {
        if (a.star < b.star)
            return -1;
        if (a.star > b.star)
            return 1;
        return 0;
    };



    lowCosts.sortByPansionCodes = function(a, b) {
        var p = ['BB', 'FB', 'RO', 'AI', 'HB'];
        if (p.indexOf(a.pansion_code) < p.indexOf(b.pansion_code))
            return -1;
        if (p.indexOf(a.pansion_code) > p.indexOf(b.pansion_code))
            return 1;
        return 0;
    };



    //on load
    lowCosts.initTable();
});
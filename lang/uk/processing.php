<?php

if (!defined('order_flight_step1')) define('order_flight_step1', 'Бронювання');
if (!defined('order_flight_step2')) define('order_flight_step2', 'Підтвердження');
if (!defined('order_flight_step3')) define('order_flight_step3', 'Оплата');
if (!defined('order_flight_step4')) define('order_flight_step4', 'Видача документів');
if (!defined('processing_specialists')) define('processing_specialists', 'Дані про обраний турі обробляються нашими фахівцями');
if (!defined('processing_doing')) define('processing_doing', 'Обробка займе не більше доби');
if (!defined('processing_got')) define('processing_got', 'Ваші дані надійшли в обробку. Як тільки вони будуть перевірені - Ви отримаєте відповідне повідомлення та інструкції на електронну пошту та в');
if (!defined('processing_lk')) define('processing_lk', 'особистому кабінеті');
if (!defined('processing_reading')) define('processing_reading', 'Поки Ви очікуєте, рекомендуємо до прочитання');
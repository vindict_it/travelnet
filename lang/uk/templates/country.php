<?php

if (!defined('page_video')) define('page_video', 'Дивитися відео');
if (!defined('page_video_header')) define('page_video_header', 'Відео-презентація країни');
if (!defined('page_map_open')) define('page_map_open', 'Відкрити на карті');
if (!defined('page_people')) define('page_people', 'Населення');
if (!defined('page_people_text')) define('page_people_text', 'близько %s млн. чоловік');
if (!defined('page_language')) define('page_language', 'Основна мова');
if (!defined('country_currency')) define('country_currency', 'Валюта в обороті');
if (!defined('tours_currency')) define('tours_currency', 'Валюта продажу турів');
if (!defined('page_phone_code')) define('page_phone_code', 'Телефонний код');
if (!defined('page_meal_cost')) define('page_meal_cost', 'Вартість типової вечері');
if (!defined('page_car_rent')) define('page_car_rent', 'Вартість оренди авто');
if (!defined('page_tip_amount')) define('page_tip_amount', 'Розмір чайових');
if (!defined('page_need_visa')) define('page_need_visa', 'Віза');
if (!defined('page_need')) define('page_need', 'потрібна для деяких країн');
if (!defined('page_no_need')) define('page_no_need', 'не потрібна');

if (!defined('page_tours_search')) define('page_tours_search', 'Переглянути всі тури');
if (!defined('page_more_info')) define('page_more_info', 'Детальний опис');
if (!defined('page_for_tourists')) define('page_for_tourists', 'Пам\'ятка туристу');
if (!defined('page_for_visa')) define('page_for_visa', 'Детально про візу');
if (!defined('page_for_car_rent')) define('page_for_car_rent', 'Детально про оренду авто');
if (!defined('page_country_description')) define('page_country_description', 'Про країну %s');
if (!defined('page_for_customs')) define('page_for_customs', 'Про митний контроль');
if (!defined('page_for_phones')) define('page_for_phones', 'Корисні номери');
if (!defined('page_for_transport')) define('page_for_transport', 'Про транспорт');
if (!defined('page_for_security')) define('page_for_security', 'Безпека туристів');
if (!defined('page_for_climate')) define('page_for_climate', 'Клімат');
if (!defined('page_for_hotels')) define('page_for_hotels', 'Про готелi');
if (!defined('page_for_money')) define('page_for_money', 'Гроші та обмін валют');
if (!defined('page_for_curorts')) define('page_for_curorts', 'Курорти і місця відпочинку');
if (!defined('page_for_shops')) define('page_for_shops', 'Шопінг та магазини');
if (!defined('page_for_kitchen')) define('page_for_kitchen', 'Ресторани і кухня');
<?php

if (!defined('page_video')) define('page_video', 'Смотреть видео');
if (!defined('page_video_header')) define('page_video_header', 'Видео-презентация страны');
if (!defined('page_map_open')) define('page_map_open', 'Открыть на карте');
if (!defined('page_people')) define('page_people', 'Население');
if (!defined('page_people_text')) define('page_people_text', 'около %s млн. человек');
if (!defined('page_language')) define('page_language', 'Основной язык');
if (!defined('country_currency')) define('country_currency', 'Валюта в обороте');
if (!defined('tours_currency')) define('tours_currency', 'Валюта продажи туров');
if (!defined('page_phone_code')) define('page_phone_code', 'Телефонный код');
if (!defined('page_meal_cost')) define('page_meal_cost', 'Стоимость типичного ужина');
if (!defined('page_car_rent')) define('page_car_rent', 'Стоимость аренды авто');
if (!defined('page_tip_amount')) define('page_tip_amount', 'Размер чаевых');
if (!defined('page_need_visa')) define('page_need_visa', 'Виза');
if (!defined('page_need')) define('page_need', 'нужна для некоторых стран');
if (!defined('page_no_need')) define('page_no_need', 'не нужна');

if (!defined('page_tours_search')) define('page_tours_search', 'Смотреть все туры');
if (!defined('page_more_info')) define('page_more_info', 'Подробное описание');
if (!defined('page_for_tourists')) define('page_for_tourists', 'Памятка туристу');
if (!defined('page_for_visa')) define('page_for_visa', 'Подробно о визе');
if (!defined('page_for_car_rent')) define('page_for_car_rent', 'Подробно об аренде авто');
if (!defined('page_country_description')) define('page_country_description', 'О стране %s');
if (!defined('page_for_customs')) define('page_for_customs', 'О таможенном контроле');
if (!defined('page_for_phones')) define('page_for_phones', 'Полезные номера');
if (!defined('page_for_transport')) define('page_for_transport', 'О транспорте');
if (!defined('page_for_security')) define('page_for_security', 'Безопасность туристов');
if (!defined('page_for_climate')) define('page_for_climate', 'Климат');
if (!defined('page_for_hotels')) define('page_for_hotels', 'Об отелях');
if (!defined('page_for_money')) define('page_for_money', 'Деньги и обмен валют');
if (!defined('page_for_curorts')) define('page_for_curorts', 'Курорты и места отдыха');
if (!defined('page_for_shops')) define('page_for_shops', 'Шоппинг и магазины');
if (!defined('page_for_kitchen')) define('page_for_kitchen', 'Рестораны и кухня');